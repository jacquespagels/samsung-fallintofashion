// Fill this with useful values, like link URLs, animation durations, etc.
// Use it as a single reference-point for app-settings and configuration.
var config = {

	colours:{

		'Winter White': [255,255,255],
		'GI Jane': [53,73,51],
		'Savile Row': [62,53,36],
		'Wild': [240,164,17],
		'Wine & Dine': [99,19,57],
		'Pretty in Pink': [204,158,174],
		'Locked on Leather': [0,0,0],
		'Tartan Queen': [168,13,31],
		'Warm & Cosy': [254,242,111]
	},	

	smallAssets:[
	
			[
				'rotate.png',
				'background.jpg'
				
			],

			[
				'globe-1.png',
				'globe-2.png',
				'globe-3.png',
				'spin.png',
				'marker.png',
				'latte.png',
				'pot.png',
				'woman.png',
				'featured.png',
				'ethopia.png',
				'button.png',
				'espresso.png'
			]	
			
	],

	largeAssets:[
	
			[
				'rotate.png',
				'background.jpg'
				
				
			],

			[
				'globe-1.png',
				'globe-2.png',
				'globe-3.png',
				'spin.png',
				'marker.png',
				'latte.png',
				'pot.png',
				'woman.png',
				'featured.png',
				'ethopia.png',
				'button.png',
				'espresso.png'
			]	
			
	],

	spinnerId: 'loader',
	showRotatePromptWhen: 'landscape',

	videoURL: '../res/media/placeholder.m4v',

	smallLoader: {
		imagePath: '../res/img/small/',

		retinaPath: 'retina/',
		normalPath: 'normal/',
		universalPath: '',
		callbacks: [ function() { M5App.main.init(this.options.imagePath); },
					 function() { M5App.main.assignSecondLoadSmallAssets(this.options.imagePath); }, 
					 function() { console.log("third lot loaded"); }]  	 

	},

	largeLoader: {
		imagePath: '../res/img/large/',

		retinaPath: '',
		normalPath: '',
		universalPath: '',
		callbacks: [ function() { M5App.main.init(this.options.imagePath); },
					 function() { M5App.main.assignSecondLoadLargeAssets(this.options.imagePath); }, 
					 function() { console.log("third lot loaded"); }]  	 

	},
	
	// The pixel sizes of most of the elements in the ad.
	// Used to get ratios when resizing elements to fit the screens.
	pixelSizes: {
		rotatePrompt: [229, 222]
	},

	sizes: {
		rotatePrompt: [54, 55.6]
	},

	widthProperties: ['width', 'minWidth', 'maxWidth'],
	heightProperties: ['height', 'minHeight', 'maxHeight'],

	errors: {},

	copy: {
		notice: 'This is an advertisement',
		header: {
			ios: 'Utopia',
			android: 'Settings'
		}
	}

};

config.smallLoader.assetLists = config.smallLoader.retinaAssetLists = config.smallAssets;
config.largeLoader.assetLists = config.largeLoader.retinaAssetLists = config.largeAssets;
