//M5App.quiz.js

var M5App = M5App || {};


M5App.utils = {

	_ios: /iPad|iPod|iPhone/,
    _iosVersion: /OS\s((\d_*)+)/,
    _android: /Android/,
    _androidVersion: /Android\s((\.?\d)+)/,
    _webkitVersion: /AppleWebKit\/((\d+\.?)*)/,
    _tablet: /iPad/,
    isMobile: 0,
    isTablet: 0,
    isBrowser: 0,
    ios: 0,
    android: 0,
    blackberry: 0,
    windows: 0,
    osVersion: 0,

	_$cache: {},

	$: function (id) {
		
		var el;

		if (M5App.utils._$cache[id]) {
			el = M5App.utils._$cache[id];
		}
		else {
			
			el = document.getElementById(id);

			if (el) {
				M5App.utils._$cache[id] = el;
			}
		}

		return el;
	},

	detect: function() {
            
        var ua = navigator.userAgent;

        // Detect iOS
        if( M5App.utils._ios.test(ua) ) {
  
            M5App.utils.ios = 1;
            M5App.utils.osVersion = ua.match(M5App.utils._iosVersion)[1].replace(/_/g, '.');

            if(M5App.utils._tablet.test(ua)) {
                M5App.utils.isTablet = 1;
            }
            else {
                M5App.utils.isMobile = 1;
            }

        }

        // Detect Android
        else if( M5App.utils._android.test(ua) ) {
            M5App.utils.android = 1;
            M5App.utils.osVersion = ua.match(M5App.utils._androidVersion)[1];
            M5App.utils.isMobile = 1;
        }

        // Assume browser.
        else {
            M5App.utils.isBrowser = 1;
        }
    },

	getLargest: function (array) {
		
		return Math.max.apply(Math,array);
	},

	assignImage: function(id, path) {
		var el = typeof id === 'string' ? this.$(id) : id;

		if(!el) return;

		el.style.backgroundImage = 'url("' + path + '")';
	},

	assignImageToClass: function (klass, path) {
		
		var els = document.querySelectorAll('.' + klass);

		for(var i = 0,il = els.length; i < il; i++) {
			els[i].style.backgroundImage = 'url("' + path + '")';
		}

	},


	getOrientation: function() {
		
		if((window.orientation && window.orientation === 'landscape') || window.innerWidth > window.innerHeight) {
			return 'landscape';
		}
		else {
			return 'portrait';
		}

	},

	scaleElement: function(el, dimensions, pxSize, adjustMarginLeft, adjustMarginTop) {
		var width = dimensions[0],
			height = dimensions[1],
			windowWidth = M5App.main.dimensions.width,
			windowHeight = M5App.main.dimensions.height,
			element,
			ratio;

		height = ((windowHeight / 100) * height);



		if(pxSize) {
			ratio = (pxSize[0] / pxSize[1]);

			width = height * ratio;

			// Clamp the width if we get too wide
			if(width > windowWidth) {
				width = windowWidth;
				height = width / ratio;
			}
		}
		else {
			width = ((windowWidth / 100) * width);
		}

		width = width | 0;
		height = height | 0;

		element = M5App.utils.$(el);


		// Apply the width properties
		for(var i = 0, il = config.widthProperties.length; i < il; ++i) {
			element.style[ config.widthProperties[i] ] = width + 'px';
		}

		// Apply height properties
		for(var i = 0, il = config.heightProperties.length; i < il; ++i) {
			element.style[ config.heightProperties[i] ] = height + 'px';
		}

		// Center horizontally if requested
		if(adjustMarginLeft) {
			element.style.marginLeft = -(width/2) + 'px';
		}

		// Center vertically...
		if(adjustMarginTop) {
			element.style.marginTop = -(height / 2) + 'px';
		}

		// Catch top and left properties if specified
		if(dimensions.length === 4) {
			element.style.left = dimensions[2] + '%';
			element.style.top = dimensions[3] + '%';
		}


		// If we're dealing with a canvas element, we'll need to set the w/h
		// properties of the element itself as well as thru the style object.
		if(element.nodeName.toLowerCase() === 'canvas') {
			element.width = width * window.devicePixelRatio;
			element.height = height * window.devicePixelRatio;
		}
	},

	throttle: function(func, wait) {
		var context, args, timeout, throttling, more, result;
		var whenDone = M5App.utils.debounce(function(){ more = throttling = false; }, wait);
		return function() {
			context = this; args = arguments;
			var later = function() {
				timeout = null;
				if (more) {
					result = func.apply(context, args);
				}
				whenDone();
			};
			if (!timeout) timeout = setTimeout(later, wait);
			if (throttling) {
				more = true;
			} else {
				throttling = true;
				result = func.apply(context, args);
			}
			whenDone();
			return result;
		};
	},

	debounce: function(func, wait, immediate) {
		var timeout, result;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) result = func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) result = func.apply(context, args);
			return result;
		};
	},

	track: function (label) {
		
		_gaq.push(['_trackEvent', 'Ad', 'Ad', label]);
		
	},

	componentToHex: function(c) {
    	var hex = c.toString(16);
    	return hex.length == 1 ? "0" + hex : hex;
	},

	rgbToHex: function(r, g, b) {
    	return "#" + M5App.utils.componentToHex(r) + M5App.utils.componentToHex(g) + M5App.utils.componentToHex(b);
	},

	hexDec: function (r,g,b) {
		var dec = (r << 16 | g << 8 | b);
		return dec;
	},

	removeElPostAnim: function (id) {

		document.getElementById(id).addEventListener('webkitTransitionEnd', M5App.handlers.transitionEndRemovalHandler, false);

	},

	//make sure this is called before ios6 scroll to takes effect as these sizes are pre hidden URL bar sizes    
	checkForiPhone: function () {

		var ua = navigator.userAgent,
    		saf = /Safari/; 

    	if(saf.test(ua) && M5App.utils.osVersion.charAt(0) == "5") M5App.main.ios5OnIphone4 = true; //416 	
    	if(saf.test(ua) && window.innerHeight == 356 && M5App.utils.osVersion == "6.0") M5App.main.ios6OnIphone4 = true; //416 
    	if(saf.test(ua) && window.innerHeight == 372 && M5App.utils.osVersion == "7.0") M5App.main.ios7OnIphone4 = true; 
    	if(saf.test(ua) && window.innerHeight == 444 && M5App.utils.osVersion == "6.0") M5App.main.ios6OnIphone5 = true; //504
    	if(saf.test(ua) && window.innerHeight == 460 && M5App.utils.osVersion == "7.0") M5App.main.ios7OnIphone5 = true; 
  	
	} 
	
};