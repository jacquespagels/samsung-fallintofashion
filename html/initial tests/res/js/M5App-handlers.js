var M5App = M5App || {};


M5App.handlers = {

	canRespondToOrientationEvent: true,

	orientationHandler: function(e) {
		
		if(M5App.handlers.canRespondToOrientationEvent){

			M5App.handlers.canRespondToOrientationEvent = false;
			M5App.handlers.onResize();

			setTimeout(function() {
				M5App.handlers.canRespondToOrientationEvent = true
			},250);
		}
			
	},

	onResize: function(e) {
				
		M5App.main.setWindowDimensions();
		M5App.main.scaleElements();	

		setTimeout(M5App.main.showRotatePromptIfRequired, 0);

	},

	hideLoaderHandler: function (e) {

		M5App.utils.$('loader').removeEventListener("webkitTransitionEnd", M5App.handlers.hideLoaderHandler, false);
		M5App.utils.$('loader').style.display = "none";
	},

	getStartedHandler: function (e) {
		
		document.getElementById('landing-get-started').removeEventListener('click', M5App.handlers.getStartedHandler, false);
		M5App.main.initIntro();

	},

	transitionEndRemovalHandler: function (e) {
		e.currentTarget.removeEventListener('webkitTransitionEnd', M5App.handlers.transitionEndRemovalHandler, false);
		e.currentTarget.style.display = 'none'; 
	},

	introHandler: function (e) {

		e.currentTarget.removeEventListener('click', M5App.handlers.introHandler, false);

		//fade out intro page
		M5App.utils.removeElPostAnim('intro-page');
		document.getElementById('intro-page').style.opacity = 0;

		if(e.currentTarget.id == "intro-take") M5App.main.initQuestionPage();
		else M5App.main.initUploadPage();
		
	}



};