var M5App = M5App || {};


M5App.image = {

	imgEl : document.getElementById('image-upload'),
	previewEl : null, //document.getElementById('scan-holder'),
	uploadedFile : null,
	maxFileSize : 5000 * 1000, //5MB
	imageOriginalWidth : null,
	imageOriginalHeight : null,
	frameWidth: 200,
	frameHeight: 270,

	personaColours: [
						config.colours['Winter White'],
						config.colours['GI Jane'],
						config.colours['Savile Row'],
						config.colours['Wild'],
						config.colours['Wine & Dine'],
						config.colours['Pretty in Pink'],
						config.colours['Locked on Leather'],
						config.colours['Tartan Queen'],
						config.colours['Warm & Cosy']
					],

	prepareImageUpload: function() {
				
		M5App.image.imgEl.addEventListener('change', M5App.image.imageUploadChangeHandler, false);
	},

	imageUploadChangeHandler: function(e){

		var rFilter = /^(image\/bmp|image\/gif|image\/jpeg|image\/png|image\/tiff)$/i;
			reader = new FileReader();

	
		M5App.image.uploadedFile = M5App.image.imgEl.files[0];
	      
        if (! rFilter.test(M5App.image.uploadedFile.type)) {
	        alert("This isn't an image, please ulpoad an image file");
        	return;
	    }
	
	    // test filesize
	    if (M5App.image.uploadedFile.size > M5App.image.MaxFilesize) {
	      	alert('file too big');
	        return;
	    }

	    //read the file with a file reader  
	    reader.addEventListener('load', M5App.image.readerLoadHandler, false);
   	    reader.readAsDataURL(M5App.image.uploadedFile);

	}, 

	//put data url into image preview (we can then get its dimensions too)
	readerLoadHandler: function(e){
	
		M5App.image.previewEl = document.createElement('img');
		M5App.image.previewEl.setAttribute('id', 'scan-holder');
		M5App.image.previewEl.addEventListener('load', M5App.image.previewLoadHandler, false);
        M5App.image.previewEl.src = e.target.result;
	},

	previewLoadHandler: function(e){

		M5App.image.imageOriginalWidth = M5App.image.previewEl.naturalWidth;
	    M5App.image.imageOriginalHeight = M5App.image.previewEl.naturalHeight;

		M5App.image.resizePreview();

		document.getElementById('scan-frame').appendChild(M5App.image.previewEl);
	    document.getElementById('test').innerHTML = M5App.image.previewEl.naturalWidth + ' * ' + M5App.image.previewEl.naturalHeight;

	    //analyze colour
	    var rgb = new ColorFinder().getMostProminentColor(M5App.image.previewEl);
  		//document.getElementById('colour').style.backgroundColor = 'rgb('+rgb.r+','+rgb.g+','+rgb.b+')';

  		M5App.image.matchToNearestColour(rgb);
		

	},

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	We convert the rgb to a decimal value and compare it to decimal values of the
	//  persona rgbs. The persona value that is nearast our value is selected            
	//
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	matchToNearestColour: function(userRGB){

		var colours = [],
			closest = null,
			decToMatch = M5App.utils.hexDec(
								userRGB.r,
								userRGB.g,
								userRGB.b
							);   

		//fill array with decimalized persona colours that we can test against  
		for(var i = 0, il = M5App.image.personaColours.length; i < il; i++){

			colours.push(M5App.utils.hexDec(
								M5App.image.personaColours[i][0],
								M5App.image.personaColours[i][1],
								M5App.image.personaColours[i][2]
							)); 
		}

		//get closest from persona colours  
		for(var d = 0, dl = colours.length; d < dl; d++){
			if (closest == null || Math.abs(colours[d] - decToMatch) < Math.abs(colours[closest] - decToMatch)) closest = d;
  		}
		
		console.log(M5App.main.personas[closest]);	
		
	}, 

	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//
	//	   IMAGE RESIZING SCENARIOS 
	//	
	//		1. image is wider than the frame and taller than the frame
	//
	//			- if the aspect ratio is less than that of the frame (i.e it's more of a square than the frame)
	//			  make the image height the same as the frame, calculate the width based on this and align horizontally
	//
	//			- if the aspect ratio is more than that of the frame (i.e it's a narrower rectangle than the frame)
	//			  make the image width the same as the frame, calculate the height based on this and align vertically  	             
	//
	//		2. image is wider than the frame but not as tall
	//
	//		    -  make the image height the same as the frame, calculate the width based on this and align horizontally and vertically   
	//
	//		3. image is taller than the frame but not as wide
	//
	//			-  make the image width the same as the frame, calculate the height based on this and align vertically  
	//
	//		4. image is not as wide or tall as the frame 
	//
	//			- if the aspect ratio is less than that of the frame (i.e it's more of a square than the frame)
	//			  make the image height the same as the frame, calculate the width based on this and align horizontally
	//
	//			- if the aspect ratio is more than that of the frame (i.e it's a narrower rectangle than the frame)
	//			  make the image width the same as the frame, calculate the height based on this and align vertically  
	//
	//
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	resizePreview: function(){

		var frameAspectRatio = M5App.image.frameHeight / M5App.image.frameWidth,
			imageAspectRatio = 0,
			width = 0,
			height = 0;
		
		if(M5App.image.imageOriginalHeight > M5App.image.imageOriginalWidth) {
			imageAspectRatio = M5App.image.imageOriginalHeight / M5App.image.imageOriginalWidth;
		}
		else imageAspectRatio = M5App.image.imageOriginalWidth / M5App.image.imageOriginalHeight;
	
		// 1. 
		 
		if(M5App.image.imageOriginalWidth >= M5App.image.frameWidth && M5App.image.imageOriginalHeight >= M5App.image.frameHeight) {
			
			if(imageAspectRatio <= frameAspectRatio) {
                                
                width = Math.floor(M5App.image.frameHeight / imageAspectRatio);
                M5App.image.previewEl.style.width =  width + 'px';
                M5App.image.previewEl.style.height = M5App.image.frameHeight + 'px';
                M5App.image.previewEl.style.marginLeft = '-' + Math.floor((width - M5App.image.frameWidth)/2) + 'px';
            }
            else {

	            if(M5App.image.imageOriginalHeight > M5App.image.imageOriginalWidth) {

					height = Math.floor(M5App.image.frameWidth * imageAspectRatio);
					M5App.image.previewEl.style.width =  M5App.image.frameWidth + 'px';
					M5App.image.previewEl.style.height = height + 'px';
					M5App.image.previewEl.style.marginTop = '-' + Math.floor((height - M5App.image.frameHeight)/2) + 'px';

				}
				else {
					width = Math.floor(M5App.image.frameHeight * imageAspectRatio);
					M5App.image.previewEl.style.width =  width + 'px';
					M5App.image.previewEl.style.height = M5App.image.frameHeight + 'px';
					M5App.image.previewEl.style.marginLeft = '-' + Math.floor((width - M5App.image.frameWidth)/2) + 'px';
				}
			}
		}

		// 2.

		if(M5App.image.imageOriginalWidth >= M5App.image.frameWidth && M5App.image.imageOriginalHeight < M5App.image.frameHeight) {

			if(M5App.image.imageOriginalHeight > M5App.image.imageOriginalWidth) width = Math.floor(M5App.image.frameHeight / imageAspectRatio);
			else width = Math.floor(M5App.image.frameHeight * imageAspectRatio); 

			M5App.image.previewEl.style.width =  width + 'px';
			M5App.image.previewEl.style.height = M5App.image.frameHeight + 'px';
			M5App.image.previewEl.style.marginLeft = '-' + Math.floor((width - M5App.image.frameWidth)/2) + 'px';

		}

		// 3. 
		
		if(M5App.image.imageOriginalWidth < M5App.image.frameWidth && M5App.image.imageOriginalHeight >= M5App.image.frameHeight) {

			if(M5App.image.imageOriginalHeight > M5App.image.imageOriginalWidth) height = Math.floor(M5App.image.frameWidth * imageAspectRatio);
			else height = Math.floor(M5App.image.frameWidth / imageAspectRatio);

			M5App.image.previewEl.style.width =  M5App.image.frameWidth + 'px';
			M5App.image.previewEl.style.height = height + 'px';
			M5App.image.previewEl.style.marginTop = '-' + Math.floor((height - M5App.image.frameHeight)/2) + 'px';

		}

		// 4. 

		if(M5App.image.imageOriginalWidth < M5App.image.frameWidth && M5App.image.imageOriginalHeight < M5App.image.frameHeight) {  

		  	if(imageAspectRatio <= frameAspectRatio) {
				
				width = Math.floor(M5App.image.frameHeight / imageAspectRatio);
				M5App.image.previewEl.style.width =  width + 'px';
				M5App.image.previewEl.style.height = M5App.image.frameHeight + 'px';
				M5App.image.previewEl.style.marginLeft = '-' + Math.floor((width - M5App.image.frameWidth)/2) + 'px';
			}
			else {
				
		  		if(M5App.image.imageOriginalWidth > M5App.image.imageOriginalHeight) {

		  			width = Math.floor(M5App.image.frameWidth * imageAspectRatio);
			  		M5App.image.previewEl.style.width =  width + 'px';
					M5App.image.previewEl.style.height = M5App.image.frameHeight + 'px';
					M5App.image.previewEl.style.marginLeft = Math.floor((M5App.image.frameWidth - width)/2) + 'px';

		  		}
		  		else {

		  			height = Math.floor(M5App.image.frameWidth * imageAspectRatio);
			  		M5App.image.previewEl.style.width =  M5App.image.frameWidth + 'px';
					M5App.image.previewEl.style.height = height + 'px';
					M5App.image.previewEl.style.marginTop = Math.floor((M5App.image.frameHeight - height)/2) + 'px';

				}

			}
	
		}	

	}      
           

};