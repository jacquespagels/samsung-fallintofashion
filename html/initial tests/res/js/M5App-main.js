//m5App.main.js

var M5App = M5App || {};

M5App.main = {

	smallLoader: new Loader(config.smallLoader),
	largeLoader: new Loader(config.largeLoader),
	largeDevice: false,
	imageUploadAvailable: false,
	ios5OnIphone4: false,
	ios6OnIphone4: false,
	ios7OnIphone4: false,
	ios6OnIphone5: false,
	ios7OnIphone5: false,
	currentOrientation: "portrait",
	imagePath : "",
	dimensions: {
	width: 0,
	height: 0,
	ratio: 0
	},
	secondLoadReady:false,

	personas: [	'Winter White',
				'GI Jane',
				'Savile Row',
				'Wild',
				'Wine & Dine',
				'Pretty in Pink',
				'Locked on Leather',
				'Tartan Queen',
				'Warm & Cosy'	],

	smallAssets: {
		background:[763,1200]
	},

	largeAssets: {
		backgroundPortrait:[763,1200],
		backgroundLandscape:[763,1200],
	},			

	setWindowDimensions: function () {
		var width = +window.innerWidth,
		height = +window.innerHeight;

		M5App.main.dimensions.width = width;
		M5App.main.dimensions.height = height;
		M5App.main.dimensions.ratio = width/height;
	},

	scaleElements: function() {
		var sizes = config.sizes,
			px = config.pixelSizes;

		M5App.utils.scaleElement('rotatePrompt', sizes.rotatePrompt, px.rotatePrompt, true, true);

		// Resize the stage and loading spinner
		M5App.utils.$('container').style.width = M5App.main.dimensions.width + 'px';
		M5App.utils.$('container').style.height = M5App.main.dimensions.height + 'px';
		M5App.utils.$('loader').style.height = M5App.main.dimensions.height + 'px';

		// Set the body height, can fix some quirks on Android
		document.body.style.height = M5App.main.dimensions.height + 'px';
		document.body.style.maxHeight = M5App.main.dimensions.height + 'px';
	},

	assignSmallAssets: function (prefix) {
		
		M5App.utils.assignImage('rotatePrompt', prefix + 'rotate.png');
		M5App.utils.assignImage('main-background', prefix + 'background.jpg');
	},

	assignLargeAssets: function (prefix) {
		
		M5App.utils.assignImage('rotatePrompt', prefix + 'rotate.png');
		M5App.utils.assignImage('main-background', prefix + 'background.jpg');
	},

	showRotatePromptIfRequired: function () {
		
		var orientation = M5App.utils.getOrientation();
		
		if(!M5App.main.largeDevice) {

			if(orientation === "landscape") {
				
				M5App.utils.$('rotator').style.display = 'block';

				setTimeout(function() {
						M5App.utils.$('rotator').style.opacity = '1';
					}, 0);
			}
			else {

				M5App.utils.$('rotator').style.opacity = '0';
				
				setTimeout(function() {
						M5App.utils.$('rotator').style.display = 'none';
					}, 100);
			}

			// Make sure we show the entire page
			setTimeout(function() {
				window.scrollTo(0, 1);
				M5App.utils.$('rotator').style.height = (window.innerHeight + 1) + 'px';
			}, 100);

		}
		else{
					
			setTimeout( function () {
				M5App.main.handleOrientationChangeForLargeDevice(orientation);
				},100);
		} 
	},

	handleOrientationChangeForLargeDevice: function (orientation) {

		M5App.main.setWindowDimensions();
		M5App.main.scaleElements();	
		//M5App.main.assignLargeAssets();
	
		/* DO ANY TABLET LANDSCAPE POSITIONING HERE */  

		if(orientation == "portrait") M5App.main.resizeBackground(M5App.main.largeAssets.backgroundPortrait, true, false);
		else M5App.main.resizeBackground(M5App.main.largeAssets.backgroundLandscape, false, false);
		
		/*

		M5App.main.resizeSecondLoadLargeAssets();	 
		M5App.main.positionElements(orientation);

		var newHeight = M5App.main.dimensions.height;
		document.getElementById('container').style.minHeight = newHeight+'px';  

		*/

		setTimeout(function(){
		    window.scrollTo(0, 1);
		}, 0);

	},

	showProgLoader: function () {
		
		if(M5App.main.secondLoadReady) {
				
			document.getElementById('loader').style.display = "none";
			
			//use second lot of assets somehow
			//M5App.main.initGlobe();
		}
		else {
							
			document.getElementById('loader').style.display = "block";

			setTimeout(function() {
					document.getElementById('loader').style.opacity = "0.5";
				},0);

			setTimeout( function() {
					M5App.main.showProgLoader();	
				},1000);
		}
	},

	resizeBackground : function (dimensions, portrait, retina) {

		var width = window.innerWidth,
			height = window.innerHeight,
			aspectRatio = portrait ? height/width : width/height,
			bgWidth = dimensions[0],
			bgHeight = dimensions[1],
			bgAspectRatio = bgHeight/bgWidth,
			deviceShorter = aspectRatio <= bgAspectRatio ? true : false;

		document.getElementById('main-background').style.marginLeft = '0px';
		document.getElementById('main-background').style.marginTop = '0px';	

		if(deviceShorter) {

			document.getElementById('main-background').style.width = width + 'px';
			document.getElementById('main-background').style.height = Math.floor(width * bgAspectRatio) + 'px';

			if(!portrait) {
				//align  vertically
				var yOffset = Math.floor((Math.floor(width * bgAspectRatio) - height)/2) - 175;
				document.getElementById('main-background').style.marginTop = '-' + yOffset + 'px';
			}

		}	
		else{

			document.getElementById('main-background').style.width = Math.floor(height/bgAspectRatio) + 'px';
			document.getElementById('main-background').style.height = height + 'px';
						
			//align  horizontally
			var xOffset = Math.floor((Math.floor(height/bgAspectRatio) - width)/2);
			document.getElementById('main-background').style.marginLeft = '-' + xOffset + 'px';
		} 
		
	},

	positionLandingAndIntroElements: function (orientation) {

		if(!M5App.main.largeDevice) {

			//assume portrait
			var height = window.innerWidth > window.innerHeight ? window.innerWidth : window.innerHeight,
				width =  window.innerWidth > window.innerHeight ? window.innerHeight : window.innerWidth;
				logoPos = height * 0.05,
				logoHeight = 120,

			/* LANDING PAGE */  

			//position copy relative to logo
			document.getElementById('landing-copy').style.top = (Math.floor(logoPos) + logoHeight + 10) + 'px';

			//iOS7 offsetting
			if(M5App.main.ios7OnIphone4 || M5App.main.ios7OnIphone5) {
				document.getElementById('landing-get-started').style.bottom = '60px';
			} 

			/* INTRODUCTION PAGE */

			var introQuestionPosPC = 0.15;

			if(!M5App.main.imageUploadAvailable) introQuestionPosPC = 0.30;
			if(M5App.main.ios6OnIphone4 || M5App.main.ios7OnIphone4) introQuestionPosPC = 0.12;
			
			document.getElementById('intro-question').style.top = 100 * introQuestionPosPC + '%';	

			//position everything relative to intro-question
			var introQuestionPos = height * introQuestionPosPC, 
				introQuestionHeight = 30,
				introQuestionInstructPos = Math.floor(introQuestionPos) + introQuestionHeight + ( M5App.main.ios6OnIphone4 || M5App.main.ios7OnIphone4 ? 0 : 10 ), //10
				introQuestionInstructHeight = 30,
				introTakePos = introQuestionInstructPos + introQuestionInstructHeight + 20,
				introTakeHeight = 47,
				introOrPos = introTakePos + introTakeHeight + ( M5App.main.ios6OnIphone4 || M5App.main.ios7OnIphone4 ? 10 : 30 ),
				introOrHeight = 30,
				introStylizerPos = introOrPos + introOrHeight + ( M5App.main.ios6OnIphone4 || M5App.main.ios7OnIphone4 ? 10 : 25 ),
				introStylizerHeight = 30,
				introStylizerInstructPos = introStylizerPos + introStylizerHeight + ( M5App.main.ios6OnIphone4 || M5App.main.ios7OnIphone4 ? 0 : 10 ),
				introStylizerInstructHeight = 30,
				introUploadPos = introStylizerInstructPos + introStylizerInstructHeight + 20;

			document.getElementById('intro-question-instructions').style.top = introQuestionInstructPos + 'px';
			document.getElementById('intro-take').style.top = introTakePos + 'px';
			document.getElementById('intro-or').style.top = introOrPos + 'px';
			document.getElementById('intro-stylizer').style.top = introStylizerPos + 'px';
			document.getElementById('intro-stylizer-instructions').style.top = introStylizerInstructPos + 'px';
			document.getElementById('intro-upload').style.top = introUploadPos + 'px';
			document.getElementById('intro-smallprint').style.bottom = ( M5App.main.ios6OnIphone4 || M5App.main.ios7OnIphone4 ? 48 : 6 ) + 'px';

			//iOS5 bug
			if(M5App.main.ios5OnIphone4) {
				var xOffset = 40;
				document.getElementById('intro-question-instructions').style.top = introQuestionInstructPos + xOffset + 'px';
				document.getElementById('intro-take').style.top = introTakePos + xOffset + 'px';
			}

		}
		else {


		}	


	},	

	init: function (imagePath) {

		M5App.utils.detect();
		M5App.utils.checkForiPhone();
		
		//determine if can use file upload 
		if(window.FileReader) M5App.main.imageUploadAvailable = true;
		
		M5App.main.setWindowDimensions();
		M5App.main.scaleElements();
		M5App.main.imagePath = imagePath;	
		
		if(M5App.main.largeDevice) {
				
			M5App.utils.$('rotator').style.display = 'none';
			M5App.main.assignLargeAssets(imagePath);
		}	
		else M5App.main.assignSmallAssets(imagePath);

		var newHeight = M5App.main.dimensions.height;

		//don't use this for simplytics
		if(screen.availHeight === 548) {
		    newHeight = 504 > newHeight ? 504 : newHeight;
		}  
		else {
		    newHeight = 416 > newHeight ? 416 : newHeight;
		}
		
		document.getElementById('container').style.minHeight = newHeight+'px';  

		setTimeout(function(){
		    window.scrollTo(0, 1);
		}, 0);
		
		setTimeout(M5App.main.showRotatePromptIfRequired, 0);

		window.addEventListener('orientationchange', M5App.handlers.orientationHandler);
		window.addEventListener('resize', M5App.handlers.orientationHandler);

		//fade out loader - fade in app
		M5App.utils.$('loader').addEventListener("webkitTransitionEnd", M5App.handlers.hideLoaderHandler, false);
		M5App.utils.$('loader').style.opacity = '0'; 
		M5App.utils.$('container').style.opacity = '1';

		M5App.main.initApp(M5App.utils.getOrientation());
		
	},

	initApp : function (orientation) {
		
		console.log("we're off!");

		console.log('M5App.main.largeDevice: ' + M5App.main.largeDevice);

		if(M5App.main.largeDevice) {
			
			if(orientation == "portrait") M5App.main.resizeBackground(M5App.main.largeAssets.backgroundPortrait, true, false);
			else M5App.main.resizeBackground(M5App.main.largeAssets.backgroundLandscape, false, false); 

			//M5App.main.resizeLargeAssets(orientation);
		}
		else M5App.main.resizeBackground(M5App.main.smallAssets.background, true, true);
		
		//setTimeout(function() {
			
			M5App.main.positionLandingAndIntroElements(orientation);
		//},1000) 
		
		//in case reset required	
		M5App.main.timer1 = setTimeout(function() {
			M5App.main.initFirstPage();
		},400);

	},

	initFirstPage : function () {

		document.getElementById('landing-page').style.display = 'block';
		
		setTimeout(function() {
				 document.getElementById('landing-page').style.opacity = 1;
			},0);
		
		document.getElementById('landing-get-started').addEventListener('click', M5App.handlers.getStartedHandler, false);

	},

	initIntro : function () {

		console.log('init intro');

		//fade out landing page
		M5App.utils.removeElPostAnim('landing-page');
		document.getElementById('landing-page').style.opacity = 0;

		document.getElementById('intro-page').style.display = 'block';
		
		//hide image upload button if image upload not available 
		if(!M5App.main.imageUploadAvailable) {

			document.getElementById('intro-or').style.display = 'none';
			document.getElementById('intro-stylizer').style.display = 'none';
			document.getElementById('intro-stylizer-instructions').style.display = 'none';
			document.getElementById('intro-upload').style.display = 'none';
			document.getElementById('intro-smallprint').style.display = 'none';
		} 

		setTimeout(function() {
				 document.getElementById('intro-page').style.opacity = 1;
				 document.getElementById('intro-take').addEventListener('click', M5App.handlers.introHandler, false);
				 if(M5App.main.imageUploadAvailable) document.getElementById('intro-upload').addEventListener('click', M5App.handlers.introHandler, false);
			},0);


	},

	initQuestionPage : function () {

		console.log('questions');
	},

	initUploadPage : function () {

		console.log('image');

		document.getElementById('image-page').style.display = 'block';

		setTimeout(function() {
				 document.getElementById('image-page').style.opacity = 1;
			},0);

		M5App.image.prepareImageUpload()
	} 


};