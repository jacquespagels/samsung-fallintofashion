/**
 * An image preloader.
 * @param {Object} options A map of options. Detailed below.
 */
function Loader(options) {

	// Default values if none provided.
	this.options = {

		retinaAssetLists: options.retinaAssetLists || [],

		assetLists: options.assetLists || [],

		universalAssetLists: options.universalAssetLists || [],

		//callback: typeof options.callback === 'function' ? options.callback : function() {},

		callbacks: options.callbacks || [],

		assignAssets: typeof options.assignAssets === 'function' ? options.assignAssets : function() {},

		imagePath: options.imagePath || 'images/',

		retinaPath: (typeof options.retinaPath === 'string' ? options.retinaPath : 'retina/'),

		normalPath: (typeof options.normalPath === 'string' ? options.normalPath : 'normal/'),

		universalPath: (typeof options.universalPath === 'string' ? options.universalPath : 'universal/'),

		autostart: 0

	};

	this.isRetina = 0;

	// Kick off the housekeeping.
	this.initialize();
}


/**
 * A bit of housekeeping
 */
Loader.prototype.initialize = function() {

	this.isRetina = window.devicePixelRatio > 1 ? 1 : 0;

	if(this.options.autostart) {
		this.fetchAssets(0);
	}

};



Loader.prototype.fetchAssets = function(assetListNumber) {

	this.loadedImageCount = 0;
	this.loadedImageList = [];
	this.toLoadCount = 0;		

	// Cache some useful values.
	var opts = this.options,
		that = this,
		retinaAssetLists = opts.retinaAssetLists,
		assetLists = opts.assetLists,
		imagePath = opts.imagePath,
		toLoad = [];

	console.log("TEST: " + imagePath);	


	if(opts.assetLists[assetListNumber].length || opts.retinaAssetLists[assetListNumber].length){

		// If we're on a retina display then add the retina image path to the
		// imagePrefix string, otherwise just add the normal image path.
		if(this.isRetina){
			
			if(!this.pathDetermined) {
				opts.imagePath += opts.retinaPath;
				this.pathDetermined = true;
			}
			for(var i = 0, il = retinaAssetLists[assetListNumber].length; i < il; ++i) {
				toLoad.push(opts.imagePath + retinaAssetLists[assetListNumber][i]);
			}
		}
		else{
			
			if(!this.pathDetermined) {
				opts.imagePath += opts.normalPath;
				this.pathDetermined = true;
			}	
			for(var i = 0, il = assetLists[assetListNumber].length; i < il; ++i) {
				toLoad.push(opts.imagePath + assetLists[assetListNumber][i]);
			}
		}


		this.toLoadCount = toLoad.length;

		// Start preload
		for(i = 0; i < toLoad.length; i++){
			var img = new Image();

			// Cache this image object.
			this.loadedImageList.push( img );

			// Create an onload closure.
			img.onload = this.onImageLoad(img, assetListNumber);

			// Set the src.
			img.src = toLoad[i];

		}

	}
	else{
		opts.callbacks[assetListNumber].call(that);
	}

};


/**
 * [onImageLoad description]
 * @param  {[type]} img [description]
 * @return {[type]}     [description]
 */
Loader.prototype.onImageLoad = function(img, assetListNumber) {

	var that = this;

	return function load() {

		console.log('Loaded image: ...' + this.src.substr(this.src.length/2, this.src.length));

		var img, allLoaded = 1;

		++that.loadedImageCount;

		if(that.loadedImageCount >= that.toLoadCount) {

			// Run a double-check to make sure the image has actually loaded
			// and this onload event wasn't called prematurely.
			for( var i = 0, il = that.loadedImageList.length; i < il; ++i ) {

				img = that.loadedImageList[i];

				if(!img.complete) {
					allLoaded = 0;
					break;
				}
			}

			if(allLoaded) {
				that.options.callbacks[assetListNumber].call(that);
			}
			else {
				setTimeout(load, 150);
			}

		}
	};

};