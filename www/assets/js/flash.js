
	// -------------------------------------------------------------------------------------------------------------------------------
	// flash
	
		var matches = location.href.match(/\/(\w{2}(-\w{2})?)\//);
	
		var flashvars = 
		{
			locale:matches ? matches[1] : 'en'
		};
		
		var params = 
		{
			menu: 'false',
			scale: 'noScale',
			allowFullscreen: 'true',
			allowScriptAccess: 'always',
			bgcolor: '',
			wmode: 'transparent' // can cause issues with FP settings & webcam
		};
		
		var attributes = 
		{
			id:'flash'
		};
		
		swfobject.embedSWF
		(
			'flash.swf', 
			'altContent', 728, 728, '10.0.0', 
			'../../../assets/swf/expressInstall.swf', 
			flashvars, params, attributes
		);

	// -------------------------------------------------------------------------------------------------------------------------------
	// tracking
	
		function track(protocol, id)
		{
			// do something here...
		}
	
	// -------------------------------------------------------------------------------------------------------------------------------
	// development
	
		// ensure there's a console
		
			if( ! window.console )
			{
				document.write('<textarea id="log"></textarea>');
				var output = document.getElementById('log');
				window.console = 
				{
					log:function()
					{
						output.value += Array.prototype.slice.call(arguments) + '\n';
					}
				}
			}