# Samsung Fall into Fashion
Sells a coloured phone.   

## LIBRARIES
* Will prob use a dominant-colour helper class 

## TARGETED DEVICES
* Android 3+
* iOS 5+

## DESKTOP BROWSER SUPPORT
* Cross-browser (Flash used)

## PEOPLE
* Developer: Barney Staddon (Android/iOS web)
* Designer: ?
* Manager: Chris Lethbridge

## HOSTING
* Live: -
* Dev: http://mobile-5.com/dev/samsung/fashion/ad

## NOTES

The user is offered the choice of answering several questions or using the 'Stylizer' to determine the persona that matches their style. The Stylizer allows the user to upload an image of their choice (this image isn't sent to the server, just briefly rendered in a canvas offscreen). The pixels of this image are then parsed to determine a dominant colour. This dominant colour is then matched to the closest of nine colours, each associated with a persona.                  