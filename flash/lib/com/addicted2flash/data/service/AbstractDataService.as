/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.data.service 
{
	import com.addicted2flash.data.DataProgress;
	import com.addicted2flash.util.ArraySet;
	import com.addicted2flash.util.HashMap;
	import com.addicted2flash.util.ICloneable;
	import com.addicted2flash.util.IList;
	import com.addicted2flash.util.IMap;
	import com.addicted2flash.util.ISet;
	
	import flash.errors.IllegalOperationError;		

	/**
	 * This class provides a skeletal implementation of the IDataService interface to minimize 
	 * the effort required to implement this interface. 
	 * 
	 * @author Tim Richter
	 */
	public class AbstractDataService implements IDataService
	{
		protected var _observers: IMap;
		protected var _serviceRequest: IServiceRequest;
		protected var _anchor: *;
		protected var _priority: int;
		protected var _state: String;
		protected var _httpStatus: int;
		protected var _errorMessage: String;

		/**
		 * Create a new <code>AbstractDataService</code>.
		 * 
		 * @param serviceRequest <code>IServiceRequest</code>
		 * @param anchor (optional) any data (f.e. binded to service)
		 * @param priority (optional) priority
		 */
		public function AbstractDataService( serviceRequest: IServiceRequest, anchor: * = null, priority: int = 0 ) 
		{
			_serviceRequest = serviceRequest;
			_anchor = anchor;
			_priority = priority;
			
			_state = ServiceState.READY;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get serviceRequest(): IServiceRequest
		{
			return _serviceRequest;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get anchor(): *
		{
			return _anchor;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get data(): *
		{
			throw new IllegalOperationError( "abstract method needs to be overridden in subclasses" );
		}
		
		/**
		 * @inheritDoc
		 */
		public function get dataList(): IList
		{
			throw new IllegalOperationError( "abstract method needs to be overridden in subclasses" );
		}
		
		/**
		 * @inheritDoc
		 */
		public function get dataProgress(): DataProgress
		{
			throw new IllegalOperationError( "abstract method needs to be overridden in subclasses" );
		}
		
		/**
		 * @inheritDoc
		 */
		public function get priority(): int
		{
			return _priority;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get state(): String
		{
			return _state;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get httpStatus(): int
		{
			return _httpStatus;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get errorMessage(): String
		{
			return _errorMessage;
		}
		
		/**
		 * @inheritDoc
		 */
		public function addObserver( o: IDataServiceObserver ): void
		{
			if( _observers == null ) _observers = new HashMap();
			
			var a: Array = o.acceptedDataEvents;
			var len: int = a.length;
			var list: ISet;
			var note: String;
				
			while( --len > -1 )
			{
				note = a[ len ];
				
				list = _observers.getValue( note );
				
				if( list == null )
				{
					list = new ArraySet();
					
					_observers.put( note, list );
				}
				
				list.add( o );
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeObserver( o: IDataServiceObserver ): void
		{
			if( _observers != null )
			{
				var notifications: Array = o.acceptedDataEvents;
				var len: int = notifications.length;
				var list: ISet;
				var note: String;
				
				while( --len > -1 )
				{
					note = notifications[ len ];
					
					list = ArraySet( _observers.getValue( note ) );
					
					if( list != null )
					{
						list.remove( o );
						
						if( list.isEmpty() )
						{
							list.dispose();
							list = null;
							
							_observers.remove( note );
						}
					}
				}
				
				if( _observers.size() == 0 )
				{
					_observers.dispose();
					_observers = null;
				}
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeObservers(): void
		{
			if( _observers != null )
			{
				_observers.dispose();
				_observers = null;
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function notifyObservers( type: String ): void
		{
			if( _observers != null )
			{
				var list: ISet = ISet( _observers.getValue( type ) );
				var a: Array;
				var len: int;
				var i: int;
				
				if( list != null )
				{
					a = list.toArray();
					len = a.length;
					
					for( i = 0; i < len; ++i )
					{
						IDataServiceObserver( a[ i ] ).dataServiceUpdate( type, this );
					}
				}
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function get observerAmount(): int
		{
			return _observers == null ? 0 : _observers.size();
		}

		/**
		 * @inheritDoc
		 */
		public function clone(): ICloneable
		{
			throw new IllegalOperationError( "abstract class cannot be cloned" );
		}
		
		/**
		 * @inheritDoc
		 */
		public function dispose(): void
		{
			_anchor = null;
			_state = null;
			
			if( _serviceRequest != null )
			{
				_serviceRequest.dispose();
				_serviceRequest = null;
			}
			
			if( _observers != null )
			{
				_observers.dispose();
				_observers = null;
			}
		}
	}
}
