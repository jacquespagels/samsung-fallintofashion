/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.data.service 
{

	/**
	 * Interface all objects interested in events from <code>IDataService</code>s have to implement.
	 * 
	 * @author Tim Richter
	 */
	public interface IDataServiceObserver 
	{
		/**
		 * Returns a list of <code>DataEventType</code>s the <code>IDataServiceObserver</code> is interested in.
		 * 
		 * @return list of interested <code>DataEventType</code>s
		 */
		function get acceptedDataEvents(): Array;
		
		/**
		 * This method is called whenever the <code>IDataService</code>s notifyObservers() method was invoked.
		 * 
		 * @param type <code>DataEventType</code>
		 * @param service <code>IDataService</code>
		 */
		function dataServiceUpdate( type: String, service: IDataService ): void;
	}
}
