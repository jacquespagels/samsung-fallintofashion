/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.data.service 
{
	import flash.errors.IllegalOperationError;
	
	import com.addicted2flash.data.DataProgress;
	import com.addicted2flash.data.loader.ILoaderStrategy;
	import com.addicted2flash.data.service.IManageableDataService;
	import com.addicted2flash.data.service.IServiceRequest;
	import com.addicted2flash.util.ArrayList;
	import com.addicted2flash.util.ICloneable;
	import com.addicted2flash.util.IList;	

	/**
	 * This class is represents a <code>ILoaderQueueService</code> created by <code>LoaderQueue</code>.
	 * If interested in specific <code>DataEventType</code>s, it's recommended to implement
	 * <code>ISpecificObserver</code>. If all <code>DataEventType</code>s are relevant an
	 * implementation of <code>IObserver</code> is recommended.
	 * 
	 * The following <code>DataEventType</code>s are going to be dispatched:
	 * <li><code>DataEventType.COMPLETE</code></li>
	 * <li><code>DataEventType.PROGRESS</code></li>
	 * <li><code>DataEventType.ERROR</code></li>
	 * 
	 * @author Tim Richter
	 */
	public class LoaderQueueService extends AbstractDataService implements IManageableDataService 
	{
		private var _loaderStrategy: ILoaderStrategy;

		/**
		 * Create a new <code>LoaderQueueService</code>.
		 * 
		 * @param loaderStrategy <code>ILoaderStrategy</code> for different loading procedures (f.e. bitmaps or xml)
		 * @param serviceRequest <code>IServiceRequest</code>
		 * @param anchor (optional) any data (f.e. data dependent to the request)
		 * @param priority (optional) priority
		 */
		public function LoaderQueueService( loaderStrategy: ILoaderStrategy, serviceRequest: IServiceRequest, anchor: * = null, priority: int = 0 ) 
		{
			super( serviceRequest, anchor, priority );
			
			_loaderStrategy = loaderStrategy;
			
			_loaderStrategy.dataService = this;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function get data(): *
		{
			return ( _loaderStrategy != null ) ? _loaderStrategy.data : null;
		}

		/**
		 * @inheritDoc
		 */
		override public function get dataList(): IList
		{
			return new ArrayList( [ data ] );
		}
		
		/**
		 * @inheritDoc
		 */
		override public function get dataProgress(): DataProgress
		{
			return ( _loaderStrategy != null ) ? _loaderStrategy.dataProgress : null;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function get state(): String
		{
			return ( _loaderStrategy != null ) ? _loaderStrategy.state : ServiceState.STOP;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function get httpStatus(): int
		{
			return ( _loaderStrategy != null ) ? _loaderStrategy.httpStatus : -1;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function get errorMessage(): String
		{
			return ( _loaderStrategy != null ) ? _loaderStrategy.errorMessage : null;
		}
		
		/**
		 * @inheritDoc
		 */
		public function start(): void
		{
			if( _loaderStrategy != null ) _loaderStrategy.start();
		}

		/**
		 * @inheritDoc
		 */
		public function stop(): void
		{
			if( _loaderStrategy != null ) _loaderStrategy.stop();
		}

		/**
		 * @inheritDoc
		 */
		public function pause(): void
		{
			throw new IllegalOperationError( "pause functionality is not provided within this service" );
		}

		/**
		 * @inheritDoc
		 */
		override public function clone(): ICloneable
		{
			return new LoaderQueueService( _loaderStrategy, _serviceRequest, _anchor );
		}
		
		/**
		 * @inheritDoc
		 */
		override public function dispose(): void
		{
			super.dispose();
			
			if( _loaderStrategy != null )
			{
				_loaderStrategy.dispose();
				_loaderStrategy = null;
			}
		}
		
		/**
		 * String representation of <code>LoaderQueueService</code>.
		 * 
		 * @return String representation of <code>LoaderQueueService</code>
		 */
		public function toString() : String 
		{
			return "[ LoaderQueueService anchor=" + _anchor + " strategy=" + _loaderStrategy + " serviceRequest=" + _serviceRequest + " priority=" + _priority + " ]";
		}
	}
}
