/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.data.service 
{
	import flash.net.URLRequest;
	
	import com.addicted2flash.data.service.IServiceRequest;
	import com.addicted2flash.data.Range;	

	/**
	 * This class represents an service request object with basic information
	 * necessary for data services.
	 * 
	 * @author Tim Richter
	 */
	public class ServiceRequest implements IServiceRequest 
	{
		protected var _range: Range;
		protected var _urlRequest: URLRequest;

		/**
		 * Create a new <code>AbstractServiceRequest</code>
		 * 
		 * @param range (optional) <code>Range</code>
		 * @param urlRequest (optional) <code>URLRequest</code>
		 */
		public function ServiceRequest( range: Range = null, urlRequest: URLRequest = null ) 
		{
			_range = range;
			_urlRequest = urlRequest;
		}

		/**
		 * @inheritDoc
		 */
		public function get range(): Range
		{
			return _range;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get urlRequest(): URLRequest
		{
			return _urlRequest;
		}
		
		/**
		 * @inheritDoc
		 */
		public function dispose(): void
		{
			_range = null;
			_urlRequest = null;
		}
		
		/**
		 * String representation of <code>ServiceRequest</code>.
		 * 
		 * @return String representation of <code>ServiceRequest</code>
		 */
		public function toString() : String 
		{
			return "[ ServiceRequest range=" + _range + " urlRequest=" + _urlRequest + " ]";
		}
	}
}
