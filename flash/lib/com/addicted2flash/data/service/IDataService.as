/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.data.service 
{
	import com.addicted2flash.util.ICloneable;	
	import com.addicted2flash.util.IDisposable;	
	import com.addicted2flash.data.DataProgress;
	import com.addicted2flash.util.IList;	

	/**
	 * Interface for data services.
	 * 
	 * @author Tim Richter
	 */
	public interface IDataService extends IDisposable, ICloneable
	{
		/**
		 * Adds an <code>IDataServiceObserver</code> to the set of observers for this object, provided that it is not 
		 * the same as some observer already in the set.
		 * 
		 * @param o an <code>IDataServiceObserver</code> to be added.
		 */
		function addObserver( o: IDataServiceObserver ): void;
		
		/**
		 * Deletes an <code>IDataServiceObserver</code> from the set of observers of this object. Passing null to this method will have no effect. 
		 * 
		 * @param o the <code>IDataServiceObserver</code> to be deleted.
		 */
		function removeObserver( o: IDataServiceObserver ): void;
		
		/**
		 * Clears the <code>IDataServiceObserver</code> list so that this object no longer has any observers. 
		 */
		function removeObservers(): void;
		
		/**
		 * Returns the number of <code>IDataServiceObserver</code> of this <code>IDataService</code>.
		 * 
		 * @return the number of <code>IDataServiceObserver</code> of this <code>IDataService</code>
		 */
		function get observerAmount(): int;
		
		/**
		 * Registered <code>IDataServiceObserver</code> will be notified.
		 * 
		 * @param type type of notification
		 */
		function notifyObservers( type: String ): void;
		
		/**
		 * return dependend data given at <code>IDataService</code> construction time.
		 * 
		 * @return any data 
		 */
		function get anchor(): *;
		
		/**
		 * Returns priority of this <code>IDataService</code>.
		 * 
		 * @return priority of this <code>IDataService</code>
		 */
		function get priority(): int;
		
		/**
		 * Returns requested data.
		 * 
		 * @return requested data
		 */
		function get data(): *
		
		/**
		 * Returns list with requested data.
		 * 
		 * @return list with requested data
		 */
		function get dataList(): IList
		
		/**
		 * Returns <code>DataProgress</code> of this <code>IDataService</code>.
		 * 
		 * @return progress of this <code>IDataService</code>
		 */
		function get dataProgress(): DataProgress;
		
		/**
		 * Returns <code>IServiceRequest</code> of this <code>IDataService</code>.
		 * 
		 * @return request of this <code>IDataService</code>
		 */
		function get serviceRequest(): IServiceRequest;
		
		/**
		 * Returns state of this <code>IDataService</code>.
		 * 
		 * @see com.addicted2flash.data.service.ServiceState
		 * @return state of <code>IDataService</code>
		 */
		function get state(): String
		
		/**
		 * Returns http status of this <code>IDataService</code>.
		 * 
		 * @return http status of this <code>IDataService</code>
		 */
		function get httpStatus(): int;
		
		/**
		 * Returns error message of this <code>IDataService</code>.
		 * 
		 * @return error message of this <code>IDataService</code>
		 */
		function get errorMessage(): String;
	}
}
