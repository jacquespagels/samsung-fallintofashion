/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.data.loader 
{
	import com.addicted2flash.data.DataEventType;
	import com.addicted2flash.data.DataProgress;
	import com.addicted2flash.data.loader.ILoaderStrategy;
	import com.addicted2flash.data.service.IDataService;
	import com.addicted2flash.data.service.ServiceState;
	
	import flash.errors.IllegalOperationError;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.events.SecurityErrorEvent;		

	/**
	 * This class provides a skeletal implementation of the ILoaderStrategy interface to minimize 
	 * the effort required to implement this interface. 
	 * 
	 * @author Tim Richter
	 */
	public class AbstractLoaderStrategy implements ILoaderStrategy 
	{
		protected var _dataService: IDataService;
		protected var _dispatcher: EventDispatcher;
		protected var _dataProgress: DataProgress;
		protected var _state: String;
		protected var _httpStatus: int;
		protected var _errorMessage: String;

		/**
		 * Create a new <code>AbstractLoaderStrategy</code>.
		 */
		public function AbstractLoaderStrategy() 
		{
			_state = ServiceState.READY;
		}

		/**
		 * @inheritDoc
		 */
		public function start(): void
		{
			_state = ServiceState.RUNNING;
		}
		
		/**
		 * @inheritDoc
		 */
		public function stop(): void
		{
			_state = ServiceState.STOP;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get data(): *
		{
			throw new IllegalOperationError( "abstract method needs to be overridden in subclasses!" );
		}
		
		/**
		 * @inheritDoc
		 */
		public function get dataProgress(): DataProgress
		{
			return _dataProgress;
		}
		
		/**
		 * @inheritDoc
		 */
		public function set dataService( dataService: IDataService ): void
		{
			_dataService = dataService;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get state(): String
		{
			return _state;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get httpStatus(): int
		{
			return _httpStatus;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get errorMessage(): String
		{
			return _errorMessage;
		}

		/**
		 * @inheritDoc
		 */
		public function dispose(): void
		{
			removeListener();
			
			_dataService = null;
			_dataProgress = null;
			_dispatcher = null;
			_state = null;
		}

		/**
		 * every subclass of this abstract class has to register there specific loader
		 * as an eventdispatcher (necessary for adding and removing from events).
		 * 
		 * @param dispatcher <code>EventDispatcher</code> (f.e. <code>URLLoader</code> or <code>Loader.contentLoaderInfo</code>)
		 */
		protected function registerDispatcher( dispatcher: EventDispatcher ): void
		{
			_dispatcher = dispatcher;
			
			addListener();
		}
		
		/**
		 * @private
		 */
		protected function onComplete( event: Event ): void
		{
			removeListener();
			
			_state = ServiceState.COMPLETE;
			
			_dataService.notifyObservers( DataEventType.COMPLETE );
		}
		
		/**
		 * @private
		 */
		protected function onProgress( event: ProgressEvent ): void
		{
			if( _dataProgress == null )
			{
				_dataProgress = new DataProgress( event.bytesLoaded, event.bytesTotal );
			}
			else
			{
				_dataProgress.bytesLoaded = event.bytesLoaded;
				_dataProgress.bytesTotal = event.bytesTotal;
			}
			
			_dataService.notifyObservers( DataEventType.PROGRESS );
		}
		
		/**
		 * @private
		 */
		protected function onIOError( event: IOErrorEvent ): void
		{
			removeListener();
			
			_errorMessage = event.text;
			
			_state = ServiceState.COMPLETE;
			
			_dataService.notifyObservers( DataEventType.IO_ERROR );
		}
		
		/**
		 * @private
		 */
		private function onHTTPStatus( event: HTTPStatusEvent ): void
		{			
			_httpStatus = event.status;
			
			_dataService.notifyObservers( DataEventType.HTTP_STATUS );
		}
		
		/**
		 * @private
		 */
		private function onSecurityError( event: SecurityErrorEvent ): void
		{
			removeListener();
			
			_errorMessage = event.text;
			
			_state = ServiceState.COMPLETE;
			
			_dataService.notifyObservers( DataEventType.SECURITY_ERROR );
		}
		
		/**
		 * @private
		 */
		protected function addListener(): void
		{
			_dispatcher.addEventListener( Event.COMPLETE, onComplete );
			_dispatcher.addEventListener( ProgressEvent.PROGRESS, onProgress );
			_dispatcher.addEventListener( IOErrorEvent.IO_ERROR, onIOError );
			_dispatcher.addEventListener( HTTPStatusEvent.HTTP_STATUS, onHTTPStatus );
			_dispatcher.addEventListener( SecurityErrorEvent.SECURITY_ERROR, onSecurityError );
		}

		/**
		 * @private
		 */
		protected function removeListener(): void
		{
			_dispatcher.removeEventListener( Event.COMPLETE, onComplete );
			_dispatcher.removeEventListener( ProgressEvent.PROGRESS, onProgress );
			_dispatcher.removeEventListener( IOErrorEvent.IO_ERROR, onIOError );
			_dispatcher.removeEventListener( HTTPStatusEvent.HTTP_STATUS, onHTTPStatus );
			_dispatcher.removeEventListener( SecurityErrorEvent.SECURITY_ERROR, onSecurityError );
		}
	}
}
