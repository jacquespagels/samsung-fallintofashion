/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.data.loader 
{
	import com.addicted2flash.data.DataEventType;
	import com.addicted2flash.data.loader.ILoaderStrategy;
	import com.addicted2flash.data.service.IDataService;
	import com.addicted2flash.data.service.IDataServiceObserver;
	import com.addicted2flash.data.service.IManageableDataService;
	import com.addicted2flash.data.service.LoaderQueueService;
	import com.addicted2flash.data.service.ServiceRequest;
	import com.addicted2flash.data.service.ServiceState;
	import com.addicted2flash.util.ArraySet;
	import com.addicted2flash.util.IQueue;
	import com.addicted2flash.util.ISet;
	import com.addicted2flash.util.SortedArrayQueue;
	import com.addicted2flash.util.comparator.DESCDataServicePriorityComparator;
	
	import flash.net.URLRequest;		

	/**
	 * This class represents a FIFO implementation of <code>IDataService</code>s.
	 * With this class it's possible to create and process services application wide in a queue.
	 * Also it is possible to pause, stop and resume the process.
	 * 
	 * @author Tim Richter
	 */
	public class LoaderQueue implements IDataServiceObserver
	{
		private static const MAX_ACTIVE_SERVICES: int = 3;
		private static const INSTANCE: LoaderQueue = new LoaderQueue( );
		private static const SERVICE_QUEUE: IQueue = new SortedArrayQueue( new DESCDataServicePriorityComparator( ) );
		private static const ACTIVE_SERVICES: ISet = new ArraySet( );
		
		private static var _state: String = ServiceState.READY;
		
		
		/**
		 * return <code>ILoaderQueueService</code>. <code>IObserver</code> can be added for notification of data-events.
		 * 
		 * @param loaderStrategy <code>ILoaderStrategy</code> for different loading procedures (f.e. bitmaps or xml)
		 * @param request <code>URLRequest</code>
		 * @param anchor (optional) any data (f.e. data binded to the request)
		 * @param priority (optional) priority
		 * 
		 * @return loader service
		 */
		public static function createService( loaderStrategy: ILoaderStrategy, request: URLRequest, anchor: * = null, priority: int = 0 ): IDataService
		{
			var service: IManageableDataService = new LoaderQueueService( loaderStrategy, new ServiceRequest( null, request ), anchor, priority );
			
			SERVICE_QUEUE.add( service );
			
			processService( );
			
			return service;
		}

		/**
		 * remove specific <code>IDataService</code> from the queue and stop the process of the
		 * specific service that is running.
		 * 
		 * @param service queued <code>IDataService</code>
		 */
		public static function removeService( service: IDataService ): void
		{
			if( service is IManageableDataService )
			{
				IManageableDataService( service ).dispose();
			}
			
			if( ACTIVE_SERVICES.remove( service ) )
			{
				processService( );
			}
			else
			{
				SERVICE_QUEUE.remove( service );
			}
		}

		/**
		 * stop the loading progress of active services and remove all <code>IDataService</code>s from the queue.
		 */
		public static function stop(): void
		{
			for( var i: int = 0; i < MAX_ACTIVE_SERVICES; ++i )
			{
				IManageableDataService( ACTIVE_SERVICES.getAt( i ) ).dispose();
			}
			
			ACTIVE_SERVICES.clear( );
			SERVICE_QUEUE.clear( );
			
			_state = ServiceState.READY;
		}

		/**
		 * pause the loading progress of active services.
		 */
		public static function pause(): void
		{
			if( _state != ServiceState.PAUSE )
			{
				for( var i: int = 0; i < MAX_ACTIVE_SERVICES; ++i )
				{
					IManageableDataService( ACTIVE_SERVICES.getAt( i ) ).stop();
				}
				
				_state = ServiceState.PAUSE;
			}
		}

		/**
		 * resume the loading progress of active services.
		 */
		public static function resume(): void
		{
			if( _state != ServiceState.RUNNING )
			{
				for( var i: int = 0; i < MAX_ACTIVE_SERVICES; ++i )
				{
					IManageableDataService( ACTIVE_SERVICES.getAt( i ) ).start();
				}
				
				_state = ServiceState.RUNNING;
			}
		}

		/**
		 * return state of <code>LoaderQueue</code>.
		 * 
		 * @see com.addicted2flash.data.service.ServiceState
		 * @return state of <code>LoaderQueue</code>
		 */
		public static function get state(): String
		{
			return _state;
		}

		/**
		 * @private
		 */
		private static function processService(): void
		{
			if( ( ACTIVE_SERVICES.size() < MAX_ACTIVE_SERVICES ) && ( _state != ServiceState.PAUSE ) && ( !SERVICE_QUEUE.isEmpty( ) ) )
			{
				var len: int = Math.abs( ACTIVE_SERVICES.size( ) - MAX_ACTIVE_SERVICES );
				var size: int = SERVICE_QUEUE.size( );
				var service: IManageableDataService;
				
				for( var i: int = 0; i < len && size != 0 ; ++i, --size )
				{
					service = IManageableDataService( SERVICE_QUEUE.poll( ) );
					
					ACTIVE_SERVICES.add( service );
					
					service.addObserver( INSTANCE );
					
					service.start( );
				}
				
				_state = ServiceState.RUNNING;
			}
		}

		/**
		 * @private
		 */
		public function dataServiceUpdate( type: String, service: IDataService ): void
		{
			service.removeObserver( INSTANCE );
			
			ACTIVE_SERVICES.remove( service );
			
			processService( );
		}
		
		/**
		 * @private
		 */
		public function get acceptedDataEvents(): Array
		{
			return [ DataEventType.COMPLETE, DataEventType.IO_ERROR, DataEventType.SECURITY_ERROR ];
		}
	}
}