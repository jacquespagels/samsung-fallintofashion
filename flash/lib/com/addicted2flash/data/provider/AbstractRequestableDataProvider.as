/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.data.provider
{
	import com.addicted2flash.data.DataEventType;
	import com.addicted2flash.data.Range;
	import com.addicted2flash.data.provider.IRequestableDataProvider;
	import com.addicted2flash.data.service.IDataService;
	import com.addicted2flash.data.service.IDataServiceObserver;
	import com.addicted2flash.data.service.IManageableDataService;
	import com.addicted2flash.data.service.IServiceRequest;
	import com.addicted2flash.data.service.ServiceRequest;
	import com.addicted2flash.util.ArrayList;
	import com.addicted2flash.util.ArrayQueue;
	import com.addicted2flash.util.ICloneable;
	import com.addicted2flash.util.ICollection;
	import com.addicted2flash.util.IList;
	import com.addicted2flash.util.IQueue;
	import com.addicted2flash.util.Observable;
	import com.addicted2flash.util.iterator.IIterator;
	
	import flash.errors.IllegalOperationError;		

	/**
	 * This class provides a skeletal implementation of the IRequestableDataProvider interface to minimize
	 * the effort required to implement this interface.
	 *
	 * @author Tim Richter
	 */
	public class AbstractRequestableDataProvider extends Observable implements IRequestableDataProvider, IDataServiceObserver
	{
		protected var _list: IList;
		protected var _requestQueue: IQueue;
		protected var _currentService: IManageableDataService;
		protected var _pageSize: int;
		protected var _fromPage: int;
		protected var _toPage: int;
		protected var _isReady: Boolean;

		/**
		 * Create a new <code>AbstractRequestableDataProvider</code>.
		 *
		 * @param pageSize page size
		 */
		public function AbstractRequestableDataProvider( pageSize: int )
		{
			_pageSize = pageSize;
			_list = new ArrayList();
			_requestQueue = new ArrayQueue();
		}

		/**
		 * @inheritDoc
		 */
		public function getAt( i: int ): *
		{
			return _list.getAt( i );
		}

		/**
		 * @inheritDoc
		 */
		public function contains( o: * ): Boolean
		{
			return _list.contains( o );
		}

		/**
		 * @inheritDoc
		 */
		public function isEmpty(): Boolean
		{
			return _list.isEmpty();
		}

		/**
		 * @inheritDoc
		 */
		public function size(): int
		{
			return _list.size();
		}

		/**
		 * @inheritDoc
		 */
		public function toArray(): Array
		{
			return _list.toArray();
		}

		/**
		 * @inheritDoc
		 */
		public function iterator(): IIterator
		{
			return _list.iterator();
		}
		
		/**
		 * @inheritDoc
		 */
		public function request( range: Range ): Boolean
		{
			if( isAreaRequested( range ) || isAreaLoaded( range ) )
			{
				return false;
			}
			else
			{
				// -- _fromPage is calculated in isAreaLoaded() (no second loop is needed for calculating _fromPage)
				_toPage = Math.ceil( range.end / _pageSize );
				
				var i: int = _fromPage;
				
				for( ; i < _toPage ; ++i )
				{
					_requestQueue.add( createDataProviderService( new ServiceRequest( new Range( i * _pageSize, ( i + 1 ) * _pageSize ) ) ) );
				}
				
				processServices();
				
				return true;
			}
		}

		/**
		 * @inheritDoc
		 */
		public function isAreaLoaded( range: Range ): Boolean
		{
			var start: int = range.start;
			
			// -- this calculation is placed here, because of performance task
			// -- instead there could be another loop for calculating _fromPage
			_fromPage = int( start / _pageSize );
			
			var len: int = range.end;
			var j: int = start;
			
			for( ; j < len ; ++j )
			{
				if( _list.getAt( j ) == null )
				{
					return false;
				}
				
				// -- to save data-requests that has already been executed
				if( j > 0 && j > start && j % _pageSize == 0 )
				{
					++_fromPage;
				}
			}
			
			return true;
		}
		
		/**
		 * @inheritDoc
		 */
		public function isAreaRequested( range: Range ): Boolean
		{
			var len: int = _requestQueue.size();
			
			while( --len > -1 )
			{
				if( range.isSubsetOf( IManageableDataService( _requestQueue.getAt( len ) ).serviceRequest.range ) )
				{					
					return true;
				}
			}
			
			return false;
		}

		/**
		 * @inheritDoc
		 */
		public function clear(): void
		{
			_list.clear();
			_requestQueue.clear();
			_currentService = null;
		}
		
		/**
		 * @inheritDoc
		 */
		override public function clone(): ICloneable
		{
			throw new IllegalOperationError( "abstract instance cannot be cloned!" );
		}
		
		/**
		 * @inheritDoc
		 */
		public function get acceptedDataEvents(): Array
		{
			return [ DataEventType.COMPLETE, DataEventType.PROGRESS, DataEventType.IO_ERROR ];
		}
		
		/**
		 * @inheritDoc
		 */
		public function dataServiceUpdate( type: String, service: IDataService ): void
		{
			switch( type )
			{
				case DataEventType.COMPLETE:
					_isReady = true;
					updateData( service );
					processServices();
					notifyObservers( DataEventType.COMPLETE, service.serviceRequest );
					break;
				
				case DataEventType.PROGRESS:
					notifyObservers( DataEventType.PROGRESS, service.serviceRequest );
					break;
				
				case DataEventType.IO_ERROR:
					_isReady = true;
					processServices();
					notifyObservers( DataEventType.IO_ERROR, service.serviceRequest );
					break;
			}
		}

		/**
		 * factory method which returns <code>IStartableDataService</code>.
		 *
		 * @param request <code>IServiceRequest</code>
		 * @return IStartableDataService
		 */
		protected function createDataProviderService( request: IServiceRequest ): IManageableDataService
		{
			throw new IllegalOperationError( "abstract method needs to be overridden in subclasses" );
		}
		
		/**
		 * process services (adding/removing observer and starting next service).
		 */
		protected function processServices(): void
		{
			if( _isReady && !_requestQueue.isEmpty() )
			{
				_isReady = false;
				
				if( _currentService != null )
				{
					_currentService.removeObserver( this );
				}
				
				_currentService = IManageableDataService( _requestQueue.poll() );
				
				_currentService.addObserver( this );
				
				_currentService.start();
			}
		}

		/**
		 * @private
		 */
		protected function updateData( service: IDataService ): void
		{
			var data: IList = service.dataList;
			var range: Range = service.serviceRequest.range;
			var start: int = range.start;
			var end: int = range.end;
			var i: int = 0;
			
			for( ; start < end ; ++start )
			{
				_list.setAt( start, data.getAt( i++ ) );
			}
		}
	}
}
