/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.data.provider 
{
	import com.addicted2flash.data.IRequestable;
	import com.addicted2flash.util.ICloneable;
	import com.addicted2flash.util.IObservable;
	import com.addicted2flash.util.iterator.IIterator;	

	/**
	 * Interface for all data-structures that are requestable/pageable.
	 * 
	 * <code>IObserver</code> or <code>ISpecificObserver</code> can be added and will be
	 * therefore notified when data is loaded or when errors occurred during load progress.
	 * 
	 * @author Tim Richter
	 */
	public interface IRequestableDataProvider extends IRequestable, ICloneable, IObservable 
	{
		/**
		 * Returns the element at the specified position in this list.
		 * 
		 * @param i index
		 * @return * object at searched index (or null)
		 */
		function getAt( i: int ): *;
		
		/**
		 * Returns true if this list contains the specified element.
		 * 
		 * @param o *
		 * @return true if this list contains the specified element.
		 */
		function contains( o: * ): Boolean;
		
		/**
		 * Removes all of the elements from this list (optional operation).
		 */
		function clear(): void;
		
		/**
		 * Returns true if this list contains no elements.
		 * 
		 * @return true if this list contains no elements.
		 */
		function isEmpty(): Boolean;
		
		/**
		 * Returns the number of elements in this list.
		 * 
		 * @return number of elements in this list.
		 */
		function size(): int;
		
		/**
		 * Converts the collection into an array.
		 * 
		 * @return an array.
		 */
		function toArray(): Array;
		
		/**
		 * Returns an iterator over the elements in this list.
		 * 
		 * @return IIterator
		 */
		function iterator(): IIterator;
	}
}
