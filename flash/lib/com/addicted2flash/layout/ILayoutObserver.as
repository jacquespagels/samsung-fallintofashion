/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{

	/**
	 * Interface for all objects that are interested in layout events.
	 * 
	 * @author Tim Richter
	 */
	public interface ILayoutObserver 
	{
		/**
		 * Return a list with all <code>LayoutEventType</code>s.
		 * 
		 * @return a list with all <code>LayoutEventType</code>s
		 * @see LayoutEventType
		 */
		function get acceptedLayoutEvents(): Array;
		
		/**
		 * Updates <code>ILayoutComponent</code> about an specific <code>LayoutEventType</code>.
		 * 
		 * @param type type of event
		 * @param o <code>ILayoutObservable</code>
		 * @see LayoutEventType
		 */
		function layoutUpdate( type: int, o: ILayoutObservable ): void
	}
}
