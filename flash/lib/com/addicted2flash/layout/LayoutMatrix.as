/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import flash.errors.IllegalOperationError;	
	
	/**
	 * This class represents a matrix for complex layouts, specified by <code>Row</code>s and <code>Column</code>s. 
	 * <p>
	 * 
	 * @example the following code creates a new LayoutMatrix with 2 rows (the first with a pixel exact value and the 
	 * second with a normalized value). In addition these rows get 2 columns (both with a pixel exact value).
	 * <listing version="3.0">
	 * var matrix: LayoutMatrix = new LayoutMatrix();
	 * matrix.addRow( new Row( 100 ) );
	 * matrix.addColumn( new Column( 100 ) ); // -- add column to the first row
	 * matrix.addColumn( new Column( 100 ) ); // -- add column to the first row
	 * 
	 * matrix.addRow( new Row( 0.5, true ) );
	 * matrix.addColumn( new Column( 100 ) ); // -- add column to the second row
	 * matrix.addColumn( new Column( 100 ) ); // -- add column to the second row
	 * </listing>
	 * 
	 * @author Tim Richter
	 */
	public class LayoutMatrix 
	{
		private var _rows: Array;
		private var _n: int;
		
		// -- internal use ( see MatrixLayout layoutContainer() )
		private var _unspecificRows: int;
		private var _specificHeight: int;
		private var _normalizedHeight: Number;

		/**
		 * Create a new <code>LayoutMatrix</code>.
		 */
		public function LayoutMatrix()
		{
			clear();
		}

		/**
		 * Adds a <code>Row</code> to the <code>LayoutMatrix</code>.
		 * 
		 * @param row the <code>Row</code> to be added
		 * @throws IllegalOperationError if normalized height of added rows is bigger than 1
		 */
		public function addRow( row: Row ): void
		{
			_rows[ _n++ ] = row;
			
			calcAdd( row.height, row.normalizedHeight );
		}
		
		/**
		 * Adds a <code>Column</code> to the <code>LayoutMatrix</code>.
		 * 
		 * @param column the <code>Column</code> to be added
		 * @throws IllegalOperationError if normalized width of added columns is bigger than 1
		 */
		public function addColumn( column: Column ): void
		{
			if( _n == 0 )
			{
				addRow( new Row() );
			}
			
			getRowAt( _n-1 ).addColumn( column );
		}
		
		/**
		 * Removes a <code>Row</code>.
		 * 
		 * @param row the <code>Row</code> to be removed
		 */
		public function removeRow( row: Row ): void
		{
			removeRowAt( _rows.indexOf( row ) );
		}
		
		/**
		 * Removes a <code>Row</code> at a given index.
		 * 
		 * @param i index
		 * @return the removed <code>Row</code> (null if it does not exist)
		 */
		public function removeRowAt( i: int ): Row
		{
			if( i >= _n ) return null;
			
			var row: Row = _rows.splice( i, 1 )[ 0 ];
			
			if( row != null )
			{
				calcRemove( row.height, row.normalizedHeight );
				
				--_n;
				
				return row;
			}
			else
			{
				return null;
			}
		}
		
		/**
		 * Removes a <code>Column</code>.
		 * 
		 * @param column the <code>Column</code> to be removed
		 */
		public function removeColumn( column: Column ): void
		{
			var nrows: int = rowAmount;
			var n: int = 0;
			
			while( n++ < nrows && !getRowAt( n ).removeColumn( column ) ) {}
		}
		
		/**
		 * Removes a <code>Column</code> at a given index.
		 * 
		 * @param i index
		 * @return the removed <code>Column</code> (null if it does not exist)
		 */
		public function removeColumnAt( i: int ): Column
		{
			var nrows: int = rowAmount;
			var ncols: int = 0;
			var n: int = 0;
			
			var row: Row;
			
			while( n < nrows )
			{
				row = getRowAt( n );
				
				ncols += row.columnAmount;
				
				if( ncols > i )
				{
					return row.removeColumnAt( ncols - i );
				}
				
				++n;
			}
			
			return null;
		}

		/**
		 * Returns the <code>Row</code>, specified by index.
		 * 
		 * @param i index
		 * @return the <code>Row</code>, specified by index (null if it does not exist)
		 */
		public function getRowAt( i: int ): Row
		{
			return _rows[ i ];
		}
		
		/**
		 * Sets a new height of a <code>Row</code>, specified by index.
		 * 
		 * @param i index
		 * @param height (optional) height of the <code>Row</code>
		 * @param normalizedHeight (optional) flag whether height should be calculated in pixel or normalized values
		 */
		public function setRowHeightAt( i: int, height: Number, normalizedHeight: Boolean = false ): void
		{
			if( i < rowAmount ) setRowHeight( getRowAt( i ), height, normalizedHeight );
		}

		/**
		 * Sets a new height of a given <code>Row</code>.
		 * 
		 * @param row <code>Row</code>
		 * @param height (optional) height of the <code>Row</code>
		 * @param normalizedHeight (optional) flag whether height should be calculated in pixel or normalized values
		 */
		public function setRowHeight( row: Row, height: Number, normalizedHeight: Boolean = false ): void
		{
			if( row != null )
			{
				calcRemove( row.height, row.normalizedHeight );
				calcAdd( height, normalizedHeight );
				
				row.setHeight( height, normalizedHeight );
			}
		}
		
		/**
		 * Sets a new width of a <code>Column</code>, specified by index.
		 * 
		 * @param i index
		 * @param width (optional) width of the <code>Column</code>
		 * @param normalizedWidth (optional) flag whether width should be calculated in pixel or normalized values
		 */
		public function setColumnWidthAt( i: int, width: Number, normalizedWidth: Boolean = false ): void
		{
			setColumnWidth( getColumnAt( i ), width, normalizedWidth );
		}

		/**
		 * Sets a new width of a given <code>Column</code>.
		 * 
		 * @param c <code>Column</code>
		 * @param width (optional) width of the <code>Column</code>
		 * @param normalizedWidth (optional) flag whether width should be calculated in pixel or normalized values
		 */
		public function setColumnWidth( c: Column, width: Number, normalizedWidth: Boolean = false ): void
		{
			if( c != null ) c.parentRow.setColumnWidth( c, width, normalizedWidth );
		}
		
		/**
		 * Returns the <code>Column</code>, specified by index.
		 * 
		 * @param i index
		 * @return the <code>Column</code>, specified by index (null if it does not exist)
		 */
		public function getColumnAt( i: int ): Column
		{
			var nrows: int = rowAmount;
			var ncols: int = 0;
			var n: int = 0;
			
			var row: Row;
			
			while( n < nrows )
			{
				row = getRowAt( n );
				
				ncols += row.columnAmount;
				
				if( ncols > i )
				{
					return row.getColumnAt( ncols - i );
				}
				
				++n;
			}
			
			return null;
		}

		/**
		 * Returns total amount of rows in the <code>LayoutMatrix</code>.
		 * 
		 * @return total amount of rows in the <code>LayoutMatrix</code>
		 */
		public function get rowAmount(): int
		{
			return _n;
		}

		/**
		 * Returns total amount of columns in the <code>LayoutMatrix</code>.
		 * 
		 * @return total amount of columns in the <code>LayoutMatrix</code>
		 */
		public function get columnAmount(): int
		{
			var len: int = _n;
			var i: int = 0;
			
			while( --len > -1 )
			{
				i += Row( _rows[ len ] ).columnAmount;
			}
			
			return i;
		}
		
		/**
		 * Remove all <code>Row</code>s and <code>Column</code>s.
		 */
		public function clear(): void
		{
			_n = 0;
			_normalizedHeight = 0;
			_specificHeight = 0;
			_unspecificRows = 0;
			_rows = [];
		}
		
		/**
		 * String representation of <code>LayoutMatrix</code>.
		 * 
		 * @return String representation of <code>LayoutMatrix</code>
		 */
		public function toString(): String
		{
			return "[ LayoutMatrix rows=" + rowAmount + " columns=" + columnAmount + " ]";
		}
		
		/**
		 * @private
		 */
		internal function get unspecificRowAmount(): int
		{
			return _unspecificRows;
		}
		
		/**
		 * @private
		 */
		internal function get specificHeight(): int
		{
			return _specificHeight;
		}
		
		/**
		 * @private
		 */
		internal function get normalizedHeight(): Number
		{
			return _normalizedHeight;
		}
		
		
		private function calcRemove( height: Number, normalizedHeight: Boolean ): void
		{
			if( height == -1 )
			{
				--_unspecificRows;
			}
			else if( !normalizedHeight )
			{
				_specificHeight -= height;
			}
			else
			{
				_normalizedHeight -= height;
			}
		}
		
		private function calcAdd( height: Number, normalizedHeight: Boolean ): void
		{
			if( height == -1 )
			{
				++_unspecificRows;
			}
			else if( !normalizedHeight )
			{
				_specificHeight += height;
			}
			else
			{
				_normalizedHeight += height;
				
				if( _normalizedHeight > 1 )
				{
					throw new IllegalOperationError( "normalized height of added rows is bigger than 1!" );
				}
			}
		}
	}
}