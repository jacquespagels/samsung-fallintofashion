/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import com.addicted2flash.util.IDisposable;	
	
	import flash.geom.Rectangle;	

	/**
	 * Interface for all classes that want to be layouted.
	 * 
	 * @author Tim Richter
	 */
	public interface ILayoutComponent extends IDisposable
	{
		/**
		 * Returns the alignment along the x axis.
		 * <p>The value should be a number between 0 and 1 where 0 represents alignment along 
		 * the origin, 1 is aligned the furthest away from the origin, 0.5 is centered, etc.</p>
		 * 
		 * @return alignment along the x axis
		 */
		function get alignmentX(): Number;
		
		/**
		 * Returns the alignment along the y axis.
		 * <p>The value should be a number between 0 and 1 where 0 represents alignment along 
		 * the origin, 1 is aligned the furthest away from the origin, 0.5 is centered, etc.</p>
		 * 
		 * @return alignment along the y axis
		 */
		function get alignmentY(): Number;
		
		/**
		 * Returns the preferred size of this <code>ILayoutComponent</code>.
		 * 
		 * @return the preferred size of this <code>ILayoutComponent</code>
		 */
		function get preferredSize(): Size;
		
		/**
		 * Sets the preferred size of this <code>ILayoutComponent</code>.
		 * 
		 * @param size the preferred size of this <code>ILayoutComponent</code>
		 */
		function set preferredSize( size: Size ): void;
		
		/**
		 * Returns the minimum size of this <code>ILayoutComponent</code>.
		 * 
		 * @return the minimum size of this <code>ILayoutComponent</code>
		 */
		function get minimumSize(): Size;
		
		/**
		 * Sets the minimum size of this <code>ILayoutComponent</code>.
		 * 
		 * @param size the minimum size of this <code>ILayoutComponent</code>
		 */
		function set minimumSize( size: Size ): void;
		
		/**
		 * Returns the cropped <code>Size</code> of this <code>ILayoutComponent</code>.
		 * 
		 * @return the cropped <code>Size</code> of this <code>ILayoutComponent</code>
		 */
		function get croppedSize(): Size;
		
		/**
		 * Set the cropped <code>Size</code> of this <code>ILayoutComponent</code>.
		 * 
		 * @param size the cropped <code>Size</code> of this <code>ILayoutComponent</code>
		 */
		function set croppedSize( size: Size ): void;
		
		/**
		 * Returns the bounds of this <code>ILayoutComponent</code>.
		 * 
		 * @return the bounds of this <code>ILayoutComponent</code>
		 */
		function get layoutBounds(): Rectangle;
		
		/**
		 * Sets the bounds of this <code>ILayoutComponent</code>.
		 * <p>NOTE: to avoid instantiation of new <code>Rectangle</code> objects, the existing bounds 
		 * will be readjusted.</p>
		 * 
		 * @param bounds the bounds of this <code>ILayoutComponent</code>
		 */
		function set layoutBounds( bounds: Rectangle ): void;
		
		/**
		 * Returns parent <code>ILayoutableContainer</code>.
		 * 
		 * @return parent <code>ILayoutableContainer</code>
		 */
		function get parentContainer(): ILayoutContainer;
		
		/**
		 * Sets parent <code>ILayoutableContainer</code>.
		 * 
		 * @param parent parent <code>ILayoutableContainer</code>
		 */
		function set parentContainer( parent: ILayoutContainer ): void;
	}
}
