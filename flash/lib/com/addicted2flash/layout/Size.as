/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import flash.errors.IllegalOperationError;	
	
	import com.addicted2flash.util.ICloneable;	
	
	/**
	 * The Size class encapsulates the width and height of a component. 
	 * 
	 * It is possible to specify the width and height in normalized values or in pixel values. 
	 * 
	 * @author Tim Richter
	 */
	public class Size implements ICloneable
	{
		public static const UNSPECIFIED: Size = Size.createUnspecifiedSize();
		
		protected var _width: Number;
		protected var _height: Number;
		
		protected var _normalizedWidth: Boolean;
		protected var _normalizedHeight: Boolean;
		
		protected var _isUnspecified: Boolean = false;
		
		/**
		 * Create a new <code>Size<code>.
		 * 
		 * @param width (optional) width
		 * @param height (optional) height
		 * @param normalizedWidth (optional) flag whether width should be calculated in pixel or normalized values
		 * @param normalizedHeight (optional) flag whether height should be calculated in pixel or normalized values
		 */
		public function Size( width: Number = -1, height: Number = -1, normalizedWidth: Boolean = false, normalizedHeight: Boolean = false )
		{
			_width = width;
			_height = height;
			
			_normalizedWidth = normalizedWidth;
			_normalizedHeight = normalizedHeight;
		}

		/**
		 * The union of a given <code>Size</code> and this <code>Size</code> (biggest width/height of both).
		 * NOTE: union is only build if both sizes specified the same calculation type (f.e. normalized values)
		 * 
		 * @param d <code>Size</code>
		 */
		public function union( d: Size ): void
		{
			if( normalizedWidth == d.normalizedWidth ) _width = d.width;
			if( normalizedHeight == d.normalizedHeight ) _height = d.height;
		}
		
		/**
		 * The union of a given <code>Size</code> and this <code>Size</code> (biggest width/height of both).
		 * NOTE: union is only build if both sizes specified the same calculation type (f.e. normalized values)
		 * 
		 * @param d <code>Size</code>
		 * @return the union <code>Size</code>
		 */
		public function getUnion( d: Size ): Size
		{
			var s: Size = Size( clone() );
			s.union( d );
			
			return s;
		}
		
		/**
		 * The union of a given <code>Size</code> and this <code>Size</code> (biggest width/height of both).
		 * NOTE: difference is only build if both sizes specified the same calculation type (f.e. normalized values)
		 * and if this <code>Size</code> is bigger than the given <code>Size</code>
		 * 
		 * @param d <code>Size</code>
		 */
		public function difference( d: Size ): void
		{
			if( normalizedWidth == d.normalizedWidth && _width > d.width ) _width -= d.width;
			if( normalizedHeight == d.normalizedHeight && _height > d.height ) _height -= d.height;
		}
		
		/**
		 * The union of a given <code>Size</code> and this <code>Size</code> (biggest width/height of both).
		 * NOTE: difference is only build if both sizes specified the same calculation type (f.e. normalized values)
		 * and if this <code>Size</code> is bigger than the given <code>Size</code>
		 * 
		 * @param d <code>Size</code>
		 * @return the difference <code>Size</code>
		 */
		public function getDifference( d: Size ): Size
		{
			var s: Size = Size( clone() );
			s.difference( d );
			
			return s;
		}
		
		/**
		 * Returns true if <code>this</code> and the given <code>Size</code> are equal.
		 * 
		 * @param d <code>Size</code>
		 * @return true if <code>this</code> and the given <code>Size</code> are equal
		 */
		public function equals( d: Size ): Boolean
		{
			return ( normalizedWidth == d.normalizedWidth && 
					 normalizedHeight == d.normalizedHeight && 
					 width == d.width && 
					 height == d.height );
		}
		
		/**
		 * Returns the width of the <code>Size</code>.
		 * 
		 * @return width of the <code>Size</code>
		 */
		public function get width(): Number
		{
			return _width;
		}
		
		/**
		 * Sets the width of the <code>Size</code>.
		 * 
		 * @param width width of the <code>Size</code>
		 * @throws IllegalOperationError if UNSPECIFIED size are used (cannot be modified)
		 */
		public function set width( width: Number ): void
		{
			if( _isUnspecified ) throw new IllegalOperationError( "cannot be setted in UNSPECIFIED Size!" );
			
			_width = width;
		}
		
		/**
		 * Returns the height of the <code>Size</code>.
		 * 
		 * @return height of the <code>Size</code>
		 */
		public function get height(): Number
		{
			return _height;
		}
		
		/**
		 * Sets the height of the <code>Size</code>.
		 * 
		 * @param height height of the <code>Size</code>
		 * @throws IllegalOperationError if UNSPECIFIED size are used (cannot be modified)
		 */
		public function set height( height: Number ): void
		{
			if( _isUnspecified ) throw new IllegalOperationError( "cannot be setted in UNSPECIFIED Size!" );
			
			_height = height;
		}
		
		/**
		 * Returns true if width has been setted to a normalized value. Otherwise pixel values has been setted.
		 * 
		 * @return true if width has been setted to a normalized value. Otherwise pixel values has been setted.
		 */
		public function get normalizedWidth(): Boolean
		{
			return _normalizedWidth;
		}
		
		/**
		 * Sets the type of internal calculation (<code>true</code> = a normalized value; <code>false</code> = pixel values). 
		 * 
		 * @param normalizedWidth type of internal calculation (<code>true</code> = a normalized value; <code>false</code> = pixel values)
		 * @throws IllegalOperationError if UNSPECIFIED size are used (cannot be modified)
		 */
		public function set normalizedWidth( normalizedWidth: Boolean ): void
		{
			if( _isUnspecified ) throw new IllegalOperationError( "cannot be setted in UNSPECIFIED Size!" );
			
			_normalizedWidth = normalizedWidth;
		}
		
		/**
		 * Returns true if height has been setted to a normalized value. Otherwise pixel values has been setted.
		 * 
		 * @return true if height has been setted to a normalized value. Otherwise pixel values has been setted.
		 */
		public function get normalizedHeight(): Boolean
		{
			return _normalizedHeight;
		}
		
		/**
		 * Sets the type of internal calculation (<code>true</code> = a normalized value; <code>false</code> = pixel values). 
		 * 
		 * @param normalizedHeight type of internal calculation (<code>true</code> = a normalized value; <code>false</code> = pixel values)
		 * @throws IllegalOperationError if UNSPECIFIED size are used (cannot be modified)
		 */
		public function set normalizedHeight( normalizedHeight: Boolean ): void
		{
			if( _isUnspecified ) throw new IllegalOperationError( "cannot be setted in UNSPECIFIED Size!" );
			
			_normalizedHeight = normalizedHeight;
		}
		
		/**
		 * @inheritDoc
		 */
		public function clone(): ICloneable
		{
			return new Size( _width, _height, _normalizedWidth, _normalizedHeight );
		}
		
		/**
		 * String representation of <code>Size</code>.
		 * 
		 * @return String representation of <code>Size</code>
		 */
		public function toString(): String
		{
			return "[ Size width=" + width + " height=" + height + " normalizedWidth=" + normalizedWidth + " normalizedHeight=" + normalizedHeight + " ]";
		}
		
		/**
		 * @private
		 */
		private static function createUnspecifiedSize(): Size
		{
			var s: Size = new Size();
			s._isUnspecified = true;
			
			return s;
		}
	}
}
