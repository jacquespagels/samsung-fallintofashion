/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{

	/**
	 * <code>ILayoutComponent</code>s added to a <code>ILayoutContainer</code> are tracked in a list. The order of the list will 
	 * define the <code>ILayoutComponent</code>' front-to-back stacking order within the <code>ILayoutContainer</code>. If no index 
	 * is specified when adding a <code>ILayoutComponent</code> to a <code>ILayoutContainer</code>, it will be 
	 * added to the end of the list (and hence to the bottom of the stacking order).
	 * 
	 * @author Tim Richter
	 */
	public interface ILayoutContainer extends ILayoutComponent
	{
		/**
		 * Adds the specified component to the end of this container.
		 * 
		 * @param c <code>ILayoutComponent</code>
		 * @return the added <code>ILayoutComponent</code>
		 */
		function addComponent( c: ILayoutComponent ): ILayoutComponent;
		
		/**
		 * Adds the specified <code>ILayoutComponent</code>, specified by index, to this <code>ILayoutContainer</code>.
		 * <p>NOTE: if an <code>ILayoutComponent</code> already exist at the specified index, it will be removed.</p>
		 * 
		 * @param index the position at which to insert the <code>ILayoutComponent</code>
		 * @param c <code>ILayoutComponent</code>
		 * @return the added <code>ILayoutComponent</code> (not the removed one at the given index!)
		 */
		function addComponentAt( index: int, c: ILayoutComponent ): ILayoutComponent;
		
		/**
		 * Removes the specified <code>ILayoutComponent</code> from this <code>ILayoutContainer</code>.
		 * 
		 * @param c the component to be removed
		 * @return the removed <code>ILayoutComponent</code>
		 */
		function removeComponent( c: ILayoutComponent ): ILayoutComponent;
		
		/**
		 * Removes the <code>ILayoutComponent</code>, specified by index, from this <code>ILayoutContainer</code>.
		 * 
		 * @param index the index of the component to be removed.
		 * @return null if ther is no <code>ILayoutComponent</code> at the given index. Otherwise the removed <code>ILayoutComponent</code>
		 */
		function removeComponentAt( index: int ): ILayoutComponent;
		
		/**
		 * Returns the <code>ILayoutComponent</code>, specified by index, in this <code>ILayoutContainer</code>.
		 * 
		 * @param index index
		 * @return the <code>ILayoutComponent</code> at the specified index
		 */
		function getComponentAt( index: int ): ILayoutComponent;
		
		/**
		 * Locates the <code>ILayoutComponent</code> that contains the x,y position.
		 * 
		 * @param x x-position
		 * @param y y-position
		 * @return null if there is no child <code>ILayoutComponent</code> at the requested point. Otherwise the top-most child is returned.
		 */
		function findComponentAt( x: Number, y: Number ): ILayoutComponent;
		
		/**
		 * Returns the number of <code>ILayoutComponent</code> in this <code>ILayoutContainer</code>.
		 * 
		 * @return the number of <code>ILayoutComponent</code> in this <code>ILayoutContainer</code>
		 */
		function get componentAmount(): int;
		
		/**
		 * Returns the <code>Padding</code> of this <code>ILayoutContainer</code>, which indicate the size of the container's border.
		 * 
		 * @return the padding of this <code>ILayoutContainer</code>
		 */
		function get padding(): Padding;
		
		/**
		 * Sets the <code>Padding</code> of this <code>ILayoutContainer</code>, which indicate the size of the container's border.
		 * 
		 * @param padding the padding of this <code>ILayoutContainer</code>
		 */
		function set padding( padding: Padding ): void;
		
		/**
		 * Returns the layout strategy for this <code>ILayoutContainer</code>.
		 * 
		 * @return the layout strategy for this <code>ILayoutContainer</code>
		 */
		function get layout(): ILayoutStrategy;
		
		/**
		 * Sets the layout strategy for this <code>ILayoutContainer</code>.
		 * 
		 * @param layoutStrategy the layout strategy for this <code>ILayoutContainer</code>
		 */
		function set layout( layoutStrategy: ILayoutStrategy ): void;
				
		/**
		 * Validates this <code>ILayoutContainer</code> and all of its subcomponents.
		 * <p>The validate method is used to cause a <code>ILayoutContainer</code> to lay out its subcomponents again. 
		 * It should be invoked when this <code>ILayoutContainer</code>'s subcomponents are modified (added to or 
		 * removed from the <code>ILayoutContainer</code>)</p>
		 */
		function validate(): void;
		
		/**
		 * Forces the root <code>ILayoutContainer</code> to validate itself and all of its subcomponents.
		 */
		function validateTree(): void;
	}
}
