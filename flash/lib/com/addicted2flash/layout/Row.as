/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import flash.errors.IllegalOperationError;	
	
	/**
	 * This class represents a <code>Row</code> in a <code>LayoutMatrix</code>.
	 * 
	 * @author Tim Richter
	 */
	public class Row 
	{
		private var _columns: Array;
		private var _n: int;
		private var _height: Number;
		private var _normalizedHeight: Boolean;
		
		// -- internal use ( see MatrixLayout layoutContainer() )
		private var _unspecificColumns: int;
		private var _specificWidth: int;
		private var _normalizedWidth: Number;
		
		/**
		 * Create a new <code>Row</code>.
		 * 
		 * @param height (optional) height of the <code>Row</code>
		 * @param normalizedHeight (optional) flag whether height should be calculated in pixel or normalized values
		 */
		public function Row( height: Number = -1, normalizedHeight: Boolean = false )
		{
			_height = height;
			_normalizedHeight = normalizedHeight;
			
			_columns = [];
			_n = 0;
			_specificWidth = 0;
			_normalizedWidth = 0;
		}

		/**
		 * Returns the height of the <code>Row</code>.
		 * 
		 * @return the height of the <code>Row</code>
		 */
		public function get height(): Number
		{
			return _height;
		}
		
		/**
		 * Returns true if height is specified as a normalized value. Otherwise it is specified as a pixel value.
		 * 
		 * @return true if height is specified as a normalized value. Otherwise it is specified as a pixel value
		 */
		public function get normalizedHeight(): Boolean
		{
			return _normalizedHeight;
		}
		
		/**
		 * Adds a <code>Column</code>.
		 * 
		 * @param c the <code>Column</code> to be added
		 */
		public function addColumn( c: Column ): void
		{
			c.parentRow = this;
			
			_columns[ _n++ ] = c;
			
			calcAdd( c.width, c.normalizedWidth );
		}
		
		/**
		 * Returns <code>Column</code> at a given index.
		 * 
		 * @param i index
		 * @return the <code>Column</code>, specified by index (null if it does not exist)
		 */
		public function getColumnAt( i: int ): Column
		{
			return _columns[ i ];
		}
		
		/**
		 * Removes a <code>Column</code>.
		 * 
		 * @param column the <code>Column</code> to be removed
		 */
		public function removeColumn( c: Column ): Boolean
		{
			return removeColumnAt( _columns.indexOf( c ) ) != null;
		}
		
		/**
		 * Removes a <code>Column</code> at a given index.
		 * 
		 * @param i index
		 * @return the removed <code>Column</code> (null if it does not exist)
		 */
		public function removeColumnAt( i: int ): Column
		{
			if( i >= columnAmount ) return null;
			
			var c: Column = _columns.splice( i, 1 )[ 0 ];
			
			if( c != null )
			{
				calcRemove( c.width, c.normalizedWidth );
				
				--_n;
				
				return c;
			}
			else			
			{
				return c;
			}
		}
		
		/**
		 * Sets a new width of a <code>Column</code>, specified by index.
		 * 
		 * @param i index
		 * @param width (optional) width of the <code>Column</code>
		 * @param normalizedWidth (optional) flag whether width should be calculated in pixel or normalized values
		 */
		public function setColumnWidthAt( i: int, width: Number, normalizedWidth: Boolean = false ): void
		{
			if( i < columnAmount ) setColumnWidth( getColumnAt( i ), width, normalizedWidth );
		}
		
		/**
		 * Sets a new width of a given <code>Column</code>.
		 * 
		 * @param c <code>Column</code>
		 * @param width (optional) width of the <code>Column</code>
		 * @param normalizedWidth (optional) flag whether width should be calculated in pixel or normalized values
		 */
		public function setColumnWidth( c: Column, width: Number, normalizedWidth: Boolean = false ): void
		{
			if( c != null )
			{
				calcRemove( c.width, c.normalizedWidth );
				calcAdd( width, normalizedWidth );
				
				c.setWidth( width, normalizedWidth );
			}
		}

		/**
		 * Returns total amount of columns in the <code>Row</code>.
		 * 
		 * @return total amount of columns in the <code>Row</code>
		 */
		public function get columnAmount(): int
		{
			return _n;
		}
		
		/**
		 * @private
		 */
		public function get normalizedWidth(): Number
		{
			return _normalizedWidth;
		}
		
		/**
		 * String representation of <code>Row</code>.
		 * 
		 * @return String representation of <code>Row</code>
		 */
		public function toString(): String
		{
			return "[ Row height=" + height + " normalizedHeight=" + normalizedHeight + " columns=" + columnAmount + " ]";
		}
		
		/**
		 * @private
		 */
		internal function get unspecificColumnAmount(): int
		{
			return _unspecificColumns;
		}
		
		/**
		 * @private
		 */
		internal function get specificWidth(): int
		{
			return _specificWidth;
		}
		
		/**
		 * @private
		 */
		internal function setHeight( height: Number, normalizedHeight: Boolean ): void
		{
			_height = height;
			_normalizedHeight = normalizedHeight;
		}
		
		private function calcAdd( width: Number, normalizedWidth: Boolean ): void
		{
			if( width == -1 )
			{
				++_unspecificColumns;
			}
			else if( !normalizedWidth )
			{
				_specificWidth += width;
			}
			else
			{
				_normalizedWidth += width;
				
				if( _normalizedWidth > 1 )
				{
					throw new IllegalOperationError( "normalized width of added columns are bigger than 1!" );
				}
			}
		}

		private function calcRemove( width: Number, normalizedWidth: Boolean ): void
		{
			if( width == -1 )
			{
				++_unspecificColumns;
			}
			else if( !normalizedWidth )
			{
				_specificWidth -= width;
			}
			else
			{
				_normalizedWidth -= width;
			}
		}
	}
}
