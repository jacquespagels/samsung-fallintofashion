/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import com.addicted2flash.util.ArraySet;
	import com.addicted2flash.util.HashMap;
	import com.addicted2flash.util.IMap;
	import com.addicted2flash.util.ISet;	
	
	/**
	 * This class represents a basic observer-pattern-based data-structure for <code>ILayoutObserver</code>s.
	 * <p>For aggregation usage the reference parameter at construction time has to be setted.</p>
	 * 
	 * @author Tim Richter
	 */
	public class LayoutObservable implements ILayoutObservable
	{
		protected var _observers: IMap;
		protected var _ref: ILayoutObservable;

		/**
		 * Create a new <code>LayoutObservable</code>.
		 * 
		 * @param reference (optional) the referenced target of notifications (useful for aggregation)
		 */
		public function LayoutObservable( reference: ILayoutObservable = null )
		{
			_ref = ( reference == null) ? this : reference;
		}

		/**
		 * @inheritDoc
		 */
		public function addObserver( o: ILayoutObserver ): void
		{
			if( _observers == null ) _observers = new HashMap();
			
			var a: Array = o.acceptedLayoutEvents;
			var n: int = a.length;
			var list: ISet;
			var note: String;
				
			while( --n > -1 )
			{
				note = a[ n ];
				
				list = _observers.getValue( note );
				
				if( list == null )
				{
					list = new ArraySet();
					
					_observers.put( note, list );
				}
				
				list.add( o );
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeObserver( o: ILayoutObserver ): void
		{
			if( _observers != null )
			{
				var a: Array = o.acceptedLayoutEvents;
				var n: int = a.length;
				var list: ISet;
				var note: String;
				
				while( --n > -1 )
				{
					note = a[ n ];
					
					list = ArraySet( _observers.getValue( note ) );
					
					if( list != null )
					{
						list.remove( o );
						
						if( list.isEmpty() )
						{
							list.dispose();
							list = null;
							
							_observers.remove( note );
						}
					}
				}
				
				if( _observers.size() == 0 )
				{
					_observers.dispose();
					_observers = null;
				}
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeObservers(): void
		{
			if( _observers != null )
			{
				_observers.dispose();
				_observers = null;
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function notifyObservers( type: int ): void
		{
			if( observerAmount > 0 )
			{
				var list: ISet = ISet( _observers.getValue( type ) );
				var a: Array;
				var n: int;
				var i: int;
				
				if( list != null )
				{
					a = list.toArray();
					n = a.length;
					
					for( i = 0; i < n; ++i )
					{
						ILayoutObserver( a[ i ] ).layoutUpdate( type, _ref );
					}
				}
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function get observerAmount(): int
		{
			return _observers == null ? 0 : _observers.size( );
		}
		
		/**
		 * @inheritDoc
		 */
		public function dispose(): void
		{
			if( _observers != null )
			{
				_observers.dispose();
				_observers = null;
			}
		}
		
	}
}
