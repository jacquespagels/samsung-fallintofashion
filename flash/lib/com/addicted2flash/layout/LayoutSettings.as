/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import com.addicted2flash.util.IDisposable;	
	
	import flash.errors.IllegalOperationError;	
	
	/**
	 * This class represents encapsulated settings for <code>ILayoutComponent</code>s. 
	 * 
	 * It contains information about the size and the alignment along the x/y axis.
	 * 
	 * @author Tim Richter
	 */
	public class LayoutSettings implements IDisposable
	{
		public static const UNSPECIFIED: LayoutSettings = LayoutSettings.createUnspecifiedSettings();
		
		protected var _preferredSize: Size;
		protected var _minimumSize: Size;
		protected var _alignmentX: Number;
		protected var _alignmentY: Number;
		
		protected var _isUnspecified: Boolean = false;
		
		/**
		 * Create a new <code>LayoutComponentSettings</code> object.
		 * 
		 * @param alignmentX alignment along the x axis
		 * @param alignmentY alignment along the y axis
		 * @param preferredSize preferred size
		 * @param minimumSize minimum size
		 */
		public function LayoutSettings( alignmentX: Number = 0, 
									       		 alignmentY: Number = 0,
									       		 preferredSize: Size = null,
									       		 minimumSize: Size = null )
		{
			_alignmentX = alignmentX;
			_alignmentY = alignmentY;
			_preferredSize = ( preferredSize == null ) ? new Size() : preferredSize;
			_minimumSize = ( minimumSize == null ) ? new Size() : minimumSize;
		}
		
		/**
		 * Returns the alignment along the x axis.
		 * <p>The value should be a number between 0 and 1 where 0 represents alignment along 
		 * the origin, 1 is aligned the furthest away from the origin, 0.5 is centered, etc.</p>
		 * 
		 * @return alignment along the x axis
		 */
		public function get alignmentX(): Number
		{
			return _alignmentX;
		}
		
		/**
		 * Returns the alignment along the y axis.
		 * <p>The value should be a number between 0 and 1 where 0 represents alignment along 
		 * the origin, 1 is aligned the furthest away from the origin, 0.5 is centered, etc.</p>
		 * 
		 * @return alignment along the y axis
		 */
		public function get alignmentY(): Number
		{
			return _alignmentY;
		}
		
		/**
		 * Returns the preferred <code>Size</code>.
		 * 
		 * @return the preferred <code>Size</code>
		 */
		public function get preferredSize(): Size
		{
			return _preferredSize;
		}
		
		/**
		 * Sets the preferred <code>Size</code>.
		 * 
		 * @param size the preferred <code>Size</code>
		 * @throws IllegalOperationError if UNSPECIFIED settings are used (cannot be modified)
		 */
		public function set preferredSize( size: Size ): void
		{
			if( _isUnspecified ) throw new IllegalOperationError( "cannot be setted in UNSPECIFIED Setting!" );
			
			_preferredSize = size;
		}
		
		/**
		 * Returns the minimum <code>Size</code>.
		 * 
		 * @return the minimum <code>Size</code>
		 */
		public function get minimumSize(): Size
		{
			return _minimumSize;
		}
		
		/**
		 * Sets the minimum <code>Size</code>.
		 * 
		 * @param size the minimum <code>Size</code>
		 * @throws IllegalOperationError if UNSPECIFIED settings are used (cannot be modified)
		 */
		public function set minimumSize( size: Size ): void
		{
			if( _isUnspecified ) throw new IllegalOperationError( "cannot be setted in UNSPECIFIED Setting!" );
			
			_minimumSize = size;
		}
		
		/**
		 * @inheritDoc
		 */
		public function dispose(): void
		{
			_preferredSize = null;
			_minimumSize = null;
		}

		/**
		 * String representation of <code>LayoutComponentSettings</code>.
		 * 
		 * @return String representation of <code>LayoutComponentSettings</code>
		 */
		public function toString(): String
		{
			return "[ LayoutComponentSettings alignmentX=" + alignmentX + " alignmentY=" + alignmentY + " preferredSize=" + preferredSize + " ]";
		}
		
		
		/**
		 * @private
		 */
		private static function createUnspecifiedSettings(): LayoutSettings
		{
			var s: LayoutSettings = new LayoutSettings();
			s._isUnspecified = true;
			
			return s;
		}
	}
}

