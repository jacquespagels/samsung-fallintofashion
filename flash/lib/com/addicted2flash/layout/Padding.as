/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import com.addicted2flash.util.ICloneable;		

	/**
	 * This class represents the borders of a container. It specifies the space 
	 * that a container must leave at each of its edges.
	 * 
	 * @author Tim Richter
	 */
	public class Padding implements ICloneable
	{
		private var _top: int;
		private var _left: int;
		private var _bottom: int;
		private var _right: int;
		
		/**
		 * Create a new <code>Padding</code>-object.
		 * 
		 * @param top (optional) the space from the top
		 * @param left (optional) the space from the left
		 * @param bottom (optional) the inspaceset from the bottom
		 * @param right (optional) the space from the right
		 */
		public function Padding( top: int = 0, left: int = 0, bottom: int = 0, right: int = 0 )
		{
			_top = top;
			_left = left;
			_bottom = bottom;
			_right = right;
		}
		
		/**
		 * Returns the space from the top.
		 * 
		 * @return the space from the top
		 */
		public function get top(): int
		{
			return _top;
		}
		
		/**
		 * Returns the space from the left.
		 * 
		 * @return the space from the left
		 */
		public function get left(): int
		{
			return _left;
		}
		
		/**
		 * Returns the space from the bottom.
		 * 
		 * @return the space from the bottom
		 */
		public function get bottom(): int
		{
			return _bottom;
		}
		
		/**
		 * Returns the space from the right.
		 * 
		 * @return the space from the right
		 */
		public function get right(): int
		{
			return _right;
		}
		
		/**
		 * @inheritDoc
		 */
		public function clone(): ICloneable
		{
			return new Padding( top, left, bottom, right );
		}
		
		/**
		 * String representation of <code>Padding</code>.
		 * 
		 * @return String representation of <code>Padding</code>
		 */
		public function toString(): String
		{
			return "[ Padding top=" + top + " right=" + right + " bottom=" + bottom + " left=" + left + " ]";
		}
	}
}
