/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import com.addicted2flash.layout.ILayoutComponent;
	
	import flash.display.Sprite;
	import flash.geom.Rectangle;	

	/**
	 * Basic <code>ILayoutComponent</code> binded to the display list.
	 * <p>In addition this class implements <code>ILayoutObservable</code>. Other objects can register
	 * for specific <code>LayoutEventType</code>s.
	 * 
	 * @author Tim Richter
	 */
	public class UILayoutComponent extends Sprite implements ILayoutComponent, ILayoutObservable 
	{
		protected var _layoutObservable: ILayoutObservable;
		protected var _layoutBounds: Rectangle;
		protected var _parent: ILayoutContainer;
		protected var _croppedSize: Size;
		protected var _settings: LayoutSettings;
		
		/**
		 * Create a new <code>UILayoutComponent</code>.
		 * 
		 * @param settings <code>LayoutSettings</code> (default = unspecific size, aligned at origin)
		 */
		public function UILayoutComponent( settings: LayoutSettings = null )
		{
			_settings = ( settings == null ) ? new LayoutSettings() : settings;
			
			_croppedSize = new Size( 0, 0 );
			_layoutBounds = new Rectangle();
			
			_layoutObservable = new LayoutObservable( this );
		}
		
		/**
		 * Sets the layout settings of this <code>UILayoutComponent</code>.
		 * 
		 * @param settings the layout settings of this <code>UILayoutComponent</code>
		 */
		public function set layoutSettings( settings: LayoutSettings ): void
		{
			_settings = settings;
			
			parentContainer.validateTree();
		}
		
		/**
		 * Returns the layout settings of this <code>UILayoutComponent</code>.
		 * 
		 * @return the layout settings of this <code>UILayoutComponent</code>
		 */
		public function get layoutSettings( ): LayoutSettings
		{
			return _settings;
		}

		/**
		 * @inheritDoc
		 */
		public function get alignmentX(): Number
		{
			return ( _settings == null ) ? LayoutAlignment.LEFT : _settings.alignmentX;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get alignmentY(): Number
		{
			return ( _settings == null ) ? LayoutAlignment.TOP : _settings.alignmentY;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get preferredSize(): Size
		{
			return ( _settings == null ) ? null : _settings.preferredSize;
		}
		
		/**
		 * @inheritDoc
		 */
		public function set preferredSize( size: Size ): void
		{
			if( _settings != null ) _settings.preferredSize = size;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get minimumSize(): Size
		{
			return ( _settings == null ) ? null : _settings.minimumSize;
		}
		
		/**
		 * @inheritDoc
		 */
		public function set minimumSize( size: Size ): void
		{
			if( _settings != null ) _settings.minimumSize = size;
		}

		/**
		 * @inheritDoc
		 */
		public function get croppedSize(): Size
		{
			return _croppedSize;
		}
		
		/**
		 * @inheritDoc
		 */
		public function set croppedSize( size: Size ): void
		{
			_croppedSize = size;
			
			notifyObservers( LayoutEventType.CROP );
		}
		
		/**
		 * @inheritDoc
		 */
		public function get layoutBounds(): Rectangle
		{
			return _layoutBounds;
		}
		
		/**
		 * @inheritDoc
		 */
		public function set layoutBounds( bounds: Rectangle ): void
		{
			x = bounds.x;
			y = bounds.y;
			
			_layoutBounds = bounds;
			
			notifyObservers( LayoutEventType.UPDATE );
		}
		
		/**
		 * @inheritDoc
		 */
		public function get parentContainer(): ILayoutContainer
		{
			return _parent;
		}
		
		/**
		 * @inheritDoc
		 */
		public function set parentContainer( parent: ILayoutContainer ): void
		{
			_parent = parent;
		}
		
		/**
		 * @inheritDoc
		 */
		public function addObserver( o: ILayoutObserver ): void
		{
			if( _layoutObservable != null ) _layoutObservable.addObserver( o );
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeObserver( o: ILayoutObserver ): void
		{
			if( _layoutObservable != null ) _layoutObservable.removeObserver( o );
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeObservers(): void
		{
			if( _layoutObservable != null ) _layoutObservable.removeObservers();
		}
		
		/**
		 * @inheritDoc
		 */
		public function notifyObservers( type: int ): void
		{
			if( _layoutObservable != null ) _layoutObservable.notifyObservers( type );
		}
		
		/**
		 * @inheritDoc
		 */
		public function get observerAmount(): int
		{
			return ( _layoutObservable == null ) ? 0 : _layoutObservable.observerAmount;
		}
		
		/**
		 * @inheritDoc
		 */
		public function dispose(): void
		{
			_parent = null;
			_layoutBounds = null;
			_croppedSize = null;
			
			if( _settings != null ) 
			{
				_settings.dispose();
				_settings = null;
			}
			
			if( _layoutObservable != null )
			{
				_layoutObservable.dispose();
				_layoutBounds = null;
			}
		}
		
		/**
		 * String representation of <code>UILayoutComponent</code>.
		 * 
		 * @return String representation of <code>UILayoutComponent</code>
		 */
		override public function toString(): String
		{
			return "[ UILayoutComponent bounds=" + layoutBounds + " ]";
		}
	}
}
