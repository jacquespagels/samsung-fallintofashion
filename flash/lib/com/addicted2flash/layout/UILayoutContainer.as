/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import com.addicted2flash.layout.ILayoutContainer;
	import com.addicted2flash.util.ArrayList;
	import com.addicted2flash.util.IDisposable;
	import com.addicted2flash.util.IList;
	
	import flash.display.DisplayObject;
	import flash.geom.Point;
	import flash.geom.Rectangle;	

	/**
	 * Basic <code>ILayoutContainer</code> binded to the display list.
	 * 
	 * @example the following code creates a new UILayoutContainer with custom settings and an
	 * MatrixLayout.
	 * <listing version="3.0">
	 * var container: UILayoutContainer = new LayoutContainer( new LayoutContainerSettings( 0, 0, 300, 0.5, false, true), new MatrixLayout( createMatrix() ) );
	 * container.addComponent( new UILayoutComponent() );
	 * container.addComponent( new UILayoutComponent() );
	 * container.addComponent( new UILayoutComponent() );
	 * 
	 * container.validate();
	 * 
	 * function createMatrix(): LayoutMatrix
	 * {
	 * 		var matrix: LayoutMatrix = new LayoutMatrix();
	 * 		matrix.addRow( new Row( 100 ) );
	 * 		matrix.addColumn( new Column( 100 ) );
	 * 		matrix.addColumn( new Column() );
	 * 		
	 * 		matrix.addRow( new Row() );
	 * 		matrix.addColumn( new Column() );
	 * 		
	 * 		return matrix;
	 * }
	 * </listing>
	 * 
	 * @author Tim Richter
	 */
	public class UILayoutContainer extends UILayoutComponent implements ILayoutContainer 
	{
		protected var _components: IList;
		protected var _layout: ILayoutStrategy;
		
		private var _padding: Padding;

		/**
		 * Create a new <code>UILayoutContainer</code>.
		 * 
		 * @param settings <code>LayoutContainerSettings</code> (default = unspecific size, aligned at origin)
		 * @param layout <code>ILayoutStrategy</code>
		 */
		public function UILayoutContainer( settings: LayoutSettings = null, layout: ILayoutStrategy = null )
		{
			super( settings );
			
			_layout = layout;
			
			_layoutBounds = new Rectangle( 0, 0, _settings.preferredSize.width, _settings.preferredSize.height );
		}
		
		/**
		 * @inheritDoc
		 */
		override public function set layoutSettings( settings: LayoutSettings ): void
		{
			super.layoutSettings = settings;
			
			validateTree();
		}
		
		/**
		 * @inheritDoc
		 */
		override public function set layoutBounds( bounds: Rectangle ): void
		{
			super.layoutBounds = bounds;
			
			validate();
		}

		/**
		 * @inheritDoc
		 */
		public function get layout(): ILayoutStrategy
		{
			return _layout;
		}
		
		/**
		 * @inheritDoc
		 */
		public function set layout( layout: ILayoutStrategy ): void
		{
			_layout = layout;
		}
		
		/**
		 * @inheritDoc
		 */
		public function get padding(): Padding
		{
			return ( _padding == null ) ? _padding = new Padding( ) : _padding;
		}
		
		/**
		 * @inheritDoc
		 */
		public function set padding( padding: Padding ): void
		{
			_padding = padding;
		}

		/**
		 * @inheritDoc
		 */
		public function addComponent( c: ILayoutComponent ): ILayoutComponent
		{
			return internalAddAt( ( _components == null ) ? 0 : _components.size( ), c );
		}

		/**
		 * @inheritDoc
		 */
		public function addComponentAt( index: int, c: ILayoutComponent ): ILayoutComponent
		{
			return internalAddAt( index, c );
		}
		
		/**
		 * @inheritDoc
		 */
		public function getComponentAt( index: int ): ILayoutComponent
		{
			return ( _components == null ) ? null : ILayoutComponent( _components.getAt( index ) );
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeComponent( c: ILayoutComponent ): ILayoutComponent
		{
			if( _components != null )
			{
				if( _components.remove( c ) )
				{
					removeChild( DisplayObject( c ) );
				}
				
				c.dispose();
				
				if( _components.isEmpty() )
				{
					_components.dispose();
					_components = null;
				}
			}
			
			return c;
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeComponentAt( index: int ): ILayoutComponent
		{
			if( _components != null && index < componentAmount )
			{
				var c: ILayoutComponent = ILayoutComponent( _components.removeAt( index ) );
				
				if( c != null )
				{
					removeChild( getChildAt( index ) );
					
					c.dispose();
					
					if( _components.isEmpty() )
					{
						_components.dispose();
						_components = null;
					}
				}
				
				return c;
			}
			else
			{
				return null;
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function findComponentAt( x: Number, y: Number ): ILayoutComponent
		{
			if( _components != null )
			{
				var len: int = componentAmount;
				var c: ILayoutComponent;
				var p: Point = new Point( x, y );
				
				while( --len > -1 )
				{
					c = getComponentAt( len );
					
					if( c.layoutBounds.containsPoint( p ) )
					{
						return c;
					}
				}
				
				return null;
			}
			else
			{
				return null;
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function get componentAmount(): int
		{
			return ( _components == null ) ? 0 : _components.size();
		}
		
		/**
		 * @inheritDoc
		 */
		public function validate(): void
		{
			if( _layout != null ) _layout.layoutContainer( this );
		}
		
		/**
		 * @inheritDoc
		 */
		public function validateTree(): void
		{
			var c: ILayoutContainer = this;
			
			while( c.parentContainer != null )
			{
				c = c.parentContainer;
			}
			
			c.validate();
		}
		
		/**
		 * String representation of <code>UILayoutContainer</code>.
		 * 
		 * @return String representation of <code>UILayoutContainer</code>
		 */
		override public function toString(): String
		{
			return "[ UILayoutContainer components=" + componentAmount + " layout=" + layout + " bounds=" + layoutBounds + " ]";
		}
		
		/**
		 * @inheritDoc
		 */
		override public function dispose(): void
		{
			super.dispose();
			
			if( _components != null )
			{
				var len: int = _components.size();
				
				while( --len > -1 )
				{
					removeComponentAt( len );
				}
			}
			
			_layout = null;
			_settings = null;
		}
		
		/**
		 * @private
		 */
		protected function internalAddAt( i: int, c: ILayoutComponent ): ILayoutComponent
		{
			if( _components == null ) _components = new ArrayList();
			
			c.parentContainer = this;
			
			addChildAt( DisplayObject( c ), i );
			
			_components.addAt( i, c );
			
			return c;
		}
	}
}
