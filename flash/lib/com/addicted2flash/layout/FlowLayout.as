package com.addicted2flash.layout 
{
	import com.addicted2flash.layout.ILayoutStrategy;
	
	import flash.geom.Rectangle;		

	/**
	 * @author Tim Richter
	 */
	public class FlowLayout implements ILayoutStrategy 
	{
		public static const LEFT: int = 0;
		public static const RIGHT: int = 1;
		public static const JUSTIFY: int = 2;
		
		private var _alignment: int;
		private var _hGap: int;
		private var _vGap: int;
		
		/**
		 * Create a new <code>FlowLayout</code>.
		 * 
		 * @param alignment (optional) alignment of <code>ILayoutComponent</code>s
		 * @param hGap (optional) the horizontal gap between components
		 * @param vGap (optional) the vertical gap between components
		 */
		public function FlowLayout( alignment: int = 0, hgap: int = 0, vgap: int = 0 )
		{
			_alignment = alignment;
			_hGap = hgap;
			_vGap = vgap;
		}
		
		/**
		 * Returns the horizontal gap between components.
		 * 
		 * @return the horizontal gap between components 
		 */
		public function get horizontalGap(): int
		{
			return _hGap;
		}

		/**
		 * Sets the horizontal gap between components.
		 * 
		 * @return the horizontal gap between components 
		 */
		public function set horizontalGap( gap: int ): void
		{
			_hGap = gap;
		}

		/**
		 * Returns the vertical gap between components.
		 * 
		 * @return the vertical gap between components 
		 */
		public function get verticalGap(): int
		{
			return _vGap;
		}

		/**
		 * Sets the vertical gap between components.
		 * 
		 * @return the vertical gap between components 
		 */
		public function set verticalGap( gap: int ): void
		{
			_vGap = gap;
		}
		
		/**
		 * @inheritDoc
		 */
		public function layoutContainer( container: ILayoutContainer ): void
		{
			var ncomp: int = container.componentAmount;
			
			if( ncomp == 0 ) return;
			
			var padding: Padding = container.padding;
			var w: int = container.layoutBounds.width - ( padding.left + padding.right );
			var h: int = container.layoutBounds.height - ( padding.top + padding.bottom );
			
			var x: int = ( _alignment == LEFT || _alignment == JUSTIFY ) ? padding.left : container.layoutBounds.width - padding.right;
			var y: int = padding.top;
			
			var comp: ILayoutComponent;
			var compW: int;
			var compH: int;
			var pref: Size;
			var bounds: Rectangle;
			
			var unspecificH: int = int( h / ncomp );
			var maxH: int = 0;
			var rowW: int = 0;
			var first: int = 0;
			var i: int = 0;
			
			for( ; i < ncomp; ++i )
			{
				if( ( comp = container.getComponentAt( i ) ) != null )
				{
					pref = comp.preferredSize;
					bounds = comp.layoutBounds;
					
					compW = ( pref.width == -1 ) ? w : ( pref.normalizedWidth ) ? pref.width * w : pref.width;
					compH = ( pref.height == -1 ) ? unspecificH : ( pref.normalizedHeight ) ? pref.height * h : pref.height;
					
					// -- biggest height of component in a row
					if( compH > maxH ) maxH = compH;
					
					if( rowW + compW > w )
					{
						bounds.x = x;
						bounds.y = y;
						bounds.width = compW;
						bounds.height = compH;
						
						if( _alignment == JUSTIFY ) justifyRow( container, y, ( w - rowW ) / ( i - first - 1 ), first, i-1 );
						
						y += maxH + _vGap;
						maxH = compH;
						first = i;
						rowW = 0;
					}
					
					bounds.x = x;
					bounds.y = y;
					bounds.width = compW;
					bounds.height = compH;
					
					rowW += compW;
				}
			}
			
			if( _alignment == JUSTIFY ) justifyRow( container, y, ( w - rowW ) / ( i - first - 1 ), first, i-1 );
			
			var size: Size = container.croppedSize;
			var totalH: int = y + maxH;
			
			if( totalH > h || size.height != 0 )
			{
				size.height = ( totalH > h ) ? totalH - h : 0;
				
				container.croppedSize = size;
			}
		}

		/**
		 * String representation of <code>FlowLayout</code>.
		 * 
		 * @return String representation of <code>FlowLayout</code>
		 */
		public function toString(): String
		{
			return "[ FlowLayout hGap=" + horizontalGap + " vGap=" + verticalGap + " ]";
		}
		
		
		private function justifyRow( container: ILayoutContainer, y: int, gap: int, from: int, to: int ): void
		{
			var c: ILayoutComponent;
			var b: Rectangle;
			var x: int = 0;
			
			for( ; from <= to; ++from )
			{
				if( ( c = container.getComponentAt( from ) ) != null )
				{
					b = c.layoutBounds;
					
					c.layoutBounds.x = x;
					c.layoutBounds.y = y;
					
					x = c.layoutBounds.right + gap;
					
					c.layoutBounds = b;
				}
			}
		}
	}
}
