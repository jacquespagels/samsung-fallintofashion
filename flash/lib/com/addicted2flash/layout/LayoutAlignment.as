/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{

	/**
	 * Alignment types for layout specifications.
	 * 
	 * @author Tim Richter
	 */
	public class LayoutAlignment 
	{
		/**
		 * Specifies an alignment to the left.
		 */
		public static const LEFT: int = 0;
		
		/**
		 * Specifies an alignment to the right.
		 */
		public static const RIGHT: int = 1;
		
		/**
		 * Specifies an alignment to the top.
		 */
		public static const TOP: int = 0;
		
		/**
		 * Specifies an alignment to the bottom.
		 */
		public static const BOTTOM: int = 1;
		
		/**
		 * Specifies an alignment to the center.
		 */
		public static const CENTER: Number = .5;
	}
}
