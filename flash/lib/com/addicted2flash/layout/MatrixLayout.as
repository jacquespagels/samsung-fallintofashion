/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import com.addicted2flash.layout.ILayoutStrategy;

	import flash.geom.Rectangle;	

	/**
	 * A <code>MatrixLayout</code> is a <code>ILayoutStrategy</code> for complex layouts.
	 * <p>The layout itself is specified by an <code>LayoutMatrix</code> and all 
	 * <code>ILayoutComponent</code>s will be layouted in dynamic rectangles.</p>
	 * 
	 * @author Tim Richter
	 */
	public class MatrixLayout implements ILayoutStrategy 
	{
		private var _matrix: LayoutMatrix;
		private var _hgap: int;
		private var _vgap: int;

		/**
		 * Create a new <code>MatrixLayout</code>.
		 * 
		 * @param matrix <code>LayoutMatrix</code>
		 * @param hGap (optional) the horizontal gap between components
		 * @param vGap (optional) the vertical gap between components
		 */
		public function MatrixLayout( matrix: LayoutMatrix, hgap: int = 0, vgap: int = 0 )
		{
			_matrix = matrix;
			_hgap = hgap;
			_vgap = vgap;
		}
		
		/**
		 * Returns the <code>LayoutMatrix</code>.
		 * 
		 * @return the <code>LayoutMatrix</code>
		 */
		public function get matrix(): LayoutMatrix
		{
			return _matrix;
		}
		
		/**
		 * Sets the <code>LayoutMatrix</code>.
		 * 
		 * @param matrix <code>LayoutMatrix</code>
		 */
		public function set matrix( matrix: LayoutMatrix ): void
		{
			_matrix = matrix;
		}
		
		/**
		 * Returns the horizontal gap between the <code>ILayoutComponent</code>s.
		 * 
		 * @return the horizontal gap between the <code>ILayoutComponent</code>s
		 */
		public function get hgap(): int
		{
			return _hgap;
		}
		
		/**
		 * Sets the horizonal gap between the <code>ILayoutComponent</code>s.
		 * 
		 * @param hgap the horizonal gap between the <code>ILayoutComponent</code>s
		 */
		public function set hgap( hgap: int ): void
		{
			_hgap = hgap;
		}
		
		/**
		 * Returns the vertical gap between the <code>ILayoutComponent</code>s.
		 * 
		 * @return the vertical gap between the <code>ILayoutComponent</code>s
		 */
		public function get vgap(): int
		{
			return _vgap;
		}
		
		/**
		 * Sets the vertical gap between the <code>ILayoutComponent</code>s.
		 * 
		 * @param vgap the vertical gap between the <code>ILayoutComponent</code>s
		 */
		public function set vgap( vgap: int ): void
		{
			_vgap = vgap;
		}

		/**
		 * @inheritDoc
		 */
		public function layoutContainer( container: ILayoutContainer ): void
		{
			var ncomp: int = container.componentAmount;
			
			if( ncomp == 0 ) return;
			
			var nrows: int = _matrix.rowAmount;
			var ncols: int;
			
			var padding: Padding = container.padding;
			var x: int = padding.left;
			var y: int = padding.top;
			var w: int = container.layoutBounds.width - ( padding.left + padding.right );
			var h: int = container.layoutBounds.height - ( padding.top + padding.bottom );
			
			var r: int = 0;
			var c: int = 0;
			var i: int = 0;
			
			var row: Row;
			var col: Column;
			
			var unspecificColW: int;
			var unspecificRowH: int = ( _matrix.unspecificRowAmount > 0 ) ? ( ( h - ( nrows - 1 ) * _vgap ) * ( 1 - _matrix.normalizedHeight ) - _matrix.specificHeight ) / _matrix.unspecificRowAmount : 0;
			
			var rowH: int;
			var colW: int;
			
			var comp: ILayoutComponent;
			
			var bounds: Rectangle;
			
			var min: Size;
			var minW: int;
			var minH: int;
			
			var pref: Size;
			var prefW: int;
			var prefH: int;
			
			var crop: Size;
			var cropW: int = 0;
			var cropH: int = 0;
			
			for( r = 0; r < nrows; ++r, y += rowH + _vgap, x = padding.left )
			{
				row = _matrix.getRowAt( r );
				ncols = row.columnAmount;
				
				rowH = ( row.height == -1 ) ? unspecificRowH : ( row.normalizedHeight ) ? row.height * h : row.height;
				unspecificColW = ( row.unspecificColumnAmount == 0 ) ? 0 : ( ( w - ( ncols - 1 ) * _hgap ) * ( 1 - row.normalizedWidth ) - row.specificWidth ) / row.unspecificColumnAmount;
				 
				for( c = 0; c < ncols && i < ncomp; ++c, ++i, x += colW + _hgap )
				{
					col = row.getColumnAt( c );
					colW = ( col.width == -1 ) ? unspecificColW : ( col.normalizedWidth ) ? col.width * w : col.width;
					
					comp = container.getComponentAt( i );
					
					if( comp != null )
					{
						bounds = comp.layoutBounds;
						crop = comp.croppedSize;
						
						min = comp.minimumSize;
						minW = ( min.width == -1 ) ? 0 : ( min.normalizedWidth ) ? min.width * w : min.width;
						minH = ( min.height == -1 ) ? 0 : ( min.normalizedHeight ) ? min.height * h : min.height;
						
						if( colW < minW )
						{
							cropW += minW - colW;
							colW = minW;
						}
						
						if( rowH < minH )
						{
							cropH += minH - rowH;
							rowH = minH;
						}
						
						pref = comp.preferredSize;
						prefW = ( pref.width == -1 ) ? colW : ( pref.normalizedWidth ) ? w * pref.width : pref.width;
						prefH = ( pref.height == -1 ) ? rowH : ( pref.normalizedHeight ) ? h * pref.height : pref.height;
						
						bounds.x = int( ( prefW < colW ) ? x + comp.alignmentX * ( colW - prefW ) : x );
						bounds.y = int( ( prefH < rowH ) ? y + comp.alignmentY * ( rowH - prefH ) : y );
						bounds.width = ( prefW < colW ) ? prefW : colW;
						bounds.height = ( prefH < rowH ) ? prefH : rowH;
						
						// -- sets the layout bounds
						comp.layoutBounds = bounds;
						
						if( colW < prefW || rowH < prefH || crop.width != 0 || crop.height != 0 )
						{
							crop.width = ( colW < prefW ) ? prefW - colW : 0;
							crop.height = ( rowH < prefH ) ? prefH - rowH : 0;
							
							// -- sets the cropped size
							comp.croppedSize = crop;
						}
					}
				}
			}
			
			// -- if container is the root (otherwise root container won't be cropped)
			if( container.parentContainer == null )
			{
				var size: Size = container.croppedSize;
				
				if( cropW > 0 || cropH > 0 || size.width != 0 || size.height != 0 )
				{
					size.width = ( cropW > 0 ) ? cropW : 0;
					size.height = ( cropH > 0 ) ? cropH : 0;
					
					container.croppedSize = size;
				}
			}
		}
		
		/**
		 * String representation of <code>MatrixLayout</code>.
		 * 
		 * @return String representation of <code>MatrixLayout</code>
		 */
		public function toString(): String
		{
			return "[ MatrixLayout matrix=" + matrix + " hgap=" + hgap + " vgap=" + vgap + " ]";
		}
	}
}
