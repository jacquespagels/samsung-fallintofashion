/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{

	/**
	 * This class represents a <code>Column</code> in a <code>GridMatrix</code>.
	 * 
	 * @author Tim Richter
	 */
	public class Column 
	{
		private var _parentRow: Row;
		private var _width: Number;
		private var _normalizedWidth: Boolean;
		
		/**
		 * Create a new <code>Column</code>
		 * 
		 * @param width (optional) width of the <code>Column</code>
		 * @param normalizedWidth (optional) flag whether width should be calculated in pixel or normalized values
		 */
		public function Column( width: Number = -1, normalizedWidth: Boolean = false )
		{
			_width = width;
			_normalizedWidth = normalizedWidth;
		}
		
		/**
		 * Returns the width of the <code>Column</code>.
		 * 
		 * @return the width of the <code>Column</code>
		 */
		public function get width(): Number
		{
			return _width;
		}
		
		/**
		 * Returns true if width is specified as a normalized value. Otherwise it is specified as a pixel value.
		 * 
		 * @return true if width is specified as a normalized value. Otherwise it is specified as a pixel value
		 */
		public function get normalizedWidth(): Boolean
		{
			return _normalizedWidth;
		}
		
		/**
		 * Returns the parent <code>Row</code>.
		 * 
		 * @return the parent <code>Row</code>
		 */
		public function get parentRow(): Row
		{
			return _parentRow;
		}
		
		/**
		 * Sets the parent <code>Row</code>.
		 * 
		 * @param parentRow the parent <code>Row</code>
		 */
		public function set parentRow( parentRow: Row ): void
		{
			_parentRow = parentRow;
		}
		
		
		/**
		 * String representation of <code>Column</code>.
		 * 
		 * @return String representation of <code>Column</code>
		 */
		public function toString(): String
		{
			return "[ Column width=" + width + " normalizedWidth=" + normalizedWidth + " ]";
		}
		
		/**
		 * @private
		 */
		internal function setWidth( width: Number, normalizedWidth: Boolean ): void
		{
			_width = width;
			_normalizedWidth = normalizedWidth;
		}
	}
}
