/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import flash.errors.IllegalOperationError;	
	import flash.display.Graphics;	
	import flash.display.Sprite;	
	import flash.display.Stage;
	import flash.geom.Rectangle;	

	/**
	 * This class is a layout debugger which draws an outline around all child <code>ILayoutComponent</code>s
	 * all of a given <code>ILayoutContainer</code>.
	 * 
	 * @author Tim Richter
	 */
	public class LayoutDebugger implements ILayoutObserver 
	{
		private static const INSTANCE: LayoutDebugger = new LayoutDebugger();
		
		private static var _layoutContainer: Sprite;
		private static var _settings: LayoutDebuggerSettings;
		private static var _count: int = 0;
		private static var _total: int = 0;
		
		/**
		 * Creates a <code>LayoutDebugger</code>.
		 */
		public function LayoutDebugger()
		{
			if( INSTANCE != null ) throw new IllegalOperationError( "this class cannot be instantiated more than once!" );
		}

		
		/**
		 * Initializes the <code>LayoutDebugger</code>.
		 * 
		 * @param settings <code>LayoutDebuggerSettings</code>.
		 */
		public static function initialize( settings: LayoutDebuggerSettings ): void
		{
			if( _layoutContainer == null )
			{
				_layoutContainer = new Sprite();
				settings.stage.addChild( _layoutContainer );
			}
			
			_settings = settings;
		}
		
		/**
		 * Shows the outline of all <code>ILayoutComponent</code> of the given <code>ILayoutContainer</code>.
		 * <p>NOTE: all added components have to imlement <code>ILayoutObservable</code>.</p>
		 * 
		 * @param root <code>ILayoutContainer</code>
		 */
		public static function showOutline( root: ILayoutContainer ): void
		{
			if( root is ILayoutObservable ) ILayoutObservable( root ).addObserver( INSTANCE );
			
			addAsObserver( root );
		}
		
		/**
		 * Hides the outline of all <code>ILayoutComponent</code> of the given <code>ILayoutContainer</code>.
		 * 
		 * @param root <code>ILayoutContainer</code>
		 */
		public static function hideOutline( root: ILayoutContainer ): void
		{
			if( root is ILayoutObservable ) ILayoutObservable( root ).removeObserver( INSTANCE );
			
			removeAsObserver( root );
			
			_total = 0;
			_count = 0;
			
			_layoutContainer.graphics.clear( );
		}
		
		/**
		 * @private
		 */
		public function layoutUpdate( type: int, o: ILayoutObservable ): void
		{
			switch( type )
			{
				case LayoutEventType.UPDATE:
					checkCount();
					draw( ILayoutComponent( o ) );
					break;
			}
		}
		
		/**
		 * @private
		 */
		public function get acceptedLayoutEvents(): Array
		{
			return [ LayoutEventType.UPDATE ];
		}
		
		
		private static function addAsObserver( root: ILayoutContainer ): void
		{
			var n: int = root.componentAmount;
			var comp: ILayoutComponent;
			
			_total += ( root.parentContainer == null ) ? 1 + n : n;
			
			if( n > 0 )
			{
				while( --n > -1 )
				{
					comp = root.getComponentAt( n );
					
					if( comp is ILayoutObservable ) ILayoutObservable( comp ).addObserver( INSTANCE );
					if( comp is ILayoutContainer ) addAsObserver( ILayoutContainer( comp ) );
				}
			}
		}
		
		private static function removeAsObserver( root: ILayoutContainer ): void
		{
			var n: int = root.componentAmount;
			var comp: ILayoutComponent;
			
			if( n > 0 )
			{
				while( --n > -1 )
				{
					comp = root.getComponentAt( n );
					
					if( comp is ILayoutObservable ) ILayoutObservable( comp ).removeObserver( INSTANCE );
					if( comp is ILayoutContainer ) removeAsObserver( ILayoutContainer( comp ) );
				}
			}
		}
		
		private function checkCount(): void
		{
			if( _count++ >= _total ) 
			{
				_count = 1;
				_layoutContainer.graphics.clear( );
			}
		}
		
		private function draw( c: ILayoutComponent ): void
		{
			var b: Rectangle = getBounds( c.layoutBounds.clone(), c, 0, 0 );
			
			_layoutContainer.graphics.lineStyle( _settings.lineThickness, _settings.lineColor );
			_layoutContainer.graphics.drawRect( b.x, b.y, b.width, b.height );
		}
		
		private function getBounds( rootBounds: Rectangle, parent: ILayoutComponent, x: int, y: int ): Rectangle
		{
			x += parent.layoutBounds.x;
			y += parent.layoutBounds.y;
			
			if( parent.parentContainer == null )
			{
				rootBounds.x = x;
				rootBounds.y = y;
				
				return rootBounds;
			}
			else
			{
				return getBounds( rootBounds, parent.parentContainer, x, y );
			}
		}
	}
}
