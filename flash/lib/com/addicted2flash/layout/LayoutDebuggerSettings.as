/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import flash.display.Stage;	
	
	/**
	 * This class represents settings for an <code>LayoutDebugger</code>.
	 * 
	 * @author Tim Richter
	 */
	public class LayoutDebuggerSettings 
	{

		private var _stage: Stage;
		private var _lineColor: int;
		private var _lineThickness: int;
		
		/**
		 * Create a new <code>LayoutDebuggerSettings</code> object.
		 * 
		 * @param stage <code>Stage</code>
		 * @param lineColor color of outline
		 * @param lineThickness thickness of outline
		 */
		public function LayoutDebuggerSettings( stage: Stage, lineColor: int = 0, lineThickness: int = 1 )
		{
			_stage = stage;
			_lineColor = lineColor;
			_lineThickness = lineThickness;
		}
		
		/**
		 * Returns the <code>Stage</code>.
		 * 
		 * @return the <code>Stage</code>
		 */
		public function get stage(): Stage
		{
			return _stage;
		}
		
		/**
		 * Returns the color of the outline.
		 * 
		 * @return the color of the outline
		 */
		public function get lineColor(): int
		{
			return _lineColor;
		}
		
		/**
		 * Returns the thickness of the outline.
		 * 
		 * @return the thickness of the outline
		 */
		public function get lineThickness(): int
		{
			return _lineThickness;
		}
	}
}
