/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.layout 
{
	import com.addicted2flash.layout.ILayoutStrategy;

	import flash.errors.IllegalOperationError;
	import flash.geom.Rectangle;	

	/**
	 * The <code>GridLayout</code> class is a layout strategy that lays out a <code>LayoutContainer</code>'s 
	 * <code>ILayoutComponent</code> in a rectangular grid. 
	 * The <code>LayoutContainer</code> is divided into equal-sized rectangles, and one <code>ILayoutComponent</code> is 
	 * placed in each rectangle. The <code>ILayoutComponent</code> alignment is related to their specific 
	 * <code>alignmentX, alignmentY</code> properties.
	 * <p>NOTE: the <code>ILayoutComponent</code>s are only aligned if their preferred size is smaller than the calculated one in the grid,
	 * otherwise there are aligned to their origin.</p>
	 * 
	 * @author Tim Richter
	 */
	public class GridLayout implements ILayoutStrategy 
	{
		protected var _vGap: int;
		protected var _hGap: int;
		protected var _cols: int;
		protected var _rows: int;

		/**
		 * Create a new <code>GridLayout </code>.
		 *
		 * @param rows rows of the layout
		 * @param cols columns of the layout
		 * @param hGap (optional) the horizontal gap between components
		 * @param vGap (optional) the vertical gap between components
		 */
		public function GridLayout( rows: int, cols: int, hGap: int = 0, vGap: int = 0 )
		{
			if( rows <= 0 || cols <= 0 )
			{
				throw new IllegalOperationError( "rows and columns cannot both be <= zero" );
			}
			
			_rows = rows;
			_cols = cols;
			_hGap = hGap;
			_vGap = vGap;
		}

		/**
		 * Returns the horizontal gap between components.
		 * 
		 * @return the horizontal gap between components 
		 */
		public function get horizontalGap(): int
		{
			return _hGap;
		}

		/**
		 * Sets the horizontal gap between components.
		 * 
		 * @return the horizontal gap between components 
		 */
		public function set horizontalGap( gap: int ): void
		{
			_hGap = gap;
		}

		/**
		 * Returns the vertical gap between components.
		 * 
		 * @return the vertical gap between components 
		 */
		public function get verticalGap(): int
		{
			return _vGap;
		}

		/**
		 * Sets the vertical gap between components.
		 * 
		 * @return the vertical gap between components 
		 */
		public function set verticalGap( gap: int ): void
		{
			_vGap = gap;
		}

		/**
		 * Returns the amount of columns.
		 * 
		 * @return the amount of columns
		 */
		public function get columns(): int
		{
			return _cols;
		}

		/**
		 * Sets the amount of columns.
		 * 
		 * @param columns the amount of columns
		 */
		public function set columns( columns: int ): void
		{
			if( columns <= 0 ) throw new IllegalOperationError( "columns cannot be <= zero" );
			
			_cols = columns;
		}

		/**
		 * Returns the amount of rows.
		 * 
		 * @return the amount of rows
		 */
		public function get rows(): int
		{
			return _rows;
		}

		/**
		 * Returns the amount of rows.
		 * 
		 * @param rows the amount of rows
		 */
		public function set rows( rows: int ): void
		{
			if( rows <= 0 ) throw new IllegalOperationError( "rows cannot be <= zero" );
			
			_rows = rows;
		}

		/**
		 * @inheritDoc
		 */
		public function layoutContainer( container: ILayoutContainer ): void
		{
			var ncomp: int = container.componentAmount;
			
			if( ncomp == 0 ) return;
			
			var ncols: int = columns;
			var nrows: int = rows;
			
			var padding: Padding = container.padding;
			var w: int = container.layoutBounds.width - ( padding.left + padding.right );
			var h: int = container.layoutBounds.height - ( padding.top + padding.bottom );
			var x: int = padding.left;
			var y: int = padding.top;
			
			var r: int = 0;
			var c: int = 0;
			var i: int = 0;
			
			var comp: ILayoutComponent;
			var compW: int = ( w - ( ncols - 1 ) * horizontalGap ) / ncols;
			var compH: int = ( h - ( nrows - 1 ) * verticalGap ) / nrows;
			
			var pref: Size;
			var prefW: int;
			var prefH: int;
			
			var bounds: Rectangle;
			var crop: Size;
			
			for( r = 0; r < nrows; ++r, y += compH + verticalGap, x = padding.left )
			{
				for( c = 0; c < ncols && i < ncomp; ++c, ++i, x += compW + horizontalGap )
				{
					if( ( comp = container.getComponentAt( i ) ) != null )
					{
						bounds = comp.layoutBounds;
						crop = comp.croppedSize;
						
						pref = comp.preferredSize;
						prefW = ( pref.normalizedWidth ) ? pref.width * w : ( pref.width == -1 ) ? compW : pref.width;
						prefH = ( pref.normalizedHeight ) ? pref.height * h : ( pref.height == -1 ) ? compH : pref.height;
						
						bounds.x = int( ( prefW < compW ) ? x + comp.alignmentX * ( compW - prefW ) : x );
						bounds.y = int( ( prefH < compH ) ? y + comp.alignmentY * ( compH - prefH ) : y );
						bounds.width = ( prefW < compW ) ? prefW : compW;
						bounds.height = ( prefH < compH ) ? prefH : compH;
						
						comp.layoutBounds = bounds;
						
						if( prefW > compW || prefW > compH || crop.width != 0 || crop.height != 0 )
						{
							crop.width = ( prefW > compW ) ? prefW - compW : 0;
							crop.height = ( prefH > compH ) ? prefH - compH : 0;
							
							comp.croppedSize = crop;
						}
					}
				}
			}
		}

		/**
		 * String representation of <code>GridLayout</code>.
		 * 
		 * @return String representation of <code>GridLayout</code>
		 */
		public function toString(): String
		{
			return "[ GridLayout columns=" + columns + " rows=" + rows + " hGap=" + horizontalGap + " vGap=" + verticalGap + " ]";
		}
	}
}
