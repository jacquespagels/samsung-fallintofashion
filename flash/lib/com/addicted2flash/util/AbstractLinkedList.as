/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.util 
{
	import flash.errors.IllegalOperationError;
	
	import com.addicted2flash.util.AbstractLinkedCollection;
	import com.addicted2flash.util.IList;	

	/**
	 * This class provides a skeletal implementation of the IList interface for sequential data-structures
	 * to minimize the effort required to implement this interface.
	 * 
	 * @author Tim Richter
	 */
	public class AbstractLinkedList extends AbstractLinkedCollection implements IList 
	{
		/**
		 * Create a new <code>AbstractLinkedList </code>.
		 */
		public function AbstractLinkedList()
		{
			super();
		}
		
		/**
		 * @inheritDoc
		 */
		public function addAt( i: int, o: * ): void
		{
			if( i < _length )
			{
				var n: LinkedNode = getNodeAt( i );
				
				if( n == _first )
				{
					_first = new LinkedNode( o, null, _first );
				}
				else if( n == _last )
				{
					_last = new LinkedNode( o, null, _last );
				}
				else
				{
					n.postAdd( new LinkedNode( o, n, n.post ) );
				}
				
				++_length;
			}
			else
			{
				var j: int = 0;
				var len: int = i - _length;
				
				// -- add null elements
				for( ; j < len; ++j )
				{
					add( null );
				}
				
				add( o );
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function indexOf( o: * ): int
		{
			var n: LinkedNode = _first;
			var i: int = 0;
			
			while( n != null )
			{
				if( n.value == o )
				{
					return i;
				}
				
				++i;
				n = n.post;
			}
			
			return -1;
		}
		
		/**
		 * @inheritDoc
		 */
		public function lastIndexOf( o: * ): int
		{
			var n: LinkedNode = _last;
			var i: int = _length - 1;
			
			while( n != null )
			{
				if( n.value == o )
				{
					return i;
				}
				
				--i;
				n = n.pre;
			}
			
			return -1;
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeAt( i: int ): *
		{
			if( i >= _length || i < 0 )
			{
				return null;
			}
			else
			{
				var n: LinkedNode = getNodeAt( i );
				var v: * = n.value;
				
				removeNode( getNodeAt( i ) );
				
				return v;
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeArea( fromIndex: int, toIndex: int ): void
		{
			if( fromIndex >= toIndex || fromIndex == _length ) return;
			
			if( fromIndex < 0 ) fromIndex = 0;
			if( toIndex > _length ) toIndex = _length;
			
			var i: int = fromIndex;
			var n: LinkedNode = getNodeAt( i );
			var x: LinkedNode;
			
			while( n != null && i < toIndex )
			{
				x = n;
				n = n.post;
				
				x.clearAndCombine();
				
				++i;
			}
			
			if( fromIndex == 0 )
			{
				_first.clearAndCombine();
				_first = n;
			}
			
			if( toIndex == _length )
			{
				_last.clearAndCombine();
			}
			
			_length -= toIndex - fromIndex;
			
			if( _length == 0 )
			{
				_last.dispose();
				_last = null;
			}
		}

		/**
		 * @inheritDoc
		 */
		public function setAt( i: int, o: * ): *
		{
			if( i < 0 )
			{
				return null;
			}
			else if( i >= _length )
			{
				addAt( i, o );
				
				return null;
			}
			else
			{
				var node: LinkedNode = getNodeAt( i );
				var v: * = node.value;
				
				node.value = o;
				
				return v;
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function subList( fromIndex: int, toIndex: int ): IList
		{
			throw new IllegalOperationError( "abstract method needs to be overridden in subclasses!" );
		}
	}
}
