/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.util 
{
	import com.addicted2flash.data.DataEventType;	
	
	/**
	 * <code>Observable</code> object in the model-view-paradigm.
	 * 
	 * <p>This implementation of the observer-pattern is a specific one with the opportunity
	 * to notify observers manually.</p>
	 * 
	 * <p>In addition it has the advantage to register for specific notifications, not for all the obervable is dispatching.</p>
	 * 
	 * @author Tim Richter
	 */
	public class Observable implements IObservable
	{
		protected var _ref: IObservable;
		protected var _observers: ISet;
		protected var _specificObservers: IMap;

		/**
		 * Create a new <code>Observable</code>.
		 * 
		 * @param reference (optional) the referenced target of notifications (useful for aggregation)
		 */
		public function Observable( reference: IObservable = null ) 
		{
			_ref = ( reference == null ) ? this : reference;
		}

		/**
		 * @inheritDoc
		 */
		public function addObserver( o: IObserver ): void
		{
			if( o is ISpecificObserver )
			{
				var notifications: Array = ISpecificObserver( o ).acceptedNotifications();
				var n: int = notifications.length;
				var list: ISet;
				var note: String;
				
				if( n > 0 && _specificObservers == null )
				{
					_specificObservers = new HashMap();
				}
				
				while( --n > -1 )
				{
					note = notifications[ n ];
					
					list = _specificObservers.getValue( note );
					
					if( list == null )
					{
						list = new ArraySet();
						
						_specificObservers.put( note, list );
					}
					
					list.add( o );
				}
			}
			else if( o is IObserver )
			{
				if( _observers == null )
				{
					_observers = new ArraySet();
				}
				
				_observers.add( o );
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeObserver( o: IObserver ): void
		{
			if( o is ISpecificObserver )
			{
				if( _specificObservers != null )
				{
					var notifications: Array = ISpecificObserver( o ).acceptedNotifications();
					var n: int = notifications.length;
					var list: ISet;
					var note: String;
					
					while( --n > -1 )
					{
						note = notifications[ n ];
						
						list = ArraySet( _specificObservers.getValue( note ) );
						
						if( list != null )
						{
							list.remove( o );
							
							if( list.isEmpty() )
							{
								list.dispose();
								list = null;
								
								_specificObservers.remove( note );
							}
						}
					}
					
					if( _specificObservers.size() == 0 )
					{
						_specificObservers.dispose();
						_specificObservers = null;
					}
				}
			}
			else if( o is IObserver )
			{
				if( _observers != null )
				{
					_observers.remove( o );
					
					if( _observers.size() == 0 )
					{
						_observers.dispose();
						_observers = null;
					}
				}
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeObservers(): void
		{
			dispose();
		}
		
		/**
		 * @inheritDoc
		 */
		public function get observerAmount(): int
		{
			return ( _observers == null ? 0 : _observers.size() ) + ( _specificObservers == null ? 0 : _specificObservers.size() );
		}
		
		/**
		 * @inheritDoc
		 */
		public function notifyObservers( type: String = null, data: * = null ): void
		{
			var a: Array;
			var n: int;
			var i: int;
			
			if( type != null )
			{
				if( _specificObservers != null )
				{
					var list: ISet = ArraySet( _specificObservers.getValue( type ) );
					
					if( list != null )
					{
						a = list.toArray();
						n = a.length;
						
						for( i = 0; i < n; ++i )
						{	
							ISpecificObserver( a[ i ] ).update( _ref, type, data );
						}
					}
				}
			}
			else
			{
				type = DataEventType.UNSPECIFIC;
			}
			
			if( _observers != null )
			{
				a = _observers.toArray();
				n = a.length;
				
				for( i = 0; i < n; ++i )
				{
					IObserver( a[ i ] ).update( this, type, data );
				}
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function clone(): ICloneable
		{
			var clone: Observable = new Observable();
			if( _observers != null ) clone._observers = LinkedSet( _observers.clone() );
			if( _specificObservers != null ) clone._specificObservers = IMap( _specificObservers.clone() );
			
			return clone;
		}
		
		/**
		 * @inheritDoc
		 */
		public function dispose(): void
		{
			if( _specificObservers != null )
			{
				_specificObservers.dispose();
				_specificObservers = null;
			}
			
			if( _observers != null )
			{
				_observers.dispose();
				_observers = null;
			}
			
			_ref = null;
		}

		/**
		 * String representation of <code>Observable</code>.
		 * 
		 * @return String representation of <code>Observable</code>
		 */
		public function toString(): String
		{
			return "[ Observable observer count=" + observerAmount + " ]";
		}
	}
}
