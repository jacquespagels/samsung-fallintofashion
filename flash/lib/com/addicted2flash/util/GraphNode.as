/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.util 
{
	import com.addicted2flash.util.iterator.IIterator;		

	/**
	 * This class represents a node in a directed graph.
	 * 
	 * @author Tim Richter
	 */
	public class GraphNode 
	{
		private var _value: *;
		private var _edges: IList;

		/**
		 * Create a new <code>GraphNode</code>
		 * 
		 * @param value any data
		 */
		public function GraphNode( value: * ) 
		{
			_value = value;
			
			_edges = new LinkedList();
		}
		
		/**
		 * add an edge between a <code>GraphNode</code> and this instance with a given weight.
		 * 
		 * @param node <code>GraphNode</code>
		 * @param weight weight of the edge
		 * @return true if edge was created (false if edge already exist)
		 */
		public function addEdge( node: GraphNode, weight: Number ): Boolean
		{
			if( !hasEdgeTo( node ) )
			{
				_edges.add( new GraphEdge( this, node, weight ) );
					
				node.addEdge( this, weight );
				
				return true;
			}
			
			return false;
		}
		
		/**
		 * remove the edge between a <code>GraphNode</code> and this instance.
		 * 
		 * @param node <code>GraphNode</code
		 * @return true if edge was removed
		 */
		public function removeEdge( node: GraphNode ): Boolean
		{
			var edge: GraphEdge = getEdgeTo( node );
			
			if( edge != null )
			{
				_edges.remove( edge );
				
				node.removeEdge( this );
				
				return true;
			}
			
			return false;			
		}
		
		/**
		 * return true if this instance is connected via an edge with a given <code>GraphNode</code>.
		 * 
		 * @return true if this instance is connected via an edge with a given <code>GraphNode</code>
		 */
		public function hasEdgeTo( node: GraphNode ): Boolean
		{
			var iter: IIterator = _edges.iterator();
			var edge: GraphEdge;
			
			while( iter.hasNext() )
			{
				edge = GraphEdge( iter.next() );
				
				if( edge.getPartner( node ) == this )
				{
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * return the <code>GraphEdge</code> between this instance an a given <code>GraphNode</code>.
		 * 
		 * @return GraphEdge between this instance an a given <code>GraphNode</code>
		 */
		public function getEdgeTo( node: GraphNode ): GraphEdge
		{
			var iter: IIterator = _edges.iterator();
			var edge: GraphEdge;
			
			while( iter.hasNext() )
			{
				edge = GraphEdge( iter.next() );
				
				if( edge.getPartner( node ) == this )
				{
					return edge;
				}
			}
			
			return null;
		}
		
		/**
		 * return the value of the <code>GraphNode</code>.
		 * 
		 * @return any value
		 */
		public function get value(): *
		{
			return _value;
		}
		
		/**
		 * set the value of the <code>GraphNode</code>.
		 * 
		 * @param value any value
		 */
		public function set value( value: * ): void
		{
			_value = value;
		}
		
		/**
		 * return list of edges from this <code>GraphNode</code>.
		 * 
		 * @return list of edges from this <code>GraphNode</code>
		 */
		public function get edges(): ICollection
		{
			return _edges;
		}
		
		/**
		 * return amount of edges.
		 * 
		 * @return amount of edges
		 */
		public function get numEdges(): int
		{
			return _edges.size();
		}
		
		/**
		 * String representation of <code>GraphNode</code>.
		 * 
		 * @return String representation of <code>GraphNode</code>
		 */
		public function toString(): String
		{
			return "[ GraphNode value=" + _value + " edges=" + _edges + " ]";
		}
	}
}
