/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.util 
{

	/**
	 * This class represents an weighted edge in a directed graph.
	 * The weight of the edge can be manipulated afterwards. This is often necessary, if the position of the nodes in the
	 * graph change during application lifecycle.
	 * 
	 * @author Tim Richter
	 */
	public class GraphEdge 
	{
		private var _node1: GraphNode;
		private var _node2: GraphNode;
		private var _weight: Number;
		
		/**
		 * Create a new <code>GraphEdge</code>.
		 * 
		 * @param node1 first node (no numeric order)
		 * @param node2 second node (no numeric order)
		 * @param weight initial weight of the edge
		 */
		public function GraphEdge( node1: GraphNode, node2: GraphNode, weight: Number ) 
		{
			_node1 = node1;
			_node2 = node2;
			_weight = weight;
		}
		
		/**
		 * return partner node of a given <code>GraphNode</code> of the edge.
		 * 
		 * @param node one of the two <code>GraphNode</code>s of the edge
		 * @return partner node or null if partner doesn't exist
		 */
		public function getPartner( node: GraphNode ): GraphNode
		{
			if( _node1 != node && _node2 != node )
			{
				return null;
			}
			
			return node == _node1 ? _node1 : _node2;
		}
		
		/**
		 * return first <code>GraphNode</code> (no numeric order).
		 * 
		 * @return GraphNode
		 */
		public function getFirstNode(): GraphNode
		{
			return _node1;
		}
		
		/**
		 * return second <code>GraphNode</code> (no numeric order).
		 * 
		 * @return GraphNode
		 */
		public function getSecondNode(): GraphNode
		{
			return _node2;
		}
		
		/**
		 * return weight of the edge.
		 * 
		 * @return weight of the edge
		 */
		public function get weight(): Number
		{
			return _weight;
		}
		
		/**
		 * set weight of the edge.
		 * 
		 * @param weight weight of the edge
		 */
		public function set weight( weight: Number ): void
		{
			_weight = weight;
		}
		
		/**
		 * String representation of <code>GraphEdge</code>.
		 * 
		 * @return String representation of <code>GraphEdge</code>
		 */
		public function toString(): String
		{
			return "[ ArrayStack node1=" + _node1 + " node2=" + _node2 + " weight=" + _weight + " ]";
		}
	}
}
