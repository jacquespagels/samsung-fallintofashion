/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.util 
{

	/**
	 * This class represents a directed graph.
	 * 
	 * @author Tim Richter
	 */
	public class Graph implements ICloneable 
	{
		private var _nodes: HashMap;
		
		/**
		 * Create a new <code>Graph</code>
		 */
		public function Graph()
		{
		}

		/**
		 * add an edge between two elements with a given weight.
		 * This weight can be changed afterwards by getting the <code>GraphEdge</code>
		 * with <code>getEdge( o1: *, o2: * ): GraphEdge { ... }</code>.
		 * 
		 * @param o1 first data element (no numeric order)
		 * @param o2 second data element (no numeric order)
		 * @param weight initial weight of the edge
		 * @return true if edge was created (false if edge already exist)
		 */
		public function addEdge( o1: *, o2: *, weight: Number ): Boolean
		{
			var n1: GraphNode = _nodes.getValue( o1 );
			var n2: GraphNode = _nodes.getValue( o2 );
			
			if( n1 != null && n2 != null )
			{
				return n1.addEdge( n2, weight );
			}
			
			return false;
		}
		
		/**
		 * return edge between two elements.
		 * This method has general use for manipulating the weight of an edge.
		 * 
		 * @param o1 first data element (no numeric order)
		 * @param o2 second data element (no numeric order)
		 * @return edge between two elements or null, if it doesn't exists
		 */
		public function getEdge( o1: *, o2: * ): GraphEdge
		{
			var n1: GraphNode = _nodes.getValue( o1 );
			var n2: GraphNode = _nodes.getValue( o2 );
			
			if( n1 != null && n2 != null )
			{
				return n1.getEdgeTo( n2 );
			}
			
			return null;
		}
		
		/**
		 * removes an edge between two elements.
		 * 
		 * @param o1 first data element (no numeric order)
		 * @param o2 second data element (no numeric order)
		 * @return true if edge was removed
		 */
		public function removeEdge( o1: *, o2: * ): Boolean
		{
			var n1: GraphNode = _nodes.getValue( o1 );
			var n2: GraphNode = _nodes.getValue( o2 );
			
			if( n1 != null && n2 != null )
			{
				return n1.removeEdge( n2 );
			}
			
			return false;
		}
		
		/**
		 * add a node to the graph with a given value.
		 * 
		 * @param o data element
		 * @return true if node was created (false if node already exist)
		 */
		public function addNode( o: * ): Boolean
		{
			if( !_nodes.containsKey( o ) )
			{
				_nodes.put( o, new GraphNode( o ) );
				
				return true;
			}
			
			return false;
		}
		
		/**
		 * return <code>GraphNode</code> of a given value.
		 * 
		 * @param o data element
		 * @return GraphNode of given value or null if it doen't exist
		 */
		public function getNode( o: * ): GraphNode
		{
			return _nodes.getValue( o );
		}

		/**
		 * remove a node from the graph.
		 * 
		 * @param o data element
		 * @return true if node was part of the graph
		 */
		public function removeNode( o: * ): Boolean
		{
			return _nodes.remove( o ) != null;
		}
		
		/**
		 * return amount of nodes in this graph.
		 * 
		 * @return amount of nodes in this graph
		 */
		public function numNodes(): int
		{
			return _nodes.size();
		}

		/**
		 * @inheritDoc
		 */
		public function clone(): ICloneable
		{
			var clone: Graph = new Graph();
			clone._nodes = HashMap( _nodes.clone() );
			
			return clone;
		}
		
		/**
		 * String representation of <code>Graph</code>.
		 * 
		 * @return String representation of <code>Graph</code>
		 */
		public function toString(): String
		{
			return "[ Graph nodes=" + _nodes + " ]";
		}
	}
}
