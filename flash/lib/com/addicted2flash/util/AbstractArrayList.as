/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.util
{
	import flash.errors.IllegalOperationError;	
	
	import com.addicted2flash.util.AbstractArrayCollection;
	import com.addicted2flash.util.IList;   

	/**
	 * This class provides a skeletal implementation of the IList interface for arrays to minimize
	 * the effort required to implement this interface.
	 *
	 * @author Tim Richter
	 */
	public class AbstractArrayList extends AbstractArrayCollection implements IList
	{
		/**
		 * Create a new <code>AbstractArrayList</code>
		 *
		 * @param field (optional) an existing Array
		 */
		public function AbstractArrayList( field: Array = null )
		{
			super( field );
		}

		/**
		 * @inheritDoc
		 */
		public function addAt( i: int, o: * ): void
		{
			if( i < size() )
			{
				_list.splice( i, 0, o );
			}
            else
			{
				_list[ i ] = o;
			}
		}

		/**
		 * @inheritDoc
		 */
		public function indexOf( o: * ): int
		{
			return _list.indexOf( o );
		}

		/**
		 * @inheritDoc
		 */
		public function lastIndexOf( o: * ): int
		{
			return _list.lastIndexOf( o );
		}

		/**
		 * @inheritDoc
		 */
		public function removeAt( i: int ): *
		{
			if( i < size() )
			{
				return _list.splice( i, 1 )[ 0 ];
			}
            else
			{
				return null;
			}
		}

		/**
		 * @inheritDoc
		 */
		public function removeArea( fromIndex: int, toIndex: int ): void
		{
			if( fromIndex < toIndex )
			{
				_list.splice( fromIndex, toIndex );
			}
		}

		/**
		 * @inheritDoc
		 */
		public function setAt( i: int, o: * ): *
		{
			addAt( i, o );
			
			return removeAt( i + 1 );
		}

		/**
		 * @inheritDoc
		 */
		public function subList( fromIndex: int, toIndex: int ): IList
		{
			throw new IllegalOperationError( "abstract method needs to be overridden in subclasses!" );
		}
	}
}