/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.util 
{
	import com.addicted2flash.util.IMicroObservable;	

	/**
	 * This class represents an observer-pattern-based implementation with only one value in the model.
	 * Setting the value will notify all observers.
	 * 
	 * @author Tim Richter
	 */
	public class MicroObservable implements IMicroObservable 
	{
		protected var _ref: IMicroObservable;
		protected var _observers: ISet;
		protected var _value: *;
		protected var _hasChanged: Boolean;

		/**
		 * Create a new <code>MicroObservable</code>.
		 * 
		 * @param reference (optional) the referenced target of notifications (useful for aggregation)
		 */
		public function MicroObservable( reference: IMicroObservable = null ) 
		{
			_ref = ( reference == null ) ? this : reference;
			_hasChanged = false;
		}

		/**
		 * @inheritDoc
		 */
		public function addObserver( o: IMicroObserver ): void
		{
			if( _observers == null ) _observers = new ArraySet();
			
			_observers.add( o );
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeObserver( o: IMicroObserver ): void
		{
			if( _observers != null ) _observers.remove( o );
		}
		
		/**
		 * @inheritDoc
		 */
		public function removeObservers(): void
		{
			if( _observers != null ) _observers.clear();
		}
		
		/**
		 * @inheritDoc
		 */
		public function get observerAmount(): int
		{
			return ( _observers == null ) ? 0 : _observers.size();
		}
		
		/**
		 * @inheritDoc
		 */
		public function get value(): *
		{
			return _value;
		}
		
		/**
		 * @inheritDoc
		 */
		public function set value( value: * ): void
		{
			_value = value;
			
			_hasChanged = true;
			
			notifyObservers();
		}
		
		/**
		 * @inheritDoc
		 */
		public function get isAvailable(): Boolean
		{
			return _hasChanged;
		}
		
		
		/**
		 * Each registered <code>IMicroObserver</code> will be notified.
		 */
		protected function notifyObservers(): void
		{
			if( isAvailable && _observers != null )
			{
				var a: Array = _observers.toArray();
				var n: int = a.length;
				var i: int = 0;
				
				for( ; i < n; ++i )
				{
					IMicroObserver( a[ i ] ).microUpdate( this );
				}
			}
		}
		
		/**
		 * @inheritDoc
		 */
		public function dispose(): void
		{
			_value = null;
			_ref = null;
			
			if( _observers != null )
			{
				_observers.dispose();
				_observers = null;
			}
		}
		
		/**
		 * String representation of <code>MicroObservable</code>.
		 * 
		 * @return String representation of <code>MicroObservable</code>
		 */
		public function toString(): String
		{
			return "[ MicroObservable observer count=" + observerAmount + " ]";
		}
	}
}
