/**
 * Copyright (c) 2008 Tim Richter, www.addicted2flash.com
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package com.addicted2flash.util 
{

	/**
	 * Helper class for building sequential data-structures.
	 * 
	 * @author Tim Richter
	 */
	public class LinkedNode 
	{
		public var post: LinkedNode;
		public var pre: LinkedNode;
		public var value: *;
		
		/**
		 * Create a new <code>LinkedNode</code>.
		 * 
		 * @param value (optional) any value
		 * @param pre (optional) predecessor of node
		 * @param post (optional) post node
		 */
		public function LinkedNode( value: * = null, pre: LinkedNode = null, post: LinkedNode = null ) 
		{
			this.value = value;
			this.pre = pre;
			this.post = post;
			
			if( pre != null )
			{
				pre.postAdd( this );
			}
			if( post != null )
			{
				post.preAdd( this );
			}
		}

		/**
		 * add a node as predecessor ( also handles dependencies f.e pre/post ).
		 * 
		 * @param node predecessor
		 */
		public function preAdd( node: LinkedNode ): void
		{
			if( pre != null )
			{
				pre.post = node;
			}
			
			node.pre = pre;
			node.post = this;
			pre = node;
		}
		
		/**
		 * add a node as post node ( also handles dependencies f.e pre/post ).
		 * 
		 * @param node post node
		 */
		public function postAdd( node: LinkedNode ): void
		{
			if( post != null )
			{
				post.pre = node;
			}
			
			node.post = post;
			node.pre = this;
			post = node;
		}
		
		/**
		 * shift value to the left (value of pre node and this node change).
		 */
		public function shiftLeft(): void
		{
			if( pre != null )
			{
				var v: * = pre.value;
				
				pre.value = value;
				
				value = v;
			}
		}
		
		/**
		 * shift value to the left (value of post node and this node change).
		 */
		public function shiftRight(): void
		{
			if( post != null )
			{
				var v: * = post.value;
				
				post.value = value;
				
				value = v;
			}
		}
		
		/**
		 * clear instance and combine pre and post node (if existing).
		 */
		public function clearAndCombine(): void
		{
			if( pre != null )
			{
				pre.post = post;
			}
			if( post != null )
			{
				post.pre = pre;
			}
			
			dispose();
		}
		
		/**
		 * clear all internal dependencies.
		 */
		public function dispose(): void
		{
			pre = null;
			post = null;
			value = null;
		}
		
		/**
		 * recursive dispose.
		 */
		public function purge(): void
		{
			pre = null;
			value = null;
			post.dispose();
			post = null;
		}
		
		/**
		 * string representation of <code>LinkedNode</code>.
		 * 
		 * @return String representation of <code>LinkedNode</code>
		 */
		public function toString(): String
		{
			return "[ LinkedNode pre=" + pre + " post=" + post + " value=" + value + " ]";
		}
	}
}
