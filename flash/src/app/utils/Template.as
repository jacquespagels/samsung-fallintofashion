package app.utils 
{
	import app.display.controls.NavButton;
	import assets.placeholders.ButtonPlaceholder;
	import core.utils.Template;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Template extends core.utils.Template 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function Template(template:DisplayObjectContainer, target:DisplayObjectContainer=null, xml:XML = null) 
			{
				super(template, target, xml);
			}
		
			override protected function initialize():void 
			{
				super.initialize();
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			/*
			protected function onButtonPlaceholder(placeholder:DisplayObject, node:XML):void 
			{
				// add button
					var button:NavButton = new NavButton(node);
					button.addEventListener(MouseEvent.CLICK, onButtonClick);
					//addObject(placeholder, button);
					
				// position button
					//setStyle(placeholder, button, XMLUtils.getClasses(node));
					
				// hide target
					return button;
			}
			*/
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}