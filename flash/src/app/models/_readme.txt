
	MODEL STRUCTURE
	
		Core Models
		
			XMLModel			Wrapper class for XML which exposes various helper functions to get nodes from XML without writing E4X
			Model				Wrapper class for XMLModel data that all supsequent model classes extend
			
		Data Models
		
			DataModel			Holds the site's raw XML data
			QuizModel			Holds the logic for the quiz section of the site
			
		Value Models
		
			ProductModel		Base class for DeviceModel and CoverModel classes
				DeviceModel		Holds data about the chosen device
				CoverModel		Holds data about the chosen cover
			
			LookModel			Stores data about the fashion looks
			ColorModel			Object to hold id, name, and hex (color) value of colors
			TipModel			Holds text and image data regarding the fashion looks
