package app.models 
{
	import flash.events.DataEvent;
	import flash.events.Event;
	import core.models.Model;
	
	/**
	 * Contains the data/textual content and translations for the entire site, as well as holding the properties for the application such look and device
	 * @author Dave Stewart
	 */
	public class DataModel extends Model 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				protected static var INSTANCE	:DataModel;

				
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function DataModel(data:XML) 
			{
				// create
					INSTANCE = this;
					super(data);
			}
			
			public static function getNode(expression:String):XML
			{
				return INSTANCE.data.getNode(expression);
			}
			
			static public function getNodes(expression:String):XMLList
			{
				return INSTANCE.data.getNodes(expression);
			}
			
			//TODO Add in more static methods to slice and dice the data that already exists here so model methods aren't so unweildy

			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public static function get instance():DataModel
			{
				return INSTANCE;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}