package app.models.products 
{
	import app.models.DataModel;
	import app.models.LookModel;
	import app.models.vo.ColorModel;
	import core.models.XMLModel;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class DeviceModel extends ProductModel
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				protected var _cover			:CoverModel;
				protected var _deviceId			:String;
				
			// variables
				protected var rand				:int;
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			/**
			 * 
			 * @param	xml			<div id="note3"> ... </div>
			 * @param	deviceId
			 */
			public function DeviceModel(xml:XML, deviceId:String, colorId:String = null) 
			{
				_deviceId	= deviceId;
				super(xml);
				setCover('flip');
				rand = Math.random() * 10000;
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: static functions
		
			/**
			 * Return a single DeviceModel instance from an id
			 * @param	deviceId
			 */
			public static function factory(deviceId:String = null, colorId:String = null):DeviceModel 
			{
				// device
					var xml:XML	= deviceId
									? DataModel.getNode('#products #' + deviceId)	// named
									: DataModel.getNode('#products div');			// first 
					
				// resolve any missing device id
					deviceId = deviceId || xml.attribute('id');
					
				// temp XMLModel
					var model:XMLModel = new XMLModel(xml);
					
				// color node
					xml			= colorId
									? model.getNode('#device #' + colorId)		// named
									: model.getNode('#device ~color')			// first
									
				// return
					return new DeviceModel(xml, deviceId);
			}
			
			/**
			 * Return all devices
			 * @return
			 */
			public static function all():Vector.<DeviceModel>
			{
				return DeviceModel.fromNodes(DataModel.getNode('#products').div)
			}
			
			/**
			 * Convert a list of nodes into 
			 * @param	nodes
			 * @return
			 */
			public static function fromNodes(nodes:XMLList):Vector.<DeviceModel>
			{
				// variables
					var models		:Vector.<DeviceModel>	= new Vector.<DeviceModel>;
					
				// loop over and create color models
					for each(var node:XML in nodes)
					{
						models.push(new DeviceModel(node, node.attribute('id')));
					}
					
				// return
					return models;
			}
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			/*
			public function clone():DeviceModel
			{
				var model:DeviceModel = DeviceModel.factory(deviceId, color.id);
				model.setCover(cover.coverId);
				model.cover.setColor(cover.color.id);
				return model;
			}
			*/
			
			override protected function initialize():void 
			{
				super.initialize();
			}
			
			public function save():void
			{
				trace('SAVING: ' + this);
				dispatchEvent(new Event(Event.CHANGE));
			}
		
			public function setFromLook(lookModel:LookModel):void
			{
				// colors
					var colors:XML = lookModel.getColors(deviceId);
					
					trace(colors)
					
				// set colors
					setColor(colors.@device);
					cover.setColor(colors.@cover);
			}
			
			public function setFromModel(deviceModel:DeviceModel):void 
			{
				setType(deviceModel.deviceId);
				setColor(deviceModel.color.id)
				setCover(deviceModel.cover.coverId);
				cover.setColor(deviceModel.cover.color.id);
			}
		

			
			override public function setType(deviceId:String = null):void 
			{
				//debug
					trace('Set device type: ' + deviceId);
				
				// grab the new node
					var model:DeviceModel = DeviceModel.factory(deviceId);
					
				// replace the XML
					_data = new XMLModel(model.data.xml);
					_deviceId	= deviceId;

					
				// reset the cover
					setCover();
			}
			
			override public function setColor(colorId:String = null):void
			{
				_data = DeviceModel.factory(_deviceId, colorId).data;
			}
			
			public function setCover(coverId:String = null):void
			{
				_cover = CoverModel.factory(deviceId, coverId);
			}
			
			override public function getColors():Vector.<ColorModel>
			{
				return ColorModel.fromNodes(DataModel.getNodes('#products #' + deviceId + ' #device ~color'));
				//return ColorModel.fromNodes(data.getNodes('#device ~color'))
			}
			
			public function getVideoColors():Vector.<ColorModel>
			{
				return ColorModel.fromNodes(DataModel.getNodes('#products #' + deviceId + ' #device @video'));
				//return ColorModel.fromNodes(data.getNodes('#device @video'));
			}
			
			public function getCovers():Vector.<CoverModel>
			{
				return CoverModel.all(deviceId);
			}
			
			public function getText(template:String):String
			{
				return template
					.replace('{device_id}', type)
					.replace('{device_color_id}', color.id)
					.replace('{cover_id}', cover.type)
					.replace('{cover_color_id}', cover.color.id)
					.replace('{device_name}', name)
					.replace('{device_color}', color.name)
					.replace('{cover_name}', cover.name)
					.replace('{cover_color}', cover.color.name);
			}

			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			override public function get id():String { return data.xml.@id; }
		
			public function get deviceId():String { return _deviceId; }
			
			override public function get type():String { return _deviceId; }
			
			override public function get name():String { return DataModel.getNode('#products #' +deviceId+ ' h3'); }
			
			public function get link():String { return DataModel.getNode('#products #' +deviceId+ ' ~link'); }
			
			public function get features():XMLList { return DataModel.instance.data.getNodes('#products #' +deviceId+ ' ~feature'); }
			
			public function get cover():CoverModel { return _cover; }
			
			

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			/**
			 * Gets a cover based on 
			 * @usage	var item:XML = getCover('s3', 'flip', 'blush_pink');
			 * @param	deviceType
			 * @param	coverType
			 * @param	colorId
			 * @return
			 */
			public function xmlGetCover(deviceType:String, coverType:String, colorId:String):XML
			{
				var item:XMLList = 
					data.xml
						.div.(attribute('id') == type)
						.div.(attribute('id') == 'covers')
						.div.(attribute('id') == cover.type)
						.p.(attribute('id') == color.id);
				return item[0];
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			override public function toString():String 
			{
				return '[object DeviceModel type="' +deviceId+ '" color="' +color.id+ '" cover="' +cover.coverId+ '" coverColor="' +cover.color.id+ '" rand="' +rand+ '"]'
			}
			
	}

}