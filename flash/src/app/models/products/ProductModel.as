package app.models.products 
{
	import app.models.vo.ColorModel;
	import core.models.Model;

	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class ProductModel extends Model 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
			
				/// the type of product indicated by the parent block #id. id now becomes the colorId
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			/**
			 * @param	range	Block of data containing all the XML nodes for all the items in the range
			 */
			public function ProductModel(xml:XML)  
			{
				super(xml, true);
			}
		

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			/**
			 * Changes the type of product, e.g. from #note3 to #s4, or from #flip to #sview
			 * To do this, the model needs to look at the parent node, which will be the a of #devices, or #devices #covers
			 * @param	id
			 */
			public function setType(id:String = null):void 
			{
				throw new Error('This method must be overridden in the subclass');
			}
		
			
			/**
			 * Changes the color, e.g. from #red to #blue
			 * To do this, the model needs to look at the parent node
			 * @param	id
			 */
			public function setColor(colorId:String = null):void
			{
				throw new Error('This method must be overridden in the subclass');
			}
			
			/**
			 * Gets the range of colors as a ColorModel Vector
			 * @return
			 */
			public function getColors():Vector.<ColorModel>
			{
				throw new Error('This method must be overridden in the subclass');
				return ColorModel.fromNodes(data.getItems('color'));
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			override public function get id():String { return node.attribute('id') }
			
			public function get type():String { throw new Error('This method must be overridden in the subclass'); return null; }
		
			public function get name():String { return data.xml; }
			
			public function get hex():String { return node.attribute('hex'); }
			
			public function get front():String { return node.attribute('front'); }
			
			public function get back():String { return node.attribute('back'); }
			
			public function get sprite():String { return node.attribute('sprite'); }
			
			public function get video():String { return node.attribute('video'); }
			
			public function get color():ColorModel { return new ColorModel(node); }
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
		
			
	}

}