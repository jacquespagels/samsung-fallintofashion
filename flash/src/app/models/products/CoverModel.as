package app.models.products 
{
	import app.models.DataModel;
	import app.models.vo.ColorModel;
	import core.models.XMLModel;
	
	
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class CoverModel extends ProductModel 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				protected var _deviceId		:String;
				protected var _coverId		:String;
				
				
			// variables
			
				/// deviceId	= note3, s4, s4-mini
				/// coverId		= flip, sview
				/// id			= red, blue, green

				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			/**
			 * 
			 * @param	xml				<p item="color" code="" hex="#111316" width="210" height="400" id="jet_black" front="../../../resources/products/note3/flip/00_EF-WN900B_Standard_Jet-Black-Front.png" back="../../../resources/products/note3/flip/01_EF-WN900B_Standard_Jet-Black-Back.png">Jet Black</p>
			 * @param	deviceId
			 * @param	coverId
			 */
			public function CoverModel(xml:XML, deviceId:String, coverId:String, colorId:String = null) 
			{
				_deviceId	= deviceId;
				_coverId	= coverId;
				super(xml);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: static functions
		
			/**
			 * Return a CoverModel instance from an id
			 * @param	deviceId
			 * @param	coverId
			 */
			/*
			public static function factory(deviceId:String, coverId:String = null, colorId:String = null):CoverModel
			{
				// need to get the cover block first...
				var xml:XML = coverId
								? colorId
									? DataModel.getNode('#products #' + deviceId + ' #covers #' + coverId + ' #' + colorId)	// named
									: DataModel.getNode('#products #' + deviceId + ' #covers #' + coverId + ' ~color')		// first
								: colorId
									? DataModel.getNode('#products #' + deviceId + ' #covers div #' + colorId)				// named
									: DataModel.getNode('#products #' + deviceId + ' #covers div ~color');					// first
										
				return new CoverModel(xml, deviceId, coverId);
			}
			*/
			
			public static function factory(deviceId:String, coverId:String = null, colorId:String = null):CoverModel
			{
				// cover
					var xml:XML = coverId
								? DataModel.getNode('#products #' + deviceId + ' #covers #' + coverId)	// named
								: DataModel.getNode('#products #' + deviceId + ' #covers div')			// first
								
				// resolve any missing device id
					coverId = coverId || xml.attribute('id');
					
				// temp XMLModel
					var model:XMLModel = new XMLModel(xml);
					
				// color node
					xml			= colorId
									? model.getNode('#' + colorId)		// named
									: model.getNode('~color')			// first
									
				// return
					return new CoverModel(xml, deviceId, coverId);
			}
			
			public static function all(deviceId:String):Vector.<CoverModel>
			{
				return CoverModel.fromNodes(DataModel.getNode('#products #' + deviceId + ' #covers').div, deviceId)
			}
			
			public static function fromNodes(nodes:XMLList, deviceId:String):Vector.<CoverModel>
			{
				// variables
					var models		:Vector.<CoverModel>	= new Vector.<CoverModel>;
					
				// loop over and create color models
					for each(var node:XML in nodes)
					{
						models.push(new CoverModel(node, deviceId, node.attribute('id')));
					}
					
				// return
					return models;
			}

			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			override public function getColors():Vector.<ColorModel> 
			{
				return ColorModel.fromNodes(DataModel.getNodes('#products #' + deviceId + ' #covers #' +coverId+ ' ~color'));
			}
		
			override public function setType(coverId:String = null):void 
			{
				// grab the new node
					var model:CoverModel = CoverModel.factory(deviceId, coverId);
					
				// replace the XML
					_data = new XMLModel(model.data.xml);
			}
			
			override public function setColor(colorId:String = null):void
			{
				_data = CoverModel.factory(_deviceId, coverId, colorId).data;
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get deviceId():String { return _deviceId; }
			
			public function get coverId():String { return _coverId; }
			
			override public function get type():String { return _coverId; }
			
			override public function get name():String 
			{
				return DataModel.getNode('#common ~' + _coverId);	// grab this from the common settings, so we're not adding it to ALL the translations
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			override public function toString():String 
			{
				return '[object CoverModel type="' +coverId+ '" color="' +id+ '"]'
			}
	}

}