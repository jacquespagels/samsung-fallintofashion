package app.models 
{
	import app.models.vo.ColorModel;
	import assets.screens.Look;
	import com.flashfactory.calico.utils.ColorMathUtil;
	import core.models.Model;
	import core.net.Loader;
	import app.models.vo.TipModel;

	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class LookModel extends Model 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				protected var _tips		:Vector.<TipModel>;

			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function LookModel(xml:XML) 
			{
				super(xml);
			}
			
			override protected function initialize():void 
			{
				// super
					super.initialize();
					
				// create the tips
					_tips					= new Vector.<TipModel>;
					var nodes:XMLList		= data.getNodes('a');
					for each(var node:XML in nodes)
					{
						tips.push(new TipModel(node));
					}
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: static functions
		
			/**
			 * Return a LookModel instance from an id
			 * @param	id
			 */
			public static function factory(lookId:String):LookModel 
			{
				var xml:XML = DataModel.getNode('#looks #' + lookId);
				return new LookModel(xml);
			}
			
			/**
			 * Returns all looks as LookModels
			 * @return
			 */
			public static function all():Vector.<LookModel>
			{
				var models	:Vector.<LookModel>		= new Vector.<LookModel>;
				var nodes	:XMLList				= DataModel.getNode('#looks').div;
				for each(var node:XML in nodes)
				{
					models.push(new LookModel(node));
				}
				return models;
			}
			
			/**
			 * Gets a look according to a color
			 * @param	hex
			 */
			public static function fromColorValue(value:int):LookModel
			{
				// convert the value to hsv
					var hex			:String;
					var hsv			:Array;
					var HSV			:Array	= ColorMathUtil.hexToHsv(value);
					var distance	:int	= 360;
					var dist		:Number;
					var a:Number, b:Number;
				

				// grab the hex values for all looks
					var id			:String;
					var looks		:XMLList = DataModel.getNode('#looks').div;
				
				// compare each one to the chosen hex value
					for each (var node:XML in looks)
					{
						// get the color value
							hex 	= String(node.attribute('hex')).replace('#', '');
							value	= parseInt(hex, 16);
							hsv 	= ColorMathUtil.hexToHsv(value)
							
						// distance
							a		= hsv[0] * Math.PI/180;
							b		= HSV[0] * Math.PI/180;
							dist	= Math.abs(Math.atan2(Math.sin(a - b), Math.cos(a - b)));
							
						// compare
							if (dist < distance)
							{
								distance = dist;
								id = node.attribute('id');
							}
						
						// dump
							trace(node.attribute('id'), a, dist);
					}

				// debug
					trace(id, dist);				
				
				// return look
					return factory(id);
			}
			

			
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function setLook(lookId:String):LookModel
			{
				var model:LookModel = LookModel.factory(lookId);
				this._data = model.data;
				initialize();
				return this;
			}
		
			public function getTip(index:int):TipModel
			{
				return tips[index];
			}

			public function getColors(deviceId:String):XML
			{
				return data.getNode('~colors ~' + deviceId);
			}
			
			public function loadURLs(deviceId:String, onComplete:Function):Loader
			{
				var loader:Loader = new Loader('look loader');
				
				return loader;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get color():ColorModel
			{
				var id		:String = data.xml.attribute('id');
				var hex		:String = data.xml.attribute('hex');
				var xml		:XML	= <p item="color" code="" hex={hex} id="{id}">{id}</p>
				return new ColorModel(xml);
			}
		
			public function get name():String{ return data.getItem('name'); }
		
			public function get tips():Vector.<TipModel> { return _tips; }
			
			public function get urls():Array
			{
				var urls:Array = [];
				for each(var tip:TipModel in tips)
				{
					urls.push(tip.url);
				}
				return urls;
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			public override function toString():String 
			{
				return '[object LookModel name="' +name+ '"]';
			}
	}

}

