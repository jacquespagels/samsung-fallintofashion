package app.models.vo 
{
	import core.models.Model;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class TipModel extends Model 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function TipModel(data:XML) 
			{
				super(data);
			}
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get text():String
			{
				return data.xml;
			}
			
			public function get url():String
			{
				return data.xml.attribute('href');
			}
	
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			public override function toString():String 
			{
				return '[object TipModel tip="' +text+ '" url="' +url+ '"]';
			}
			
	}

}