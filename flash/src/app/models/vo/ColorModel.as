package app.models.vo 
{
	import core.models.XMLModel;
	import core.utils.ColorUtils;
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class ColorModel extends XMLModel 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function ColorModel(data:XML) 
			{
				super(data);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public static function fromNodes(nodes:XMLList):Vector.<ColorModel>
			{
				// variables
					var models		:Vector.<ColorModel>	= new Vector.<ColorModel>;
					
				// loop over and create color models
					for each(var node:XML in nodes)
					{
						models.push(new ColorModel(node));
					}
					
				// return
					return models;
			}
			
			    
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get id():String
			{
				return xml.attribute('id');
			}
		
			public function get name():String
			{
				return xml;
			}
			
			public function get hex():String
			{
				return xml.attribute('hex');
			}
			
			public function get value():int	
			{
				return ColorUtils.HEXStringToHEX(hex);
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}