package app.models 
{
	import app.Application;
	import core.models.Model;
	import core.models.XMLModel;

	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class QuizModel extends Model 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// variables
				protected var question			:XMLModel;
				protected var questionId		:String
				protected var questionAnswered	:Boolean;
				
			// properties
				public var lookId				:String;
				public var deviceId				:String;
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function QuizModel() 
			{
				Application.instance.quizModel = this;
				super(DataModel.getNode('#quiz'));
			}
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function hasQuestion():Boolean
			{
				return questionAnswered && deviceId == null;
			}
		
			/**
			 * Gets the next question, based on the last-generated questionId
			 * @return
			 */
			public function getQuestion(questionId:String = null):XMLModel
			{
				// resolve the next question to get
					if ( ! questionId )
					{
						questionId = this.questionId || '#q1';
					}
					
				// grab the next question
					var xml:XML		= data.getNode(questionId);
					question		= new XMLModel(xml)
					
				// set the question as not yet answered
					questionAnswered = false;
					
				// return
					return question; 
			}
			
			
			
			/**
			 * Sets the answer to the last-asked question, and generates the next questionId
			 * @param	index
			 */
			public function setAnswer(index:int):void
			{
				if (question && question.xml)
				{
					// set answered
						questionAnswered = true;
						
					// variables
						var answers		:XMLList	= question.getItems('answer');
						var answer		:XML		= answers[index].a[0];
						var deviceId	:String		= answer.attribute('device');
						var lookId		:String		= answer.attribute('look');
						
					// update the questionId
						questionId					= answer.@href;
						
					// properties
						if (lookId)
						{
							this.lookId	= lookId;
						}
						if (deviceId)
						{
							this.deviceId = deviceId;
						}
					
				}
			}
			
			public function reset():void 
			{
				lookId			= null;
				deviceId		= null;
				question		= null;
				questionId		= null;
			}
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}