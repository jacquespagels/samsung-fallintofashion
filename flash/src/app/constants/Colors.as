package app.constants 
{
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Colors 
	{
		
		public static const WHITE			:int		= 0xFFFFFF;
		public static const BLACK			:int		= 0x000000;
		public static const HEADER			:int		= 0x31212a;
		
	}

}