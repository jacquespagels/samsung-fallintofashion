package app 
{
	import app.display.screens.*;
	import app.models.DataModel;
	import app.models.products.DeviceModel;
	import app.models.QuizModel;
	import assets.screens.*;
	import com.greensock.*;
	import com.greensock.loading.*;
	import com.greensock.loading.display.*;
	import com.greensock.plugins.*;
	import com.demonsters.debugger.MonsterDebugger;
	import core.models.XMLModel;
	import core.net.Loader;
	import Document;
	import flash.display.BlendMode;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.external.ExternalInterface;
	
	/**
	 * Loads in all core data. Application classes (app or dev) can subclass this to run the app, or test out other code
	 * @author Dave Stewart
	 */
	public class ApplicationCore
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// stage instances
				public var document				:Sprite;
				
			// loaders
				public var dataLoader			:Loader;
				
			// models
				public var dataModel			:DataModel;
				public var quizModel			:QuizModel;
				public var contentModel			:XMLModel;
				
			// variables
				protected var isDev				:Boolean;
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function ApplicationCore(document:Sprite) 
			{
				// properties
					this.document		= document;
					document.blendMode	= BlendMode.LAYER;
					
				// monsterdebugger
					MonsterDebugger.initialize(this);
					
				// isLive
					isDev				= ! ExternalInterface.available;
				
				// activate plugins
					TweenPlugin.activate([FrameLabelPlugin, ColorTransformPlugin, TintPlugin, RemoveTintPlugin, AutoAlphaPlugin]);
					
				// load data
					bootstrap();
			}
			
			private function bootstrap():void
			{
				// data loader
					dataLoader = new Loader('data loader');
					dataLoader.addEventListener(Event.COMPLETE, onBootstrapComplete);
				 
				// data
					dataLoader.addData('../data/data.html', 'data');
					dataLoader.addData('../data/main.html', 'main');
				 
				// load
					dataLoader.load();
			}

			
			protected function initialize():void 
			{
				// set up all models
					dataModel		= new DataModel(dataLoader.getData('data'));
					contentModel	= new XMLModel(dataLoader.getData('main'));
			}
			
			protected function build():void 
			{
				
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		

			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			/**
			 * Fire when all data has loaded
			 * @param	event
			 */
			public function onBootstrapComplete(event:Event):void
			{
				initialize();
				build();
			}
			
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}