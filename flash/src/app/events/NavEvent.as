package app.events 
{
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class NavEvent extends Event 
	{
		
		public static const NAVIGATE:String = 'NavEvent.NAVIGATE';
		
		public var href:String;
		
		public function NavEvent(href:String = '') 
		{
			this.href = href;
			super(NAVIGATE, true, false);
		} 
		
		public override function clone():Event 
		{ 
			return new NavEvent(href);
		} 
		
		public override function toString():String 
		{ 
			return formatToString("NavEvent", "type", "bubbles", "cancelable", "eventPhase"); 
		}
		
	}
	
}