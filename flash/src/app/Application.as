package app 
{
	import app.display.elements.ScreenStack;
	import app.display.screens.*;
	import app.events.NavEvent;
	import app.models.LookModel;
	import app.models.products.DeviceModel;
	import assets.screens.*;
	import com.greensock.*;
	import com.greensock.loading.*;
	import com.greensock.loading.display.*;
	import com.greensock.plugins.*;
	import core.models.XMLModel;
	import core.net.Loader;
	import Document;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.external.ExternalInterface;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	
	/**
	 * Main Application class
	 * @author Dave Stewart
	 */
	public class Application extends ApplicationCore
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				public static var instance		:Application;
			
			// stage instances
				public var screens				:ScreenStack;
				
			// screens
				protected var homeScreen		:HomeScreen;
				protected var introScreen		:IntroScreen;
				protected var quizScreen		:QuizScreen;
				protected var uploadScreen		:UploadScreen;
				protected var lookScreen		:LookScreen;
				protected var detailScreen		:DetailScreen;
				protected var looksScreen		:LooksScreen;
				protected var customizeScreen	:CustomizeScreen;
				
			// global asset loader
				public var loader				:Loader;
				
			// models
				public var lookModel			:LookModel;
				public var userDeviceModel		:DeviceModel;
				public var lookDeviceModel		:DeviceModel;
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function Application(document:Sprite) 
			{
				// static instance reference
					Application.instance = this;
					
				// hide document until loaded
					document.alpha = 0;
					
				// super
					super(document);
			}
			
			protected override function initialize():void 
			{
				// super
					super.initialize();
					
				// set up default models
					userDeviceModel		= DeviceModel.factory();
					lookDeviceModel		= DeviceModel.factory();
					
					trace(userDeviceModel);
				
				// loader
					loader = new Loader('asset loader');
					loader.addEventListener(Event.COMPLETE, onLoaderComplete);
					
				// keyboard navigation in development
					isDev && document.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			}
			
			override protected function build():void 
			{
				// elements
					screens				= new ScreenStack(document);
					
				// screens
					homeScreen			= buildScreen('home', HomeScreen, Home);
					introScreen			= buildScreen('intro', IntroScreen, Intro);
					quizScreen			= buildScreen('quiz', QuizScreen, Quiz);
					uploadScreen		= buildScreen('upload', UploadScreen, Upload);
					lookScreen			= buildScreen('look', LookScreen, Look);
					detailScreen		= buildScreen('detail', DetailScreen, Detail);
					customizeScreen		= buildScreen('customize', CustomizeScreen, Customize);
					looksScreen			= buildScreen('looks', LooksScreen, null);
					
				// events
					screens.addEventListener(NavEvent.NAVIGATE, onNavigate);
					
				// show the document
					TweenLite.to(document, 1, { autoAlpha:1, delay:0.5 } );
					
				// test, only if not live
					isDev && test();
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: development functions
		
			protected function test():void 
			{
				// defaults
					//quizModel.deviceId	= 's4-mini';
					//quizModel.lookId	= 'pretty-in-pink';
					
				// navigate
					//navigateTo('screen', 'look');
					//navigateTo('screen', 'upload');
					navigateTo('screen', 'customize');
			}
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			/**
			 * Main navigation function used to catch all bubbling events, to go to screens, urls, looks, etc
			 * @param	protocol
			 * @param	id
			 */
			public function navigateTo(protocol:String, id:String):void 
			{
				// debug
					trace('Navigating to: ' + protocol + ':' + id);
					
				// tracking
					if (ExternalInterface.available)
					{
						ExternalInterface.call('track', protocol, id);
					}
					
				// navigate!
					switch(protocol)
					{
						
						// -------------------------------------------------------------------------------
						// screen
						
							case 'screen':
								
								switch(id)
								{
									
									case 'home':
										quizScreen.reset();
										uploadScreen.reset();
									break;
									
									case 'look':
										navigateTo('look', null);
										return;
									break;
										
									case 'detail':
										userDeviceModel.setFromModel(lookDeviceModel);
										userDeviceModel.save();
									break;
									
									case 'customize':
										customizeScreen.update();
									break;
								}
							
								screens.showElement(id);
									
							break;
							
						// -------------------------------------------------------------------------------
						// action
						
							case 'action':

								switch(id)
								{
									case 'upload_complete':
										
										// update the quiz element
											quizScreen.reset();
											quizScreen.showDevice();
											
										// set the look id
											quizModel.lookId = uploadScreen.stylizer.look.id;
										
										// show the quiz screen
											screens.showElement('quiz');
									break;
									
									case 'quiz_complete':
										//quizModel.lookId = uploadScreen.stylizer.look.id;
										//quizModel.lookId;
										navigateTo('screen', 'look');
									break;
									
									case 'customize_complete':
									
										// show the user screen
											//userDeviceModel.save();
											//detailScreen.update();
										
										// check if device has updated, and if so, update the look device as well
											if (userDeviceModel.deviceId != lookDeviceModel.deviceId)
											{
												//lookDeviceModel.setType(userDeviceModel.deviceId);
												//lookDeviceModel.setFromLook(lookModel);
											}
											
										// show the detail screen
											screens.showElement('detail');
									break;
								}
								
							break;
						
						// -------------------------------------------------------------------------------
						// look
						
							case 'look':
						
								// -------------------------------------------------------------------------------
								// defaults
								
									// grab quiz lookId if null
										if (id == null)
										{
											id = quizModel.lookId || 'pretty-in-pink';
										}
										
									// first time we go to the look page, we need to set everything up
										if ( ! lookModel )
										{
											// debug
												//trace('CREATING LOOK MODEL');
												
											// create the global look model for the first time
												lookModel = LookModel.factory(id);
												
											// set the lookDeviceModel type to the result of the quiz
												lookDeviceModel.setType(quizModel.deviceId || 'note3');
										}
										
									// debug
										//trace('NEW LOOK ID: ' + id);
										
								// -------------------------------------------------------------------------------
								// update the look and device
										
									// set the new look type
										lookModel.setLook(id);
										
									// update this
										quizModel.lookId = id;
										
									// update the device model
										lookDeviceModel.setFromLook(lookModel);
										
								// -------------------------------------------------------------------------------
								// preload all images, and navigate when done
										
									// preload look URLs, then show screen on completion
										var urls		:Array		= lookModel.urls.concat([lookDeviceModel.front, lookDeviceModel.back, lookDeviceModel.cover.front])
										var loader		:Loader		= new Loader('look loader').addMany(urls);
										loader.addEventListener(Event.COMPLETE, function():void
										{
											// show screens
												//trace('showing look: ' + id)
												screens.showElement('look');
												lookScreen.update();
											
											// also update user device model
												userDeviceModel.setFromModel(lookDeviceModel);
												userDeviceModel.save();
										} );
										loader.load();
										
									// debug
										//trace('\nabout to show the look: ' + id)
										//trace('look: ' + lookModel);
										//trace('look device: ' + lookDeviceModel + '\n');

							break;
							
						// -------------------------------------------------------------------------------
						// links
						
							case 'http':
							case 'https':
								navigateToURL(new URLRequest(protocol + ':' + id));
							break;
							
							case '#':
								// ignore
							break;
							
							case 'link':

							
							break;
							
							
						// -------------------------------------------------------------------------------
						// other
						
							default:
					}
			}
			
			public function load(urls:Array, onComplete:Function):void
			{
				for each(var url:String in urls)
				{
					dataLoader.add(url);
				}
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			/**
			 * Screen is the base View class for each main application area
			 * @param	id
			 * @param	screenClass
			 * @param	templateClass
			 * @return
			 */
			protected function buildScreen(id:String, screenClass:Class, templateClass:Class):*
			{
				var data		:XML			= contentModel.getNode('#' + id);
				var model		:XMLModel		= new XMLModel(data);
				var template	:*				= templateClass ? new templateClass() : null;
				var screen		:DisplayObject	= new screenClass(screens, template, model);
				return screen;
			}
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			/**
			 * Top-level event handler for nested navigation buttons
			 * @param	event
			 */
			protected function onNavigate(event:NavEvent):void 
			{
				var matches:Array = event.href.match(/^(\w+|#):?(.+)/);
				if (matches)
				{
					// variables
						var protocol	:String	= matches[1];
						var id			:String	= matches[2];
						
					// navigate
						navigateTo(protocol, id);
				}
				
			}
			
			protected function onLoaderComplete(event:Event):void 
			{
				
			}
			
			/**
			 * Debugging
			 * @param	event
			 */
			public function onKeyDown(event:KeyboardEvent):void
			{
				var index:int = event.keyCode - 49;
				if (index >= 0 && index <= 8)
				{
					screens.showElement(index);
				}
			}
			
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}