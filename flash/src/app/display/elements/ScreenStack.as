package app.display.elements 
{
	import com.greensock.TweenLite;
	import core.display.layout.Stack;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class ScreenStack extends Stack 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function ScreenStack(parent:DisplayObjectContainer=null) 
			{
				super(parent);
			}
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected override function _showElement(element:DisplayObject):void 
			{
				element.alpha = 0;
				_addChild(element);
				TweenLite.to(element, 0.2, { autoAlpha:1 } );
			}
			
			protected override function _hideElement(element:DisplayObject):void 
			{
				TweenLite.to(element, 0.2, { autoAlpha:1 } );
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}