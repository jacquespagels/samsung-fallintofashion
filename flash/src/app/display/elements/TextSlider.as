package app.display.elements 
{
	import app.models.vo.TipModel;
	import app.display.elements.TextPanel;
	import core.display.elements.Element;
	import core.display.layout.HBox;
	import core.utils.LayoutUtils;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class TextSlider extends Element 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// stage instances
				protected var panel			:TextPanel
				protected var nav			:HBox;
			
			// properties
				protected var _data			:Array;
				protected var _index		:int;
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function TextSlider() 
			{
				super();
				clear();
			}
			
			override protected function initialize():void 
			{
				_index	= 0;
				_data	= [];
			}
			
			override protected function build():void 
			{
				// button
					useHandCursor	= true;
					buttonMode		= true;
					mouseChildren	= false;
					
				// panel
					panel			= new TextPanel('', false, 30);
					nav				= new HBox(this, 10);
					
				// create buttons
					addChild(panel);
					addChild(nav);
				
				// add event listener
					addEventListener(MouseEvent.CLICK, onClick);
				
			}
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			override public function clear():void
			{
				initialize();
				panel.text = '';
				nav.clear();
			}
			
			public function addText(text:String):void 
			{
				_data.push(text);
				nav.addChild(new SliderButton());
				if (_data.length == 1)
				{
					index = 0;
				}
				draw();
				invalidate();
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			override public function set width(value:Number):void
			{
				panel.width = value;
			}
		
			public function get index():int { return _index; }
			public function set index(value:int):void 
			{
				if (index >= 0 && index <= _data.length)
				{
					_index = value;
					draw();
				}
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			override protected function draw():void 
			{
				// text
					panel.text	= _data[index];

				// nav
					LayoutUtils.grab(panel, nav)
						.align('center')
						.align('bottom', -20);
						
				// nav
					for (var i:int = 0; i < nav.numChildren; i++) 
					{
						var child:DisplayObject = nav.getChildAt(i);
						child.alpha = i == _index ? 1 : 0.5;
					}
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onClick(event:MouseEvent):void 
			{
				_index++;
				if (_index == _data.length)
				{
					_index = 0;
				}
				index = _index;
				dispatchEvent(new Event(Event.CHANGE));
			}
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}
import flash.display.Sprite;

class SliderButton extends Sprite
{
	public function SliderButton()
	{
		graphics.beginFill(0xFFFFFF, 1);
		graphics.drawCircle(6, 6, 6);
	}
	
}

