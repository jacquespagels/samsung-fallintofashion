package app.display.elements 
{
	import core.display.elements.Element;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	
	import assets.bitmaps.BackgroundBlurred;
	import assets.bitmaps.BackgroundHome;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Background extends Element 
	{
		protected var image1		:Bitmap;
		protected var image2		:Bitmap;
		
		public function Background(parent:DisplayObjectContainer) 
		{
			// parent
				super(parent);
				
			// images
				image1 = new Bitmap(new assets.bitmaps.BackgroundHome());
				image2 = new Bitmap(new assets.bitmaps.BackgroundBlurred());
				addChild(image2);
				addChild(image1);
				
			// border
				graphics.lineStyle(1, 0xFFFFFF);
				graphics.drawRect(10, 10, image1.width - 20, image1.height - 20);
		}
		
		public function set blurred(state:Boolean):void
		{
			image1.visible = ! state;
		}
		
	}

}