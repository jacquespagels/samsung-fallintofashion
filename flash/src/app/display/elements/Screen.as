package app.display.elements 
{
	import app.display.controls.NavButton;
	import app.display.elements.Background;
	import app.display.elements.Heading;
	import app.display.screens.HomeScreen;
	import app.events.NavEvent;
	import app.utils.Template;
	import assets.placeholders.ButtonPlaceholder;
	import assets.screens.*;
	import core.display.elements.Element;
	import core.models.XMLModel;
	import core.utils.LayoutUtils;
	import core.utils.TextUtils;
	import core.utils.XMLUtils;
	import flash.display.BlendMode;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Screen extends Element 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// instances
				public var background		:Background;
				public var heading			:Heading;
				public var content			:Element;
				
			// properties	
				protected var layout		:DisplayObjectContainer;
				protected var template		:Template;
				
			// models
				public var contentModel		:XMLModel;
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			/**
			 * Combines template asset and XML/model data to create individual screens.
			 * Also creates any components on each screen that need to be manipulated
			 * 
			 * @param	parent			The parent clip
			 * @param	template		The class of the assets.templates.Screen to pass in as the content
			 * @param	data			The correct nodes that relate to this screen
			 */
			public function Screen(parent:DisplayObjectContainer, layout:DisplayObjectContainer, model:XMLModel) 
			{
				this.layout			= layout;
				this.contentModel	= model;
				this.name			= model.xml.@id;
				super(parent);
			}

			override protected function initialize():void
			{
				blendMode = BlendMode.LAYER;
			}
			
			/**
			 * Builds each screen by populating the SWF asset with HTML data
			 * 
			 * Loops over the children in the movieclip asset, then attempts to 
			 * find same-named nodes in the HTML data. 
			 * 
			 * If a node is found, it is passed to the correct add() method, and 
			 * which populates / updates the content clip.
			 * 
			 */
			override protected function build():void 
			{
				// add backgroud
					background = new Background(this);
					background.blurred = ! (this is HomeScreen)
				
				// add template
					if (layout)
					{
						content		= new Element(this);
						template	= new Template(layout, content, contentModel.xml);
						
						//template.setRuleHandler('', set
						
						template.setClassHandler(ButtonPlaceholder, addButton);
					}
				
				// set heading
					heading = new Heading(this, contentModel.getItem('title'));
					
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			/**
			 * Create, position and style the button
			 * @param	child
			 * @param	nodes
			 */
			protected function addButton(placeholder:DisplayObject, node:XML):DisplayObject 
			{
				var element:NavButton = new NavButton(node);
				element.addEventListener(MouseEvent.CLICK, onButtonClick);
				return content.addChild(element);
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		

			protected function onButtonClick(event:MouseEvent):void 
			{
				var button:NavButton = event.currentTarget as NavButton;
				button.dispatchEvent(new NavEvent(button.href));
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}
	

}

