package app.display.elements 
{
	import core.display.elements.Element;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import assets.elements.Heading;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Heading extends Element 
	{
		protected var heading:assets.elements.Heading;
		
		public function Heading(parent:DisplayObjectContainer, text:String) 
		{
			// super
				super(parent);
				
			// create heading
				heading = new assets.elements.Heading();
				addChild(heading);
				heading.tf.autoSize = 'left';
				setText(text);
		}
		
		public function setText(text:String):void
		{
			heading.tf.text  = text;
			heading.bg.width = heading.tf.x + heading.tf.textWidth + 20;
		}
		
	}

}