package app.display.elements 
{
	import assets.elements.TextPanel;
	
	/**
	 * The logic for the TextPanel asset
	 * @author Dave Stewart
	 */
	public class TextPanel extends assets.elements.TextPanel 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				protected var _text				:String;
				protected var _fixed			:Boolean;
				protected var _bottomPadding	:Number;
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function TextPanel(text:String = 'Example text', fixed:Boolean = false, bottomPadding:Number = 0) 
			{
				_text			= text;
				_fixed			= fixed;
				_bottomPadding	= bottomPadding;
				build();
				draw();
			}
		
			protected function build():void 
			{
				tf.autoSize = 'left';
				//tf.border = true;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get text():String { return _text; }
			public function set text(value:String):void 
			{
				_text 	= String(value)
							.replace(/&amp;/g, "&")
							.replace(/&rsquo;/g, "'");
				tf.text	= _text;
				draw();
			}
		
			override public function get width():Number { return super.width; }
			override public function set width(value:Number):void 
			{
				tf.width = value - (tf.x * 2);
				draw();
			}
			
			override public function get height():Number 
			{
				return bg.height;
			}
			
			override public function set height(value:Number):void 
			{
				bg.height = value;
			}
			
			public function get fixed():Boolean { return _fixed; }
			public function set fixed(value:Boolean):void 
			{
				_fixed = value;
				draw();
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function draw():void 
			{
				bg.width = tf.width + (tf.x * 2);
				if ( ! _fixed )
				{
					bg.height	= tf.height + (tf.y * 2) + _bottomPadding;
				}
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}