package app.display.preloaders 
{
	import com.greensock.plugins.AutoAlphaPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.TweenLite;
	import core.display.elements.Element;
	import core.utils.LayoutUtils;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class SitePreloader extends Element 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// instances
				protected var progress		:ProgressBar;
			
			// properties
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function SitePreloader(parent:DisplayObjectContainer=null) 
			{
				super(parent);
			}
		
			override protected function initialize():void 
			{
				TweenPlugin.activate([AutoAlphaPlugin]);
			}
			
			override protected function build():void 
			{
				// background
					graphics.beginFill(0xBABABA, 0.4);
					graphics.drawRect(0, 0, 728, 728);
					
				// container
					progress = new ProgressBar(this);
					
				// align container
					LayoutUtils.grab(this, progress).align('center');
					
				// update
					update(0);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function update(ratio:Number):void
			{
				progress.update(ratio);
			}
			
			public override function show():void 
			{
				TweenLite.to(this, 0.5, { autoAlpha:1 } );
			}
			
			public override function hide():void 
			{
				TweenLite.to(this, 0.5, { autoAlpha:0 } );
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}