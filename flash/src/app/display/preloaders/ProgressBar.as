package app.display.preloaders 
{
	import com.greensock.plugins.AutoAlphaPlugin;
	import com.greensock.plugins.TweenPlugin;
	import com.greensock.TweenLite;
	import core.display.elements.Element;
	import core.utils.LayoutUtils;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class ProgressBar extends Element 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// instances
				protected var bg			:Shape;
				protected var container		:Sprite;
				protected var border		:Shape;
				protected var bar			:Shape;
				protected var barMask		:Shape;
				
			
			// properties
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function ProgressBar(parent:DisplayObjectContainer=null) 
			{
				super(parent);
				update(0.5);
			}
		
			override protected function build():void 
			{
				// border
					border = new Shape();
					border.graphics.lineStyle(1, 0xFFFFFF, 1, true)
					border.graphics.drawRect(-1, -1, 252, 22);
					addChild(border);
					
				// bar mask
					barMask = new Shape();
					barMask.graphics.beginFill(0xFF0000);
					barMask.graphics.drawRect(0, 0, 250, 20);
					addChild(barMask);
					
				// bar
					bar = new Shape();
					bar.graphics.beginFill(0xCA6D8C, 0.5);
					bar.graphics.drawRect(0, 0, 250, 20);
					bar.mask = barMask;
					addChild(bar);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function update(ratio:Number):void
			{
				bar.x = - bar.width + (bar.width * ratio);
			}
			
			public override function show():void 
			{
				TweenLite.to(this, 0.3, { autoAlpha:1 } );
			}
			
			public override function hide():void 
			{
				TweenLite.to(this, 0.5, { autoAlpha:0 } );
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			override public function get width():Number { return barMask.width; }
			override public function set width(value:Number):void 
			{
				
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}