package app.display.screens 
{
	import app.display.controls.NavButton;
	import app.display.widgets.stylizer.Stylizer;
	import app.events.NavEvent;
	import core.display.elements.Image;
	import core.models.XMLModel;
	import app.display.elements.Screen;
	import dev.colors.Swatch;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class UploadScreen extends Screen 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				public var next			:NavButton;
				public var stylizer		:Stylizer;
			
			// properties
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function UploadScreen(parent:DisplayObjectContainer, layout:DisplayObjectContainer, model:XMLModel) 
			{
				super(parent, layout, model);
			}
			
			override protected function initialize():void 
			{
				super.initialize();
			}
		
			override protected function build():void 
			{
				// super
					super.build();
					
				// button
					next		= content.getChildByName('next') as NavButton;	
					next.addEventListener(MouseEvent.CLICK, onButtonClick);
					
				// image
					stylizer	=  template.process('image', new Stylizer(this));
					stylizer.addEventListener(Event.COMPLETE, onStylizerComplete)
					
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			
			public function reset():void 
			{
				stylizer.reset();
			}

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function getLookId():String
			{
				return '';
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			protected function onStylizerComplete(event:Event):void 
			{
				addChild(new Swatch(stylizer.color))
			}

			override protected function onButtonClick(event:MouseEvent):void 
			{
				if (stylizer.color)
				{
					dispatchEvent(new NavEvent('action:upload_complete'))
				}
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}