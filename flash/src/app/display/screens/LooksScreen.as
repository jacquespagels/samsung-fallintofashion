package app.display.screens 
{
	import app.display.controls.LookButton;
	import app.display.controls.NavButton;
	import app.display.elements.Screen;
	import app.events.NavEvent;
	import app.models.LookModel;
	import core.display.layout.HBox;
	import core.display.layout.VBox;
	import core.models.XMLModel;
	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class LooksScreen extends Screen 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				public var box			:HBox;
				public var leftBox		:VBox;
				public var rightBox		:VBox;
			
			// properties
				protected var lookModel	:LookModel;
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function LooksScreen(parent:DisplayObjectContainer, layout:DisplayObjectContainer, model:XMLModel) 
			{
				super(parent, layout, model);
			}

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			protected override function build():void 
			{
				//super
					super.build();
					
				// hbox
					box					= new HBox(this, 20)
					box.x				= 20;
					box.y				= 80;
					
				// vbox
					leftBox				= new VBox(box, 20);
					rightBox			= new VBox(box, 20);
					
				// variables	
					var looks		:Vector.<LookModel> = LookModel.all();
					var button		:LookButton;
					var bitmap		:Bitmap;
					var xml			:XML;
					
				// create buttons
					var i:int = 0;
					for each(var look:LookModel in looks)
					{
						xml				= <a id={look.id} href={'look:' + look.id}>{look.name}</a>
						button			= new LookButton(xml);
						button.width	= 300;
						button.addEventListener(MouseEvent.CLICK, onButtonClick);
						(i < 5 ? leftBox : rightBox).addChild(button);
						i++;
					}
					
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			override protected function onButtonClick(event:MouseEvent):void 
			{
				var button:LookButton = event.currentTarget as LookButton;
				button.dispatchEvent(new NavEvent(button.href));
			}

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}