package app.display.screens 
{
	import app.Application;
	import app.models.DataModel;
	import assets.elements.Progress;
	import core.models.XMLModel;
	import app.models.QuizModel;
	import app.display.elements.Screen;
	import app.display.widgets.quiz.QuizWidget;
	import app.events.NavEvent;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class QuizScreen extends Screen 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				public var quizWidget			:QuizWidget;
				public var progress				:Progress;
			
			// properties
			
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function QuizScreen(parent:DisplayObjectContainer, layout:DisplayObjectContainer, model:XMLModel) 
			{
				super(parent, layout, model);
			}
			
			protected override function build():void 
			{
				// super
					super.build();
					
				// buttons
					content.addEventListener(NavEvent.NAVIGATE, onNavigate);
					
				// progress
					progress		= new Progress();
					progress.x		= 627;
					progress.y		= 41;
					addChild(progress);
					
				// quiz widget
					quizWidget		= template.process('quiz', new QuizWidget(this));
					quizWidget.addEventListener(Event.CHANGE, onQuizWidgetChange);
					
				// start the quiz
					quizWidget.start();
			}
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function showDevice():void
			{
				quizWidget.start('#device');
				progress.gotoAndStop(4);
			}
		
			public function reset():void 
			{
				quizWidget.reset();
				progress.gotoAndStop(1);
			}
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			protected function onQuizWidgetChange(event:Event):void 
			{
				progress.nextFrame();
			}
			
			protected function onNavigate(event:NavEvent):void 
			{
				if (quizWidget.next())
				{
					progress.nextFrame();
				}
				if (quizWidget.model.deviceId)
				{
					dispatchEvent(new NavEvent('action:quiz_complete'))
				}
			}
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}