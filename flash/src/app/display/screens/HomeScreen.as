package app.display.screens 
{
	import core.models.XMLModel;
	import app.display.elements.Screen;
	import flash.display.DisplayObjectContainer;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class HomeScreen extends Screen 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				
			
			// properties
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function HomeScreen(parent:DisplayObjectContainer, layout:DisplayObjectContainer, model:XMLModel) 
			{
				super(parent, layout, model);
			}
			
			override protected function build():void
			{
				super.build();
				heading.visible = false;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}