package app.display.screens 
{
	import adobe.utils.ProductManager;
	import app.Application;
	import app.display.widgets.customize.ColorChooser;
	import app.display.widgets.customize.CoverChooser;
	import app.display.widgets.customize.DeviceChooser;
	import app.display.widgets.customize.DeviceColorChooser;
	import app.events.NavEvent;
	import app.models.products.DeviceModel;
	import assets.screens.Customize;
	import core.display.layout.Stack;
	import core.models.XMLModel;
	import app.display.elements.Screen;
	import core.net.Loader;
	import flash.display.DisplayObjectContainer;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.setTimeout;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class CustomizeScreen extends Screen 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				public var progress				:MovieClip;
				public var customize			:Customize;
				public var stack				:Stack;
				public var deviceChooser		:DeviceChooser;
				public var colorChooser			:ColorChooser;
				public var coverChooser			:ColorChooser;
				
			
			// properties
				public var deviceModel			:DeviceModel;
				
			// progression
				protected var index				:int;
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function CustomizeScreen(parent:DisplayObjectContainer, layout:Customize, contentModel:XMLModel) 
			{
				customize = layout;
				super(parent, layout, contentModel);
				showSection(index);
			}
			
			override protected function initialize():void 
			{
				// super
					super.initialize();
					
				// new
					deviceModel		= Application.instance.userDeviceModel;
					
				//
					index = 0;
			}

			override protected function build():void 
			{
				// super
					super.build();
					
				// add tabs
					//addObject(
					
				// add progress indicator
					progress		= customize.progress;
				
				// add stack
					stack			= new Stack(this);
				
				// choosers
					deviceChooser	= new DeviceChooser(stack, contentModel, deviceModel);
					colorChooser	= new DeviceColorChooser(stack, contentModel, deviceModel);
					coverChooser	= new CoverChooser(stack, contentModel, deviceModel);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
		
			public function update():void 
			{
				showSection(0, true);
			}
			
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function showSection(index:int, update:Boolean = true):void 
			{
				// debug
					if (update)
					{
						trace('update:' + update);
						trace(deviceModel);
					}
					
				// navigate
					switch (index) 
					{
						// device type chooser
							case 0:
								heading.setText(contentModel.getNode('#device ~subtitle'));
								if (update)
								{
									deviceChooser.update();
								}
							break;
							
						// device color chooser
							case 1:
								heading.setText(contentModel.getNode('#color ~subtitle'));
								if (update)
								{
									colorChooser.update();
								}
							break;
							
						// case type and color chooser
							case 2:
								heading.setText(contentModel.getNode('#cover ~subtitle'));
								if (update)
								{
									coverChooser.update();
								}
							break;
							
						// end
							case 3:
								trace('SAVING DEVICE MODEL: ' + deviceModel);
								deviceModel.save();
								
						// start or end
							default:
								heading.setText(contentModel.getNode('~subtitle'));
								index = 0;
								dispatchEvent(new NavEvent('action:customize_complete'));
								setTimeout(reset, 500);
								return;
					}
				
				// show correct stack element
					progress.gotoAndStop(index + 1);
					stack.showElement(index);
			}
			
			protected function reset():void 
			{
				index = 0;
				stack.showElement(index);
			}
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected override function onButtonClick(event:MouseEvent):void 
			{
				// variables
					var dir:int = event.currentTarget.name == 'next' ? 1 : -1;
					index += dir;
					
				// do it
					showSection(index, dir > 0);

			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}