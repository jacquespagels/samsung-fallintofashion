package app.display.screens 
{
	import app.Application;
	import app.display.controls.NavButton;
	import app.events.NavEvent;
	import core.models.XMLModel;
	import app.models.products.DeviceModel;
	import app.display.elements.Screen;
	import app.display.widgets.preview.PreviewWidget;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class DetailScreen extends Screen 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				protected var preview	:PreviewWidget;
				protected var details	:TextField;
				
			
			// properties
				protected var deviceModel:DeviceModel;
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function DetailScreen(parent:DisplayObjectContainer, layout:DisplayObjectContainer, model:XMLModel) 
			{
				super(parent, layout, model);
			}

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			protected override function initialize():void 
			{
				// super
					super.initialize();
					
				// model	
					deviceModel = Application.instance.userDeviceModel;
					deviceModel.addEventListener(Event.CHANGE, onDeviceModelUpdate)
			}
			
			protected override function build():void 
			{
				// super
					super.build();
				
				// elements
					details			= content.getChildByName('details') as TextField;
					
				// add content
					preview			= template.process('preview', new PreviewWidget(this, deviceModel));
					preview.width	= 440;
			}
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		

		
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			protected function onDeviceModelUpdate(event:Event):void 
			{
				trace('LOADING:' + event.target)
				preview.update(deviceModel);
				details.text = deviceModel.getText(contentModel.getItem('details'));
			}
		
			override protected function onButtonClick(event:MouseEvent):void 
			{
				// href
					var href:String = (event.currentTarget as NavButton).href;
					
				// intercept buy link
					if (href.indexOf('link:') > -1)
					{
						var item	:String	= href.replace('link:', '');
						var link	:String	= contentModel.getItem(item);
						href				= deviceModel.getText(link);
						dispatchEvent(new NavEvent(href));
					}
					else 
					{
						super.onButtonClick(event);
					}
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}