package app.display.screens 
{
	import app.Application;
	import app.events.NavEvent;
	import app.models.products.DeviceModel;
	import assets.placeholders.ButtonPlaceholder;
	import core.net.Loader;
	import app.models.DataModel;
	import core.models.XMLModel;
	import app.models.LookModel;
	import app.models.products.DeviceModel;
	import assets.screens.Look;
	import core.display.layout.Stack;
	import app.display.elements.Screen;
	import app.display.elements.TextPanel;
	import app.display.widgets.panels.TipsPanel;
	import app.display.widgets.preview.PreviewWidget;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.DataEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import core.utils.LayoutUtils;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class LookScreen extends Screen 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				protected var subtitle		:TextField;
				protected var preview		:PreviewWidget;
				protected var details		:TextPanel;
				protected var explore		:DisplayObject;
				protected var restart		:DisplayObject;
				
			// elements
				protected var tips			:TipsPanel;
			
			// properties
				protected var lookModel		:LookModel;
				protected var deviceModel	:DeviceModel;
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function LookScreen(parent:DisplayObjectContainer, layout:DisplayObjectContainer, model:XMLModel) 
			{
				super(parent, layout, model);
			}
			
			protected override function initialize():void 
			{
				super.initialize();
				deviceModel		= Application.instance.lookDeviceModel;
				lookModel		= Application.instance.lookModel;
			}
			
			protected override function build():void 
			{
				// super
					super.build();
					
				// existing elements
					subtitle		= content.getChildByName('subtitle') as TextField;
					explore			= content.getChildByName('explore') as DisplayObject;
					restart			= content.getChildByName('restart') as DisplayObject;
					
				// restart scaling
					restart.height	= 27;
					restart.scaleX	= restart.scaleY;
					restart.x		= 728 - restart.width - 10 - 29 - 15;
					
				// add sharing link
					template.source.getChildByName('btnFacebook').addEventListener(MouseEvent.CLICK, onShareButtonClick);
					
				// preview
					preview			= template.process('preview', new PreviewWidget(this, deviceModel));
					preview.width	= explore.width;
					
				// detail
					details			= template.process('details', new TextPanel('Detail panel', true));
					
				// tips
					tips			= new TipsPanel(this, (layout as Look).tips);
					
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			
			public function update():void 
			{
				// debug
					//trace('SETTING LOOK')
					
				// do it
					subtitle.text	= Application.instance.lookModel.name;
					details.text	= deviceModel.getText(contentModel.getItem('details'));
					preview.update(Application.instance.lookDeviceModel)
					tips.update();
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			protected function onShareButtonClick(event:MouseEvent):void 
			{
				var href:String = contentModel.getItem('link_share');
				href = deviceModel.getText(href);
				dispatchEvent(new NavEvent(href));
			}
			
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}