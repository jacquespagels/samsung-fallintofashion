package app.display.controls 
{
	import assets.ui.NavButton;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.text.TextField;
	import core.utils.XMLUtils;

	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class NavButton extends assets.ui.NavButton 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
			
			
			// properties
				protected var data		:XML;
			
				
			// variables
				protected var padding	:int;
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function NavButton(node:XML)
			{
				// properties
					data = node;
					
				// draw
					draw();
			}
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
		
			public function get node():XML
			{
				return data;
			}
			
			public function get href():String
			{
				return data.attribute('href');
			}
		
			override public function set width(value:Number):void
			{
				bg.width = value * 1 / scaleX;
				arrow.x = bg.width - arrow.width - padding;
			}
			
			public function set text(value:String):void
			{
				data.* = value;
				draw();
			}

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function draw():void
			{
				// graphics
					arrow.graphic.rotation	= XMLUtils.hasClass(data,'back')
												? -180
												: XMLUtils.hasClass(data, 'up')
													? -90
													: 0;
					arrow.mouseEnabled		= false;
					arrow.mouseChildren		= false;
					tf.text					= data ? data.toString() : 'Unnamed button';
					tf.autoSize				= 'left';
					tf.mouseEnabled			= false;
					
				// variables
					padding	= 20;
					var gap			:int	= 10;
				
				// button has arrow on left
					if (arrow.graphic.rotation == -180)
					{
						arrow.x		= padding;
						tf.x		= arrow.x + arrow.width + gap;
						bg.width	= tf.getBounds(this).right + padding;
					}
					
				// button has arrow on right
					else
					{
						arrow.x		= tf.getBounds(this).right + gap;
						bg.width	= arrow.getBounds(this).right + padding;
					}
			}
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			public override function toString():String 
			{
				return '[object NavButton href="' +href+ '"]';
			}
	}

}