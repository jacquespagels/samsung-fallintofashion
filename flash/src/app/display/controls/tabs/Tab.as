package app.display.controls.tabs 
{
	import core.display.elements.Element;
	import flash.display.DisplayObjectContainer;
	import assets.ui.Tab ;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Tab extends assets.ui.Tab 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				protected var _state	:String;
			
			
			// variables
				protected var colors	:Object;
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function Tab(parent:DisplayObjectContainer, text:String) 
			{
				tf.text = text;
				parent.addChild(this);
				initialize();
			}
		
			protected function initialize():void 
			{
				// colors
					colors =
					{
						'up'	:0x322129,
						'over'	:0xFFFFFF,
						'down'	:0xFFFFFF
					}
				
				// set button interactivity
					buttonMode		= true;
					mouseChildren	= false;
					
				// events
					addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
					addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
					addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
					
				// elements
					tf.autoSize		= 'left';
					bg.width		= tf.x + tf.width + 20;
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get state():String { return _state; }
			public function set state(value:String):void 
			{
				_state = value;
				draw();
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function draw():void 
			{
				// colors
					tf.textColor = colors[state];
				
				// background
					bg.gotoAndStop(state);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onMouseOver(event:MouseEvent):void 
			{
				if ( state !== 'down' )
				{
					state = 'over';
				}
			}
			
			protected function onMouseOut(event:MouseEvent):void 
			{
				if ( state !== 'down' )
				{
					state = 'up';
				}
			}
		
			protected function onMouseDown(event:MouseEvent):void 
			{
				if ( state !== 'down' )
				{
					dispatchEvent(new Event(Event.COMPLETE, true));
					state = 'down';
				}
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}