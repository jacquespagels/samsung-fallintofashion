package app.display.controls.tabs 
{
	import assets.ui.TabBackground;
	import core.display.elements.Element;
	import core.display.layout.HBox;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class TabGroup extends Element 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// instances
				protected var bg		:TabBackground;
				protected var hbox		:HBox;
				
			
			// properties
				protected var data		:Array;
				protected var tabs		:Vector.<Tab>;
				protected var tab		:Tab;
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function TabGroup(parent:DisplayObjectContainer) 
			{
				super(parent);
			}
		
			override protected function initialize():void 
			{
				data	= [];
				tabs	= new Vector.<Tab>;
			}
			
			override protected function build():void 
			{
				// background
					bg		= new TabBackground();
					bg.y	= 53;
					addChild(bg);
					
				// tab container
					hbox	= new HBox(this, 8);
					hbox.addEventListener(Event.COMPLETE, onTabActivate);
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function update(data:Array):void
			{
				// clear everything
					clear();
					tabs.length = 0;
					
				// update
					for each(var datum:Object in data)
					{
						addTab(datum);
					}
			}
			
			public function addTab(datum:Object):void 
			{
				// add tab
					var tab:Tab = new Tab(hbox, datum.name);
					
				// save tab reference if  no tabs
					if (data.length == 0)
					{
						this.tab = tab;
					}
					
				// if first tab, activate
					tab.state = data.length == 0 ? 'down' : 'up';
					
				// add data
					tabs.push(tab);
					data.push(datum);
			}
		
			public function activateTab(type:String):void
			{
				// set last tab up
					if (tab)
					{
						tab.state = 'up';
					}
					
				// find new tab and set down
					for (var i:int = 0; i < data.length; i++) 
					{
						if (data[i].type == type)
						{
							tabs[i].state = 'down';
							tab = tabs[i];
							break;
						}
					}
			}
		
			public override function clear():void 
			{
				data = [];
				hbox.clear();
			}
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get index():int
			{
				return hbox.getChildIndex(tab);
			}
			
			public function get id():String
			{
				return data[index].id;
			}
		
			public function get type():String
			{
				return data[index].type;
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onTabActivate(event:Event):void 
			{
				// update tabs
					if (tab)
					{
						tab.state = 'up';
					}
					tab = event.target as Tab;

				// manage events
					event.stopImmediatePropagation();
					dispatchEvent(new Event(Event.CHANGE));
			}
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}