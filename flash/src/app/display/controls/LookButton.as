package app.display.controls 
{
	import assets.ui.LookButton;
	import assets.ui.NavButton;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import core.utils.XMLUtils;

	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class LookButton extends assets.ui.LookButton
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
			
			
			// properties
				protected var data		:XML;
			
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function LookButton(node:XML)
			{
				// properties
					data = node;
					
				// draw
					build();
			}
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
		
			public function get node():XML
			{
				return data;
			}
			
			public function get href():String
			{
				return data.attribute('href');
			}
		
			override public function set width(value:Number):void
			{
				
			}
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function build():void
			{
				// behaviour
					buttonMode = true;
					useHandCursor = true;
					mouseChildren = false;
				
				// elements
					tf.text = data;
					bg.gotoAndStop(data.@id);
					
				// interactivity
					addEventListener(MouseEvent.ROLL_OVER, onRollOver);
					addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			}
			
			protected function onRollOver(event:MouseEvent):void 
			{
				alpha = 0.8;
			}
			
			protected function onRollOut(event:MouseEvent):void 
			{
				alpha = 1;
			}
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			public override function toString():String 
			{
				return '[object LookButton href="' +href+ '"]';
			}
	}

}