package app.display.widgets.quiz 
{
	import app.models.QuizModel;
	import assets.widgets.QuizWidget;
	import core.models.XMLModel;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class QuizWidget extends assets.widgets.QuizWidget
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// stage intances
				protected var placeholders		:Array;
				protected var options			:Vector.<QuizOption>;
				protected var option			:QuizOption;
				protected var question:XMLModel;
				
			// properties
				public var model				:QuizModel;
				
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function QuizWidget(parent:DisplayObjectContainer) 
			{
				parent.addChild(this);
				initialize();
				build();
			}
		
			protected function initialize():void 
			{
				// set up the model
					model = new QuizModel();
					
			}
		
			/**
			 * Creates the initial option elements
			 */
			protected function build():void 
			{
				// variables
					placeholders	= [option0, option1, option2];
					options			= new Vector.<QuizOption>;
					
				// build
					for each (var placeholder:DisplayObject in placeholders)
					{
						// element
							option			= new QuizOption(options.length);
							option.x		= placeholder.x;
							option.y		= placeholder.y;
							option.addEventListener(MouseEvent.CLICK, onOptionClick);
						
						// update everything
							options.push(option);
							addChild(option);
							removeChild(placeholder);
					}
			}
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function start(questionId:String = '#q1'):void
			{
				resetOptions();
				question = model.getQuestion(questionId);
				draw();
			}
			
			public function next():Boolean
			{
				if (model.hasQuestion())
				{
					question = model.getQuestion();
					draw();
					return true;
				}
				return false;
			}
			
			public function reset():void 
			{
				model.reset();
				start();
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function draw():void 
			{
				// variables
					var answers		:XMLList;
					var answer		:XML;
				
				// update the prompt
					tf.text = question.getItem('question');
					
				// reset options
					resetOptions();
				
				// populate options
					answers = question.getItems('answer');
					for (var i:int = 0; i < options.length; i++) 
					{
						// variables
							answer	= answers[i];
							option	= options[i];
							
						// set up option
							option.visible = !! answer;
							if (answer)
							{
								option.id		= answer.a.@href; 
								option.text		= answer.a[0] 
							}
					}
			}
		
			protected function resetOptions(option:QuizOption = null):void
			{
				for (var i:int = 0; i < options.length; i++) 
				{
					if (options[i] == option)
					{
						continue;
					}
					options[i].state = false;
				}
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onOptionClick(event:MouseEvent):void 
			{
				// get the option as an Option
					option			= event.currentTarget as QuizOption;

				// store the answer for this question
					model.setAnswer(option.index);
					
				// reset other options
					option.state = true;
					resetOptions(option);
			}
			
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}