package app.display.widgets.quiz 
{
	import assets.widgets.QuizOption;
	import flash.display.DisplayObjectContainer;
	import flash.text.TextFieldAutoSize;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class QuizOption extends assets.widgets.QuizOption 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// stage intances
				
			
			// properties
				public var id				:String;
				
			// accessors
				protected var _index		:int;
				protected var _state		:Boolean;
				protected var _text			:String;
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function QuizOption(index:int) 
			{
				_index		= index;
				tf.autoSize	= TextFieldAutoSize.LEFT;
				text		= 'Option ' + (index + 1);
				state		= false;
			}
		
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get index():int { return _index; }
		
			public function get state():Boolean { return _state; }
			public function set state(value:Boolean):void 
			{
				_state = value;
				gotoAndStop(state ? 2 : 1);
			}
			
			public function set text(value:String):void 
			{
				tf.text = value;
				button.width = tf.width + 70;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}