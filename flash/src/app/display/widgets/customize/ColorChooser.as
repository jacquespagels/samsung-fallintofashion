package app.display.widgets.customize 
{
	import app.display.controls.tabs.TabGroup;
	import app.display.widgets.preview.PreviewPlaceholder;
	import app.display.widgets.swatches.Swatches;
	import app.models.vo.ColorModel;
	import app.models.products.DeviceModel;
	import assets.widgets.ColorChooserAsset;
	import core.models.XMLModel;
	import core.utils.LayoutUtils;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class ColorChooser extends ColorChooserAsset 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				public var tabGroup				:TabGroup;
				public var swatchPanel			:Swatches;
				public var placeholderFront		:PreviewPlaceholder;
				public var placeholderBack		:PreviewPlaceholder;
				
			// properties
				protected var contentModel		:XMLModel;
				public var deviceModel			:DeviceModel;
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function ColorChooser(parent:DisplayObjectContainer, contentModel:XMLModel, deviceModel:DeviceModel) 
			{
				this.contentModel	= contentModel;
				this.deviceModel	= deviceModel;
				parent.addChild(this);
				initialize();
				build();
			}
			
			protected function initialize():void
			{
				deviceModel.addEventListener(Event.CHANGE, onDeviceModelChange);
			}
		
			protected function build():void 
			{
				// tabs
					tabGroup = new TabGroup(this);
					tabGroup.clear();
					LayoutUtils.replace(tabs, tabGroup).align();
					
				// swatches
					swatchPanel			= new Swatches(swatches, true);
					swatchPanel.addEventListener(Event.CHANGE, onSwatchChange);
					LayoutUtils.replace(swatches, swatchPanel).align();
					
				// preview
					placeholderFront	= new PreviewPlaceholder(front);
					placeholderBack		= new PreviewPlaceholder(back);
			}

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function update():void 
			{
				throw new Error('This method needs to be overridden in the subclass');
			}
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function setColor(color:ColorModel):void 
			{
				//trace('Set color:' + color.id);
				this.color.text = color.name;
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers


			protected function onSwatchChange(event:Event):void 
			{
				trace('swatch change')
				setColor(swatchPanel.color);
			}
			
			protected function onDeviceModelChange(event:Event):void 
			{
				update();
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}