package app.display.widgets.customize 
{
	import app.models.vo.ColorModel;
	import app.models.products.CoverModel;
	import app.models.products.DeviceModel;
	import core.models.XMLModel;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class CoverChooser extends ColorChooser
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function CoverChooser(parent:DisplayObjectContainer, contentModel:XMLModel, deviceModel:DeviceModel) 
			{
				super(parent, contentModel, deviceModel);
			}
		
			override protected function build():void 
			{
				super.build();
				tabGroup.addEventListener(Event.CHANGE, onTabChange);
				for each(var datum:Object in deviceModel.getCovers())
				{
					tabGroup.addTab(datum);
				}
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			override public function update():void 
			{
				//this.deviceModel = deviceModel;
				color.text = deviceModel.cover.color.name;
				swatchPanel.update(deviceModel.cover.getColors());
				onSwatchChange(null);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function setProduct(productId:String):void 
			{
				// update device
					deviceModel.setCover(productId);
				
				// update swatch panel
					swatchPanel.update(deviceModel.cover.getColors());
					
				// update colors
					onSwatchChange(null);
			}
			
			override protected function setColor(color:ColorModel):void 
			{
				trace('Set cover color:' + color.id);
				super.setColor(color);
				deviceModel.cover.setColor(color.id);
				placeholderFront.update(deviceModel.cover.front);
				placeholderBack.update(deviceModel.cover.back);
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onTabChange(event:Event):void 
			{
				trace('TAB COVER CHANGE')
				setProduct(tabGroup.type); 
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}