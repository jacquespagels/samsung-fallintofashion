package app.display.widgets.customize 
{
	import app.Application;
	import app.display.controls.tabs.TabGroup;
	import app.display.widgets.panels.FeaturePanel;
	import app.display.widgets.preview.PreviewPlaceholder;
	import app.display.widgets.swatches.Swatches;
	import app.display.widgets.threesixty.ThreeSixty;
	import app.models.products.DeviceModel;
	import assets.widgets.DeviceChooserAsset;
	import core.models.XMLModel;
	import core.net.Loader;
	import core.utils.LayoutUtils;
	import core.utils.Template;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class DeviceChooser extends DeviceChooserAsset 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// instances
				public var tabGroup				:TabGroup;
				public var featurePanel			:FeaturePanel;
				public var swatchPanel			:Swatches;
				public var placeholder			:PreviewPlaceholder;
				
			
			// properties
				protected var contentModel		:XMLModel;
				public var deviceModel			:DeviceModel;
				
			// variables
				protected var devices			:Vector.<DeviceModel>;
				protected var video				:ThreeSixty;
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function DeviceChooser(parent:DisplayObjectContainer, contentModel:XMLModel, deviceModel:DeviceModel) 
			{
				this.contentModel	= contentModel;
				this.deviceModel	= deviceModel;
				parent.addChild(this);
				initialize();
				build();
			}
			
			protected function initialize():void
			{
				deviceModel.addEventListener(Event.CHANGE, onDeviceModelChange);
			}
		
			protected function build():void 
			{
				// tabs
					tabGroup = new TabGroup(this);
					tabGroup.addEventListener(Event.CHANGE, onTabChange);
					tabGroup.clear();
					LayoutUtils.replace(tabs, tabGroup).align();
					addChildAt(tabGroup, 0);
					
				// tab data
					devices = DeviceModel.all();
					for each(var datum:Object in devices)
					{
						tabGroup.addTab(datum);
					}
				
				// features
					featurePanel		= new FeaturePanel(this, contentModel.getItem('features'));
					LayoutUtils.replace(features, featurePanel).align();
					
				// colors
					color.text			= contentModel.getItem('color');
					
				// swatches
					swatchPanel			= new Swatches(swatches, false);
					swatchPanel.addEventListener(Event.CHANGE, onSwatchChange);
					LayoutUtils.replace(swatches, swatchPanel).align();
					
				// preview
					placeholder		= new PreviewPlaceholder(preview);
					
				// 360 video
					video			= new ThreeSixty(this);
					video.x			= 420;
					video.y			= 160;
					
				// update
					onTabChange(null);
			}
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function update():void 
			{
				trace('Updating device tab to: ' + deviceModel.deviceId)
				tabGroup.activateTab(deviceModel.deviceId);
				onTabChange(null);
			}
			

			public function reset():void 
			{
				
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function setDevice(deviceId:String):void 
			{
				// update device
					//deviceModel = DeviceModel.factory(deviceId);
					deviceModel.setType(deviceId);

				
				// update feature panel
					featurePanel.update(deviceModel.features);
					
				// update swatch panel
					try
					{
						swatchPanel.update(deviceModel.getVideoColors());
						onSwatchChange(null);
					}
					catch (err:Error)
					{
						trace('Can\'t update the swatch panel...');
						swatchPanel.update(deviceModel.getColors());
					}
					
				// dispatch event
					dispatchEvent(new Event(Event.CHANGE));
			}
			
			protected function setColor(colorId:String):void 
			{
				// update model
					//trace('Load new video:' + colorId);
					deviceModel.setColor(colorId);
					
				// load in the correct image / video
					if (deviceModel.sprite)
					{
						placeholder.visible = false;
						video.visible		= true;
						video.load(deviceModel.sprite);
					}
					else
					{
						placeholder.visible = true;
						video.visible		= false;
						placeholder.update(deviceModel.node.@front);
					}
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			protected function onTabChange(event:Event):void 
			{
				setDevice(tabGroup.id); 
			}

			protected function onSwatchChange(event:Event):void 
			{
				setColor(swatchPanel.color.id);
			}
			
			protected function onDeviceModelChange(event:Event):void 
			{
				update();
			}
			

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}