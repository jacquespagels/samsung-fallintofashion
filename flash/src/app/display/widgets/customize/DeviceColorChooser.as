package app.display.widgets.customize 
{
	import app.models.vo.ColorModel;
	import app.models.products.DeviceModel;
	import core.models.XMLModel;
	import flash.display.DisplayObjectContainer;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class DeviceColorChooser extends ColorChooser 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function DeviceColorChooser(parent:DisplayObjectContainer, contentModel:XMLModel, deviceModel:DeviceModel) 
			{
				super(parent, contentModel, deviceModel);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			override public function update():void 
			{
				
				//this.deviceModel = deviceModel;
				//trace(deviceModel)
				color.text = deviceModel.color.name;
				swatchPanel.update(deviceModel.getColors());
				onSwatchChange(null);
				
				//swatchPanel.update(
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			override protected function setColor(color:ColorModel):void 
			{
				trace('Set device color:' + color.id);
				super.setColor(color);
				deviceModel.setColor(color.id);
				placeholderFront.update(deviceModel.front);
				placeholderBack.update(deviceModel.back);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}