package app.display.widgets.panels 
{
	import assets.widgets.FeaturePanel;
	import assets.widgets.FeatureOption;
	import core.display.layout.VBox;
	import flash.display.DisplayObjectContainer;
	
	/**
	 * Shows the phone's features in a text block with heading
	 * @author Dave Stewart
	 */
	public class FeaturePanel extends assets.widgets.FeaturePanel 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// instances
				public var vbox			:VBox;
				
			
			// properties
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function FeaturePanel(parent:DisplayObjectContainer, title:String) 
			{
				parent.addChild(this);
				this.title.text = title;
				build();
			}
		
			protected function build():void 
			{
				// vbox
					vbox			= new VBox(this);
					vbox.x			= 28;
					vbox.y			= 70;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function update(features:XMLList):void
			{
				// clear vbox
					vbox.clear();
					
				// add new options
					var option:FeatureOption;
					for each (var node:XML in features) 
					{
						option			= new FeatureOption();
						option.tf.text	= node;
						vbox.addChild(option);
					}
					
				// set background size
					vbox.redraw();
					bg.height = vbox.y + vbox.height + 20;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}