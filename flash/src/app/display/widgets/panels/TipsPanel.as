package app.display.widgets.panels 
{
	import app.Application;
	import app.models.vo.TipModel;
	import core.display.elements.Element;
	import core.display.layout.Stack;
	import app.display.elements.TextSlider;
	import core.net.Loader;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import core.utils.LayoutUtils;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class TipsPanel extends Element 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				public var stack			:Stack;
				public var slider			:TextSlider;
				public var placeholder		:DisplayObject;

				
			// properties
				protected var _index:		int;
				protected var _data			:Vector.<TipModel>;
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function TipsPanel(parent:DisplayObjectContainer, placeholder:DisplayObject) 
			{
				parent.addChildAt(this, 1); // this is called, instead of super() so it goes at the bottom
				this.placeholder = addChild(placeholder);
				placeholder.visible = false;
			}
		
			override protected function build():void 
			{
				// images
					stack	= new Stack(this);
					
				// text
					slider	= new TextSlider();
					slider.addEventListener(Event.CHANGE, onTipsChange);
					addChild(slider);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get index():int { return _index; }
			public function set index(value:int):void 
			{
				if (index >= 0 && index <= _data.length)
				{
					// variable
						_index	= value;
						
					// update stack
						stack.showElement(_index);
						
					// align tips panel
						LayoutUtils
							.grab(placeholder, slider)
							.match('width')
							.align('left')
							.align('bottom');
				}
			}
		
			public function update():void 
			{
				// set data
					_data = Application.instance.lookModel.tips;
					
				// add new images
					stack.clear();
					stack.addElement(Loader.getBitmap(_data[0].url))
					stack.addElement(Loader.getBitmap(_data[1].url))
					
				// add new text
					slider.clear();
					for each(var tip:TipModel in _data)
					{
						slider.addText(tip.text);
					}
					
				// update stack
					index = 0;
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onTipsChange(event:Event):void 
			{
				trace('SLIDER CHANGE!')
				index = slider.index;
			}
					
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}