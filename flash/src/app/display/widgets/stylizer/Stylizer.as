package app.display.widgets.stylizer 
{
	import app.display.screens.*;
	import app.models.LookModel;
	import app.utils.ColorUtils;
	import com.greensock.layout.AutoFitArea;
	import com.greensock.layout.ScaleMode;
	import core.display.elements.Element;
	import core.display.elements.Image;
	import core.display.layout.VBox;
	import dev.colors.LookSwatch;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	

	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Stylizer extends Element 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// instances
				protected var image			:Image;
				protected var area			:AutoFitArea;
				protected var shape			:Shape;
				protected var container		:Sprite;
			
			// properties
				protected var _color		:int;
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function Stylizer(parent:DisplayObjectContainer=null) 
			{
				super(parent);
			}
			
			override protected function build():void 
			{
				// super
					super.build();
					
				// properties
					buttonMode = true;
					useHandCursor = true;
					
				// background
					shape = new Shape();
					shape.graphics.beginFill(0xFFFFFF, 0.5);
					shape.graphics.drawRect(-10, -10, 320 + 20, 400 + 20);
					shape.blendMode = 'layer';
					shape.alpha = 0.5;
					addChild(shape);
					
				// container for area and image
					container = new Sprite();
					addChild(container);
					
				// image
					image = new Image(false);
					container.addChild(image);
					image.addEventListener(Event.COMPLETE, onImageComplete);
					addChild(image);
					
				// autofit
					area = new AutoFitArea(this, 0, 0, 320, 400, 0xFFFFFF);
					area.alpha = 0.2;
					addChild(area);
					area.attach(image, { scaleMode:ScaleMode.PROPORTIONAL_OUTSIDE, crop:true } );
					
				// interactivity
					addEventListener(MouseEvent.CLICK, onImageClick);
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function reset():void 
			{
				image.reset();
			}
		
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get color():int { return _color; }
			
			public function get look():LookModel
			{
				// input values
					var color	:int					= this.color;
					var looks	:Vector.<LookModel>		= LookModel.all();
				
				// temp values
					var LAB		:Array					= ColorUtils.RGBtoLAB.apply(null, ColorUtils.HEXtoRGB(color));
					var DIST	:Number					= Number.MAX_VALUE;
					
				// output values
					var LOOK	:LookModel;
					
				// loop over looks and get closest
					for each (var look:LookModel in looks)
					{
						// variables
							var lab		:Array		= ColorUtils.RGBtoLAB.apply(null, ColorUtils.HEXtoRGB(look.color.value));
							var dist	:Number		= ColorUtils.distance(LAB, lab);
							if (dist < DIST)
							{
								LOOK	= look;
								DIST	= dist;
							}
					}
					
				// update swatch
					return LOOK;
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onImageClick(event:MouseEvent):void 
			{
				image.select();
			}
		
			protected function onImageComplete(event:Event):void 
			{
				_color = ColorUtils.getAverageBitmapColour(image.bitmapData);
				area.update();
				dispatchEvent(new Event(Event.COMPLETE));
			}
			
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}