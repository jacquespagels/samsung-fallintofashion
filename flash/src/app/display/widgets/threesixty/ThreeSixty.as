package app.display.widgets.threesixty 
{
	
	import app.display.preloaders.ProgressBar;
	import com.greensock.easing.Linear;
	import com.greensock.easing.Strong;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.TweenMax;
	import core.display.elements.Element;
	import core.utils.LayoutUtils;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class ThreeSixty extends Element 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// instances
				protected var progress			:ProgressBar;
				protected var shape				:Shape;
				protected var sprite			:DisplayObject;
				protected var container			:Sprite;
				
			
			// properties
				protected var loaded			:Boolean;
				protected var loader			:ImageLoader;
				protected var url				:String;
				
				
			// variables
				protected var _scrub			:int;
				protected var _start			:int;
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function ThreeSixty(parent:DisplayObjectContainer) 
			{
				super(parent);
			}
		
			override protected function initialize():void 
			{
				_start	= 1000;
				_scrub	= 0;
				loaded	= false;
			}
			
			override protected function build():void 
			{
				// super
					super.build();
					
				// mask shape
					shape		= new Shape();
					shape.graphics.beginFill(0xFF0000);
					shape.graphics.drawRect(0, 0, 220, 450);
					addChild(shape);
					
				// progress bar
					progress = new ProgressBar(this);
					progress.scaleX = progress.scaleY = 0.5;
					progress.x	= (220 - (progress.width * progress.scaleX)) / 2;
					progress.y	= 420 / 2;
					progress.alpha = 0;
					
				// container for the loaded sprite
					container	= new Sprite();
					container.mask = shape;
					container.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
					addChild(container);
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function load(url:String):void 
			{
				// cleanup old content
					if (sprite)
					{
						loader.dispose();
						container.removeChild(sprite);
					}
					
				// load in new content
					loader		= new ImageLoader(url, { container:container, onComplete:onComplete, onProgress:onProgress } );
					sprite		= loader.content;
					loader.load();
					
				// update everything
					progress.show();
					initialize();
			}

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function spin():void 
			{
				TweenMax.to(this, 2, { scrub:25, ease:Linear.easeNone} );
			}
			
			public function get scrub():int
			{
				return _scrub;
			}
			
			public function set scrub(value:int):void 
			{
				// offset the value
					var offset:int = Math.abs(value % 25);
					
				// update the image
					sprite.x = - shape.width * offset;
					
				// update scrub value
					_scrub = value;
			}
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onProgress(event:LoaderEvent):void 
			{
				loaded = false;
				progress.update(loader.bytesLoaded / loader.bytesTotal);
			}
			
			protected function onComplete(event:LoaderEvent):void 
			{
				loaded = true;
				progress.hide();
				spin();
			}
		
			protected function onMouseDown(event:MouseEvent):void 
			{
				if (loaded)
				{
					// stop any animation
						TweenMax.killTweensOf(this);
						
					// set up listeners
						stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
						stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
						
					// set current offset
						_start = _scrub + Math.round(event.stageX / 20) + 1000;
				}
			}
			
			protected function onMouseUp(event:MouseEvent):void 
			{
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
				stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			}
			
			protected function onMouseMove(event:MouseEvent):void 
			{
				scrub = _start - Math.round(event.stageX / 20);
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}