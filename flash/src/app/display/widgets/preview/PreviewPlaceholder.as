package app.display.widgets.preview 
{
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import core.display.elements.Element;
	import core.net.Loader;
	import core.utils.LayoutUtils;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class PreviewPlaceholder extends Sprite 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				protected var type		:String;
				protected var loader	:Loader;
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function PreviewPlaceholder(target:DisplayObjectContainer, type:String = 'front') 
			{
				// properties
					this.type		= type;
					this.loader		= new Loader('preview placeholder');
					
				// replace
					LayoutUtils
						.replace(target, this)
						.match('scale')
						.align();
			}
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function update(url:String):void
			{
				loader.addImage(url, null, onComplete);
				loader.load();
			}
		
			override public function addChild(child:DisplayObject):DisplayObject 
			{
				if (child)
				{
					while (numChildren > 0)
					{
						removeChildAt(0);
					}
					super.addChild(child);
				}
				return child;
			}
			
			public function setFromNode(node:XML, type:String = null):void
			{
				// parameters
					type = type || this.type;
					
				// load in the image
					
					
				
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onComplete(event:LoaderEvent):void 
			{
				var loader	:ImageLoader	= event.target as ImageLoader;
				var bitmap	:Bitmap			= new Bitmap((loader.rawContent as Bitmap).bitmapData);
				addChild(bitmap);
			}
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}