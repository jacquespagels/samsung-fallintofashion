package app.display.widgets.preview 
{
	import app.Application;
	import app.models.products.DeviceModel;
	import assets.widgets.PreviewAsset;
	import com.greensock.loading.LoaderMax;
	import core.net.Loader;
	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * Shows the phone plus covers in a little triage
	 * @author Dave Stewart
	 */
	public class PreviewWidget extends PreviewAsset 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				protected var placeholders		:Vector.<DisplayObjectContainer>;
				
			
			// properties
				protected var deviceModel		:DeviceModel;
				protected var loader			:Loader;
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function PreviewWidget(parent:DisplayObjectContainer, deviceModel:DeviceModel) 
			{
				this.deviceModel = deviceModel;
				parent.addChild(this);
				initialize();
				build();
			}
		
			protected function initialize():void 
			{
				// loader
					loader = Application.instance.loader;
				
				// model
					deviceModel.addEventListener(Event.CHANGE, onDeviceModelChange);
					
				// placeholders
					placeholders = new Vector.<DisplayObjectContainer>();
					placeholders.push(front);
					placeholders.push(back);
					placeholders.push(cover);
			}
		
			protected function build():void 
			{
				front.placeholder.visible = false;
				back.placeholder.visible = false;
				cover.placeholder.visible = false;
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function update(deviceModel:DeviceModel):void
			{
				//trace('PREVIEW:' + deviceModel);
				
				this.deviceModel = deviceModel;
				
				var front	:Bitmap = Loader.getBitmap(deviceModel.front);
				var back	:Bitmap = Loader.getBitmap(deviceModel.back);
				var cover	:Bitmap = Loader.getBitmap(deviceModel.cover.front);

				clear();
				
				gotoAndStop(deviceModel.type);
				
				this.front.addChild(front || new Sprite);
				this.back.addChild(back || new Sprite);
				this.cover.addChild(cover || new Sprite);
			}
		
			public function clear():void
			{
				for each(var placeholder:DisplayObjectContainer in placeholders)
				{
					while (placeholder.numChildren > 1)
					{
						placeholder.removeChildAt(1);
					}
				}
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public override function set width(value:Number):void 
			{
				super.width = value;
				scaleY = scaleX;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onDeviceModelChange(event:Event):void 
			{
				update(deviceModel);
			}
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}