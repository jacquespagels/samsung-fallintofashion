package app.display.widgets.swatches 
{
	import app.models.vo.ColorModel;
	import app.models.products.ProductModel;
	import core.display.elements.Element;
	import core.display.layout.HBox;
	import flash.display.DisplayObjectContainer;
	import flash.display.Shape;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Swatches extends Element 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// instances
				public var bg					:Shape;
				public var hbox					:HBox;
				
			
			// properties
				protected var swatch			:Swatch;
				
			// variables
				protected var addBackground		:Boolean;
				protected var padding			:Number;
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function Swatches(parent:DisplayObjectContainer, addBackground:Boolean = true) 
			{
				this.addBackground = addBackground;
				super(parent);
				
			}

			override protected function initialize():void
			{
				padding = 13;
			}
			
			override protected function build():void 
			{
				// swatches
					hbox = new HBox(this, padding);
					hbox.addEventListener(MouseEvent.CLICK, onSwatchesClick);
					
				// background
					if (addBackground)
					{
						// move hbox
							hbox.x = padding; 
							hbox.y = padding;
							
						// add background
							bg = new Shape();
							bg.graphics.beginFill(0xFFFFFF, 0.8);
							bg.graphics.drawRect(0, 0, 100, 100);
							addChildAt(bg, 0);
					}
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			public function update(colors:Vector.<ColorModel>):void
			{
				// clear old elements
					hbox.clear();
					this.swatch = null;
					
				// build swatches
					var swatch:Swatch;
					for each(var color:ColorModel in colors)
					{
						swatch = new Swatch(color);
						hbox.addChild(swatch);
						if ( ! this.swatch )
						{
							this.swatch = swatch;
						}
					}
					hbox.redraw();
					
				// update background
					if (bg)
					{
						bg.height	= hbox.height + (padding * 2);
						bg.width	= hbox.width + (padding * 2);
					}
			}

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get index():int
			{
				return hbox.getChildIndex(swatch) || -1;
			}
		
			public function get color():ColorModel 
			{
				return swatch.color;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function onSwatchesClick(event:MouseEvent):void 
			{
				if (event.target is Swatch)
				{
					var swatch:Swatch= event.target as Swatch;
					if (this.swatch !== swatch)
					{
						this.swatch = swatch;
						dispatchEvent(new Event(Event.CHANGE));
					}
				}
			}
			
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}