package app.display.widgets.swatches 
{
	import app.models.vo.ColorModel;
	import core.utils.ColorUtils;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Swatch extends Sprite 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				protected var model	:ColorModel;
				
				
			// variables
				protected var over	:Boolean;
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function Swatch(model:ColorModel) 
			{
				this.model	= model;
				initialize();
				draw();
			}
			
			protected function initialize():void 
			{
				buttonMode = true;
				useHandCursor = true;
				addEventListener(MouseEvent.MOUSE_OVER, onMouseOver);
				addEventListener(MouseEvent.MOUSE_OUT, onMouseOut);
			}
			
			protected function draw():void 
			{
				var color:int = ColorUtils.HEXStringToHEX(model.hex);
				graphics.clear();
				graphics.lineStyle(2, over ? 0xCCCCCC : 0xEEEEEE);
				graphics.beginFill(color);
				graphics.drawRect(0, 0, 20, 20);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get color():ColorModel 
			{
				return model;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onMouseOver(event:MouseEvent):void 
			{
				over = true;
				draw();
			}
		
			protected function onMouseOut(event:MouseEvent):void 
			{
				over = false;
				draw();
			}
			
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}