package 
{
	import dev.Application;
	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * Alternative Document class for development.
	 * @author Dave Stewart
	 */
	public class Development extends Sprite 
	{

		public function Development():void 
		{
			new dev.Application(this);
		}

	}

}