package 
{
	import app.Application;
	import flash.display.Sprite;
	import flash.events.Event;

	/**
	 * Main site Document class
	 * @author Dave Stewart
	 */
	[Frame(factoryClass="Preloader")]
	public class Document extends Sprite 
	{

		public function Document():void 
		{
			new app.Application(this);
		}

	}

}