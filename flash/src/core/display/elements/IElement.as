package core.display.elements 
{
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public interface IElement 
	{
		function show():void;
		function hide():void;
	}
	
}