package core.net 
{
	import com.greensock.*;
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.*;
	import com.greensock.loading.display.*;
	import com.greensock.plugins.*;
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.external.ExternalInterface;
	
	/**
	 * Handles loading of all the application data
	 * @author Dave Stewart
	 */
	public class Loader extends EventDispatcher 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				
			
			// properties
				public var name			:String;
				public var queue		:LoaderMax;
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function Loader(name:String = 'loader') 
			{
				this.name = name;
				initialize();
			}

			protected function initialize():void
			{
				queue = new LoaderMax({name:name, onProgress:onProgress, onComplete:onComplete, onError:onError});
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			// starting and stopping
			
				public function clear():void 
				{
					queue.empty();
				}
				
				public function load():void 
				{
					//trace('Starting load of "' + name + '" (' +queue.getChildren().length+ ' items)');
					queue.load();
				}
				
			// loading
			
				public function addMany(urls:*):Loader 
				{
					if (urls is XMLList || urls is Array)
					{
						for each(var url:String in urls)
						{
							add(url);
						}
					}
					return this;
				}
				
				public function add(url:String, name:String = null):*
				{
					return /\.xml$/.test(url)
						? addData(url, name)
						: addImage(url, name);
				}
							
				public function addImage(url:String, name:String = null, onComplete:Function = null):ImageLoader
				{
					//trace('Adding: ' + url);
					var loader:ImageLoader = new ImageLoader(url, { name:name, onComplete:onComplete } );
					return queue.append(loader) as ImageLoader;
				}
				
				public function addData(url:String, name:String = null):XMLLoader
				{
					var loader:XMLLoader = new XMLLoader(url, { name:name } );
					return queue.append(loader) as XMLLoader
				}
				
			// retrieving data
				
				public function getImage(name:String):Bitmap
				{
					var content:* = queue.getContent(name);
					if (content && content.rawContent)
					{
						var bitmap:Bitmap = content.rawContent as Bitmap
						return new Bitmap(bitmap.bitmapData, 'auto', true);
					}
					return null;
				}
			
				public function getData(name:String):XML 
				{
					// grab data
						var data:String = queue.getContent(name);
						
					// remove doctype and xmlns
						data = data
							.replace(/<!DOCTYPE.*?>/, '')
							.replace(/<html.+?>/i, '<html>');
						var xml:XML = new XML(data);
						
					// return
						return xml.body[0];
				}

			public static function getBitmap(url:String):Bitmap 
			{
				var content:* = LoaderMax.getContent(url);
				if (content && content.rawContent)
				{
					var bitmap:Bitmap = content.rawContent as Bitmap
					return new Bitmap(bitmap.bitmapData, 'auto', true);
				}
				return null;
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			public function onProgress(event:LoaderEvent):void 
			{
				//trace('"' + name + '" progress: ' + Math.floor(event.target.progress * 100) + '%');
			}
			 
			public function onComplete(event:LoaderEvent):void 
			{
				//trace(event.target + " is complete!");
				dispatchEvent(new Event(Event.COMPLETE));
			}
			  
			public function onError(event:LoaderEvent):void 
			{
				trace("error occured with " + event.target + ": " + event.text);
			}
			
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}