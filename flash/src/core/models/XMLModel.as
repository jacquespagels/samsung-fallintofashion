package core.models 
{
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class XMLModel
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// properties
				public var xml				:XML;
				public var debug			:Boolean;
				

				// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function XMLModel(xml:XML, debug:Boolean = false) 
			{
				this.xml		= xml;
				this.debug		= debug;
			}

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: main methods
		
			
			/**
			 * Method to retrieve nodes using a CSS-style selector such as "authors #shakespear ~romance ~romeoandjuliet"
			 * 
			 * Uses the following shorthand: #id or ~item
			 * 
			 * @param	expression
			 * @return
			 */
			public function getNode(expression:String):XML
			{
				var nodes:XMLList = find(xml, expression);
				return nodes && nodes.length()
						? nodes[0]
						:null;
			}
			
			/**
			 * 
			 * @param	selector
			 * @param	node
			 * @return
			 */
			public function getNodes(expression:String):XMLList
			{
				return find(xml, expression);
			}
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: helper methods
		
			/**
			 * Gets a single node by its id attribute
			 * @param	id
			 * @return
			 */
			public function getId(id:String):XML
			{
				return getNode('#' + id);
			}

			/**
			 * Gets a single node by its item attribute
			 * @param	item
			 * @return
			 */
			public function getItem(item:String):XML
			{
				return getNode('~' + item);
			}

			/**
			 * Gets a single node by its item attribute
			 * @param	item
			 * @return
			 */
			public function getItems(item:String):XMLList
			{
				return getNodes('~' + item);
			}
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			/**
			 * Finds nodes that match an expression
			 * @param	node
			 * @param	expression
			 * @return
			 */
			protected function find(node:XML, expression:String):XMLList
			{
				// variables
					var selectors	:Array		= expression.replace(/^\s+|\s+$/g, '').split(/\s+/);
					var selector	:String;
					var nodes		:XMLList;
					
				// find node
					while (selectors.length > 0) 
					{
						// selector
							selector	= selectors.shift();
							nodes		= filter(node, selector);
							
						// error if nothing found
							if (nodes.length() == 0)
							{
								var message:String = "The selector '" + selector + "' in expression '" + expression + "' did not match any nodes in node " + String(xml.toXMLString().match(/^<.*>/));
								trace(message);
								if (debug)
								{
									throw new Error(message);
								}
								return null;//<xml><error>{message}</error></xml>.error;
							}
							
						// grab first node
							node = nodes[0];
					}
					
				// return
					return nodes;
			}
			
			/**
			 * Finds nodes that match a selector
			 * @param	selector
			 * @param	node
			 * @return
			 */
			protected function filter(node:XML, selector:String):XMLList
			{
				// attempt match for attribute shorthand
					var matches:Array = selector.match(/^(\W)(.+)/)
					
				// if matched, it must be an attribute selector
					if (matches)
					{
						// extract match variables
							var attr	:String		= matches[1];
							var value	:String		= matches[2];
							
						// item="value"
							if(attr == '~')
							{
								return node..*.(attribute('item') == value);
							}
							
						// id="value"
							else if (attr == '#')
							{
								return node..*.(attribute('id') == value);
							}
							
						// class contains value
							else if(attr == '.')
							{
								var rx:RegExp = new RegExp('\\b' + value + '\\b');
								return node..*.(rx.test(String(attribute('class'))));
							}
							
						// attr exists
							else if(attr == '@')
							{
								return node..*.(attribute(value) != undefined);
							}
					}
					
				// if not, it's a node name
					else
					{
						return node..*.(name() == selector);
					}
					
				// return null if not matched
					return null;
			}
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			public function toString():String 
			{
				return '[object XMLModel childnodes="' +xml.children().length() + '"]';
			}
			
	}

}