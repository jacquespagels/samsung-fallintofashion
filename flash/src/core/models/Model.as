package core.models 
{
	import flash.events.EventDispatcher;
	
	/**
	 * Base model, composing an XMLModel instance, with an id and initializer function
	 * @author Dave Stewart
	 */
	public class Model extends EventDispatcher 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// properties
				protected var _id		:String;
				protected var _data		:XMLModel;
				
				
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function Model(xml:XML, debug:Boolean = false) 
			{
				_data	= new XMLModel(xml, debug);
				_id		= _data.xml.attribute('id');
				initialize();
			}
			
			protected function initialize():void 
			{
				// optionally initialize variables
			}
		
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function get id():String { return _id; }
			
			public function get data():XMLModel { return _data; }
			
			public function get node():XML { return _data.xml; }
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}