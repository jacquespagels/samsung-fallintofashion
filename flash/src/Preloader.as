package 
{
	import app.display.preloaders.SitePreloader;
	import flash.events.ProgressEvent;
	import core.display.elements.Preloader
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Preloader extends core.display.elements.Preloader
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// stage instances
				public var progress:SitePreloader;
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function Preloader() 
			{
				super();
			}
			
			override protected function build():void 
			{
				progress = new SitePreloader(this);
			}
		

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			override protected function update(ratio:Number):void
			{
				progress.update(ratio);
			}
			
			override protected function complete():void 
			{
				super.complete()
				progress.hide();
			}
			
	}
	
}