package dev.loading 
{
	import core.net.Loader;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class LoaderTest 
	{
		
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// instances
				protected var document	:DisplayObjectContainer;
				
			
			// properties
				protected var front		:String;
				protected var back		:String;
				protected var loader	:Loader;
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function LoaderTest(document:DisplayObjectContainer) 
			{
				this.document = document;
				initialize();
			}
		
			protected function initialize():void 
			{
				// loader
					loader = new Loader('test');
					
				// handlers
					loader.addEventListener(Event.COMPLETE, onLoadComplete);
					document.stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseClick);
					
					trace('>' + document.stage)
					
				// load first image
					loadImages();
				
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			protected function loadImages():void 
			{
				// debug
					trace('LOAD IMAGE');
					
				// variables
					front		= "http://davestewart.co.uk/clients/samsung/resources/products/note3/flip/00_EF-WN900B_Standard_Jet-Black-Front.png";
					back		= "http://davestewart.co.uk/clients/samsung/resources/products/note3/flip/01_EF-WN900B_Standard_Jet-Black-Back.png";
					
				// load
					loader.addImage(front, 'front');
					loader.addImage(back, 'back');
					loader.load();
			}
			
			protected function addImage(x:int, y:int):void 
			{
				trace('loadImage');
				
				var name		:String			= 'frosnt';
				var image		:DisplayObject	= loader.getImage(name);
				
				trace(image);
				
				image.x = x;
				image.y = y;
				
				document.addChild(image);

			}
					
			
					
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			protected function onLoadComplete(event:Event):void 
			{
				trace('LOADER COMPLETE');
			}
			
			protected function onMouseClick(event:MouseEvent):void 
			{
				addImage(event.localX, event.localY);
			}
			
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}