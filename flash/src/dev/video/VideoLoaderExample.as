package dev.video
{
	
	import com.greensock.events.LoaderEvent;
	import com.greensock.loading.ImageLoader;
	import com.greensock.loading.LoaderMax;
	import com.greensock.loading.SWFLoader;
	import com.greensock.loading.VideoLoader;
	import com.greensock.loading.display.ContentDisplay;
	import flash.display.MovieClip;
	
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class VideoLoaderExample extends Sprite
	{
		
		public var progress			:MovieClip;
		
		
		protected var url			:String	= '../../../resources/products/note3/360/note3-black.flv'
		protected var loader		:VideoLoader;
		protected var content		:ContentDisplay;
		
		
		protected var startX		:Number = 0;
		
		
		
		public function VideoLoaderExample()
		{
			//activate the loaders we need
			LoaderMax.activate([ImageLoader, SWFLoader, VideoLoader]); 
			
			var queue:LoaderMax = LoaderMax.parse
			(
				[url], 
				{
					maxConnections		:1,
					onProgress			:onProgress, 
					onComplete			:onQueueComplete, 
					onChildComplete		:onChildComplete
				},
				{
					autoPlay:false
				}
			);

			queue.load();
		}
			
		
		protected function scrubVideo(value:Number):void 
		{
			/*
			value = (value / 25) % loader.duration
			trace(value)
			
			loader.pause();
			loader.gotoVideoTime(value)
			
			trace(loader.netStream.currentFPS)
			*/
			
			
		}
		
		
		private function onProgress(event:LoaderEvent):void
		{
			//this.progress.progressBar_mc.scaleX = event.target.progress;
			trace(event.target.progress);
		}
		
		private function onQueueComplete(event:LoaderEvent):void
		{
			loader		= LoaderMax.getLoader(url);
			content		= LoaderMax.getContent(url);
			
			content.x = content.y = 115;
			
			trace(loader.duration)
			
			//loader.playVideo()
			
			
			trace("duration:", loader.duration);
			
			addChild(content);
			//loader.playVideo();
			content.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
		}
		
		
		private function onChildComplete(event:LoaderEvent):void
		{
			trace("child loaded: " + event.target + " inside queue: " + event.currentTarget);
		}
		
		protected function onMouseDown(event:MouseEvent):void 
		{
			content.stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			content.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUp);
			startX = event.localX;
			
			loader.gotoVideoTime(startX += 1/25)
			
		}
		
		protected function onMouseUp(event:MouseEvent):void 
		{
			content.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
			content.stage.removeEventListener(MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		protected function onMouseMove(event:MouseEvent):void 
		{
			var deltaX = event.localX - startX;
			scrubVideo(event.localX - startX);
		}
	}
}