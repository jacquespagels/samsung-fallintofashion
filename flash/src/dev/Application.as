package dev 
{
	import app.ApplicationCore;
	import app.display.elements.TextSlider;
	import app.display.screens.*;
	import app.display.widgets.threesixty.ThreeSixty;
	import app.models.LookModel;
	import app.models.products.CoverModel;
	import app.utils.ColorUtils;
	import core.display.layout.VBox;
	import core.models.XMLModel;
	import core.utils.NumberUtils;
	import core.utils.StringUtils;
	import dev.colors.LookSwatch;
	import dev.loading.LoaderTest;
	import flash.display.Sprite;
	import flash.events.KeyboardEvent;
	
	/**
	 * "Test" Application class
	 * @author Dave Stewart 
	 */
	public class Application extends ApplicationCore 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				protected var threeSixy:ThreeSixty;
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function Application(document:Sprite) 
			{
				super(document);
			}
		
			override protected function initialize():void 
			{
				// create the data
					super.initialize();
					
				// test
					//new LookMatcher(this.document);
					
					//var loader:VideoLoaderExample = new VideoLoaderExample();
					//document.addChild(loader);
					
					threeSixy = new ThreeSixty(document);
					threeSixy.load('../../../resources/products/note3/360/note3-black.png');
					

					document.stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
					
			}
		
			
			protected function onKeyDown(event:KeyboardEvent):void 
			{
				threeSixy.load('../../../resources/products/note3/360/note3-pink.png');
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: loading
			

			protected function getClosestLook():void 
			{
				// input values
					var color	:int					= 0xFFCC99;
					var looks	:Vector.<LookModel>		= LookModel.all();
				
				// temp values
					var LAB		:Array					= ColorUtils.RGBtoLAB.apply(null, ColorUtils.HEXtoRGB(color));
					var DIST	:Number					= Number.MAX_VALUE;
					
				// output values
					var LOOK	:LookModel;
					var SWATCH	:LookSwatch;
					
				// build values
					var vbox	:VBox					= new VBox(document, 10);
					var swatch	:LookSwatch				= new LookSwatch(vbox, color, 'Chosen');
					
				// loop over looks and get closest
					for each (var look:LookModel in looks)
					{
						// swatch
							swatch					= new LookSwatch(vbox, look.color.value, look.name);
							
						// variables
							var lab		:Array		= ColorUtils.RGBtoLAB.apply(null, ColorUtils.HEXtoRGB(look.color.value));
							var dist	:Number		= ColorUtils.distance(LAB, lab);
							if (dist < DIST)
							{
								LOOK	= look;
								DIST	= dist;
								SWATCH	= swatch
							}
					}
					
				// update swatch
					SWATCH.active = true;

			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
		
		
			protected function textSlider():void
			{
				var slider:TextSlider = new TextSlider();
				slider.x	= 30;
				slider.y	= 30;
				slider.addText('his is less trivial. The easiest way to do it is to use a quaterni');
				slider.addText('it is to use a quaternions is less trivial. The easiest way to do ');
				slider.addText('Incremental compile o');
				document.addChild(slider)
			}
		
		
			protected function shortRot():void 
			{
				var a:Number	= 342;
				var b:Number	= 18;
				var r:Number	= Math.PI * - 0.1;
				var s:Number	= Math.PI * 0.1;
				
				trace(NumberUtils.distRad(r, s) * (180 / Math.PI));		
				trace(NumberUtils.distDeg(a, b));		
				trace(NumberUtils.distDegUnsigned(a, b));		
			}
			
			protected function covers():void
			{
				var nodes:* = CoverModel.all('note3');
				trace(nodes);
			}
		
		
			protected function loaderTest():void 
			{
				new LoaderTest(document);
			}
		
			protected function classSelector():void 
			{
				var xml		:XML =	<div id="features">
										<h3>Features</h3>
										<p id="test" class="one">Take pictures with sound thanks to Sound & Shot</p>
										<p item="color" class="one two">Share your music and photos using Group Play</p>
										<p item="color" class="two three">Control the Galaxy S4 without touching it with Air Gesture</p>
										<p class="two one three">Simply expand your memory with a MicroSD card</p>
									</div>
								
				var model	:XMLModel	= new XMLModel(xml);
				var nodes:	XMLList		= model.getNodes('.one');
				
				trace(nodes.toXMLString());

			}
		
			protected function htmlDecode():void 
			{
				var str:String = 'Hello &amp; w&#233;lcome to &lt;The &Omega; Show&gt;';
				trace(StringUtils.htmlDecode(str));
			}
			
			
			/*
			protected function tabs():void 
			{
				//var data	:Vector.<CoverModel>	= previewModel.getCovers();
				var data	:Vector.<DeviceModel>	= DeviceModel.all();
				var tabs	:TabGroup				= new TabGroup(document);
				tabs.clear();
				for each(var datum:Object in data)
				{
					tabs.addTab(datum);
				}
			}

			protected function features():void 
			{
				deviceModel.setType('s4');
				var features	:XMLList			= deviceModel.features;
				var panel		:FeaturePanel		= new FeaturePanel(document, 'This is some text');
				panel.update(features);
			}
			
			protected function swatches():void 
			{
				deviceModel.setType('s4');
				var colors		:Vector.<ColorModel>	= deviceModel.getColors();
				var swatches	:Swatches				= new Swatches(document);
				swatches.addEventListener(Event.CHANGE, function(event:Event):void {
					
					trace(swatches.color.name)
					});
				swatches.update(colors);
			}
			*/
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}