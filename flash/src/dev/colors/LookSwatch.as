package dev.colors 
{
	import app.models.LookModel;
	import core.display.elements.Element;
	import dev.colors.Swatch;
	import flash.display.DisplayObjectContainer;
	import dev.colors.TextField;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class LookSwatch extends Element 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// instances
				protected var tf:TextField;
				
			
			// properties
				public var look		:LookModel;
				public var label	:String;
				public var color	:int;
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function LookSwatch(parent:DisplayObjectContainer, color:int, label:String) 
			{
				this.label = label;
				this.color = color;
				super(parent);
				
			}

			override protected function build():void 
			{
				// swatch
					var swatch:Swatch = new Swatch(color, 20, 20)
					addChild(swatch);
				
				// name
					tf			= new TextField(label);
					tf.x		= swatch.width + 5;
					tf.y		= swatch.height / 2 - tf.height / 2;
					addChild(tf);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			public function set active(state:Boolean):void
			{
				graphics.lineStyle(0.5, 0x000000);
				graphics.drawRect(-3, -3, 500, height + 6);
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}

