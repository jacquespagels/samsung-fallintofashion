package dev.colors 
{
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class TextField extends flash.text.TextField 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// properties
				protected var _centered:Boolean;
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function TextField(text:String = '', centered:Boolean = false) 
			{
				// parametes
					_centered = centered;
					
				// format
					var format:TextFormat = new TextFormat('Arial', 13, 0xFFFFFF);
					
				// name
					this.text		= text;
					multiline		= false;
					autoSize		= 'left';
					setTextFormat(format);
			}

		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			override public function get x():Number 
			{
				return super.x;
			}
			
			override public function set x(value:Number):void 
			{
				super.x = _centered
							? value - width / 2
							: value;
			}
			
			override public function get y():Number 
			{
				return super.y;
			}
			
			override public function set y(value:Number):void 
			{
				super.y = _centered
							? value - height / 2
							: value;
			}
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		
			
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}