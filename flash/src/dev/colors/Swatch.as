package dev.colors 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.text.TextField;

	public class Swatch extends Sprite{
		
		public function Swatch(color:int = 0xFF0000, width:int = 100, height:int = 100):void 
		{
			// swatch
				graphics.beginFill(color);
				graphics.drawRect(0, 0, width, height);
		}
	}

}