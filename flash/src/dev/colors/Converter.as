package dev.colors 
{
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class Converter 
	{
		
		public static function RGBtoLAB(r:Number, g:Number, b:Number):Array
		{
			var xyz:Array = RGBtoXYZ(r, g, b);
			return XYZtoLAB.apply(null, xyz);
		}
		
		public static function RGBtoXYZ(r:Number, g:Number, b:Number):Array
		{
			// normalize values
				r	/= 255;
				g	/= 255;
				b	/= 255;

			// adjusting values
				if(r > 0.04045)
				{
					 r = (r + 0.055) / 1.055;
					 r = Math.pow(r, 2.4);
				}
				else
				{
					 r = r / 12.92;
				}
				if(g > 0.04045)
				{
					 g = (g + 0.055) / 1.055;
					 g = Math.pow(g, 2.4); 
				}
				else
				{
					 g = g/12.92;
				}
				if(b > 0.04045)
				{
					 b = (b + 0.055) / 1.055;
					 b = Math.pow(b, 2.4);
				}
				else
				{
					 b = b / 12.92;
				}

				r		*= 100;
				g	*= 100;
				b	*= 100;

			// applying the matrix
				var x:Number = r * 0.4124 + g * 0.3576 + b * 0.1805;
				var y:Number = r * 0.2126 + g * 0.7152 + b * 0.0722;
				var z:Number = r * 0.0193 + g * 0.1192 + b * 0.9505;

			// return
				return [x, y, z]
		}
		
		public static function XYZtoLAB(x:Number, y:Number, z:Number):Array
		{
			// XYZ to LAB
				x /= 95.047;
				y /= 100;
				z /= 108.883;

			// adjusting the values
				if(x > 0.008856)
				{
					 x = Math.pow(x, 1 / 3);
				}
				else
				{
					 x = 7.787 * x + 16 / 116;
				}
				if(y > 0.008856)
				{
					 y = Math.pow(y, 1 / 3);
				}
				else
				{
					 y = (7.787 * y) + (16 / 116);
				}
				if(z > 0.008856)
				{
					 z = Math.pow(z, 1 / 3);
				}
				else
				{
					 z = 7.787 * z + 16 / 116;
				}

				var l:Number = 116 * y -16;
				var a:Number = 500 * (x - y);
				var b:Number = 200 * (y - z);

			// displaying the values
				return [l, a, b]

		}

		public static function distance(a:Array, b:Array):Number
		{
			return Math.sqrt
			(
				Math.pow(b[0] - a[0], 2) + 
				Math.pow(b[1] - a[1], 2) + 
				Math.pow(b[2] - a[2], 2)
			);
			
			/*
			var d:Number = Math.sqrt(((r2 - r1) ^ 2 + (g2 - g1) ^ 2 + (b2 - b1) ^ 2);
			
			
			
			
			d = Math.sqrt(((r2 - r1) ^ 2 + (g2 - g1) ^ 2 + (b2 - b1) ^ 2);
			
			d = Math.sqrt(((l2 - l1) ^ 2 + (a2 - a2) ^ 2 + (b2 - b1) ^ 2);
			*/
			return 0;
		}
			
	}

}