package dev.colors 
{
	import app.models.DataModel;
	import app.models.LookModel;
	import com.flashfactory.calico.utils.ColorMathUtil;
	import core.display.elements.Element;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	
	/**
	 * ...
	 * @author Dave Stewart
	 */
	public class LookMatcher extends Element 
	{
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: variables
		
			// constants
				
			
			// stage instances
				
			
			// properties
				protected var looks:Vector.<LookModel>;
				
				
			// variables
				
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: instantiation
		
			public function LookMatcher(parent:DisplayObjectContainer = null) 
			{
				super(parent);
			}
			
			override protected function initialize():void 
			{
				looks = LookModel.all();
			}
			
			override protected function build():void
			{
				// declarations
					var color	:int;
					var hsv		:Array;
					var swatch	:LookSwatch;
					
				// position
					x						= stage.stageWidth / 2;
					y						= stage.stageHeight / 2;
					
				// circle
					graphics.lineStyle(0.5, 0x000000, 0.5);
					graphics.lineTo(300, 0);
					graphics.drawCircle(0, 0, 300);
					
				// values
					for (var i:int = 0; i < 360; i+= 10) 
					{
						color		= ColorMathUtil.hsvToHex(i, 100, 100);
						swatch		= new LookSwatch(this, color, String(i));
						position(swatch, i, 300);
						swatch.x -= 10;
						swatch.y -= 10;
					}
				
				// loop over looks and build swatches
					for each (var look:LookModel in looks) 
					{
						swatch = new LookSwatch(this, look.color.value, look.name);
						hsv	= ColorMathUtil.hexToHsv(look.color.value);
						
						position(swatch, hsv[0], hsv[1] * 3);
						swatch.scaleX = swatch.scaleY = (hsv[2] / 100) + 0.5;
						
						trace( hsv[1] / 100)
					}
				
					/*
				var hex		:int		= 0x25FE80;
				var look	:LookModel	= LookModel.fromColorValue(hex);
				
				//trace(look.color.hex)
				
				graphics.beginFill(hex);
				graphics.drawRect(0, 0, 200, 200);
				graphics.beginFill(look.color.value);
				graphics.drawRect(200, 0, 200, 200);
				*/
			}
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: public methods
		
			

		// ---------------------------------------------------------------------------------------------------------------------
		// { region: accessors
		
			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: protected methods
		



			/**
			 * Position elements in a circle
			 * @param	element
			 * @param	angle
			 * @param	distance
			 */
			protected function position(element:DisplayObject, angle:Number, distance:Number):void 
			{
				var rads:Number = 180 / Math.PI;
				element.x		= Math.cos(angle / rads) * distance;
				element.y		= Math.sin(angle / rads) * distance;
			}
		
			/**
			 * Gets a look according to a color
			 * @param	hex
			 */
			public static function fromColorValue(value:int):LookModel
			{
				// convert the value to hsv
					var hex			:String;
					var hsv			:Array;
					var HSV			:Array	= ColorMathUtil.hexToHsv(value);
					var distance	:int	= 360;
					var dist		:Number;
					var a:Number, b:Number;
				

				// grab the hex values for all looks
					var id			:String;
					var looks		:XMLList = DataModel.getNode('#looks').div;
				
				// compare each one to the chosen hex value
					for each (var node:XML in looks)
					{
						// get the color value
							hex 	= String(node.attribute('hex')).replace('#', '');
							value	= parseInt(hex, 16);
							hsv 	= ColorMathUtil.hexToHsv(value)
							
						// distance
							a		= hsv[0] * Math.PI / 180;
							b		= HSV[0] * Math.PI / 180;
							dist	= Math.abs(Math.atan2(Math.sin(a - b), Math.cos(a - b)));
							
						// compare
							if (dist < distance)
							{
								distance = dist;
								id = node.attribute('id');
							}
						
						// dump
							trace(node.attribute('id'), a, dist);
					}

				// debug
					trace(id, dist);				
				
				// return look
					return LookModel.factory(id);
			}
			
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: handlers

			
		
		// ---------------------------------------------------------------------------------------------------------------------
		// { region: utilities
		
			
	}

}